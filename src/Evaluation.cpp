#include "Evaluation.hpp"
#include "Class.hpp"

Evaluation::Evaluation():
	uuid_(UUID()()),
	all_points_for_histogram_(NULL)
{}

Evaluation::Evaluation(IOFiles& r):
	uuid_(r.read<std::string>()),
	title_(r.read<std::string>()),
	date_(r.read<Date>()),
	weight_(r.read<double>()),
	class_average_(r.read<double>()),
	n_criteria_(r.read<unsigned int>()),
	nfails_(r.read<unsigned int>()),
	all_points_for_histogram_(NULL)
{}

Evaluation::~Evaluation(){
	if(all_points_for_histogram_){ delete[] all_points_for_histogram_; }
}

void Evaluation::save(IOFiles& w) const {
	w<<uuid_;
	w.write("Evaluation",title_);
	w<<date_<<weight_<<class_average_<<n_criteria_<<nfails_;
}

void Evaluation::push_back(std::shared_ptr<GenericGrade> const& grade) { evaluation_results_.push_back(grade); }

bool Evaluation::remove_grade_for_student(std::shared_ptr<Student> const& student){
	for(unsigned int i(0);i<evaluation_results_.size();i++){
		if(*evaluation_results_[i]->get_student() == *student){
			evaluation_results_.erase(evaluation_results_.begin()+i);
			return true;
		}
	}
	return false;
}

void Evaluation::update_results(){
	for(auto const& grade:evaluation_results_){
		grade->compute_grade();
		grade->get_student()->compute_average();
	}
	compute_evaluation_stats();
}

std::vector<std::shared_ptr<GenericGrade> > Evaluation::get_grades() const {
	return evaluation_results_;
}

std::shared_ptr<GenericGrade> Evaluation::get_grade(std::shared_ptr<Student> const& student_ptr) const {
	for(auto const& g:evaluation_results_){
		if(*g->get_student() == *student_ptr){ return g; }
	}
	return NULL;
}

unsigned int Evaluation::student_feedback(std::string const& io_path, std::string const& class_id) const {
	std::string fname(class_id);
	if(get_type() == TP::TYPE){ fname += "-TP"; } 
	else { fname += "-" + date_.yyyymmdd('-') + "-feedback"; }
	Latex latex(io_path,fname,"article");
	latex.begindocument(Latex::header);
	summary(latex,class_id);
	latex.command("mynewpage");
	for(auto const& e:evaluation_results_){ student_feedback(latex,e); }
	if(latex.enddocument(true,true)){
		std::cerr<<__PRETTY_FUNCTION__<<": compiling the evaluation feedback failed"<<std::endl;
		return 1;
	}
	return 0;
}

int Evaluation::send_mail() const {
	if(evaluation_results_.size()){
		Class const* class_ptr(evaluation_results_[0]->get_student()->get_class());

		IOFiles r(class_ptr->get_io_path() + "mails/mail.jdbin",false,false);
		if(r.is_open()){
			std::string io_path(class_ptr->get_io_path()  + "mails/" + class_ptr->get_id() + "/" + date_.yyyymmdd('-') + "/");

			Linux command;
			command.mkpath(io_path.c_str());
			MailCurlSMTP mail;
			mail.set_account(r);
			mail.set_subject("Résultat pour l'évaluation du: " + date_.yyyymmdd('.'));
			std::string fname;
			unsigned int compilation_fails(0);
			unsigned int mail_fails(0);
			for(auto const& e:evaluation_results_){
				fname = "evaluation-" + date_.yyyymmdd('-') + "-" + e->get_student()->get_mail_name();
				if(e->student_feedback(io_path,fname)){ compilation_fails++; }
				else {
					mail.set_body(
							e->get_student()->greetings() 
							+ e->write_mail_body()
							+ "Meilleures salutations,\n"
							+ mail.get_signature());
					mail.set_pdf(io_path + fname + ".pdf");
					mail.set_log(io_path + fname + ".log");
					mail.set_to(e->get_student()->get_email());
					if(!mail.send()){ mail_fails++; }
				}
			}
			return compilation_fails+mail_fails*10000;
		}
		std::cerr<<__PRETTY_FUNCTION__<<": the mail is not configured"<<std::endl;
		return -1;
	}
	std::cerr<<__PRETTY_FUNCTION__<<": there is no grade to send"<<std::endl;
	return -2;
}
