#ifndef DEF_EVALUATION
#define DEF_EVALUATION

#include "MailCurlSMTP.hpp"
#include "Student.hpp"
#include "Date.hpp"

class Evaluation{
	public:
		Evaluation();
		Evaluation(IOFiles& r);
		virtual ~Evaluation();
		/*{Forbidden*/
		Evaluation(Evaluation&&) = delete;
		Evaluation(Evaluation const&) = delete;
		Evaluation& operator=(Evaluation const&) = delete;
		/*}*/

		std::string const& get_uuid() const { return uuid_; }
		std::string const& get_title() const { return title_; }
		Date const& get_date() const { return date_; }
		double const& get_class_average() const { return class_average_; }
		double const& get_weight() const { return weight_; }

		void push_back(std::shared_ptr<GenericGrade> const& grade);
		virtual bool remove_grade_for_student(std::shared_ptr<Student> const& student);

		virtual void save(IOFiles& w) const;
		/*!number of criteria*/
		virtual unsigned int get_n_criteria() const { return n_criteria_; }
		/*!number of criteria for which points need to be manually filled*/
		virtual unsigned int get_n_criteria_to_fill() const = 0;
		virtual double get_max_criteria_to_fill(unsigned int const& i) const = 0; 
		virtual unsigned int const& get_type() const = 0;
		virtual std::vector<std::string> list_evaluation_criteria() const = 0;
		virtual void compute_evaluation_stats() = 0;
		virtual void summary(Latex& latex, std::string const& class_id) const = 0;
		virtual void student_feedback(Latex& latex, std::shared_ptr<GenericGrade> const& grade) const = 0;
		/*!it makes sense to create the latex file inside the childfunction because the filename may be different*/
		unsigned int student_feedback(std::string const& io_path, std::string const& class_id) const;
		void update_results();

		int send_mail() const;

		std::vector<std::shared_ptr<GenericGrade> > get_grades() const;
		std::shared_ptr<GenericGrade> get_grade(std::shared_ptr<Student> const& student_ptr) const;

		bool operator==(Evaluation const& e) const { return uuid_ == e.uuid_; }

	protected:
		std::string const uuid_;
		std::string title_       = "Thème";//<!title or topic of the evaluation (must be unique)
		Date date_;                        //<!date of the evaluation
		double weight_           = 0.0;    //<!weight of the evaluaiton on the year average
		double class_average_    = 0.0;    //<!final class average
		/*TODO:(urgent) fix this n_criteria_ issue in a coherent way*/
		unsigned int n_criteria_ = 1;      //<!number of criteria on which the students are evaluated
		unsigned int nfails_     = 1;      //<!number of students failing the evaluation
		unsigned int nrecorded_  = 1;      //<!number of recorded evaluation

		std::vector<double> grades_for_histogram_;
		std::vector<double>* all_points_for_histogram_;
		std::vector<std::shared_ptr<GenericGrade> > evaluation_results_;
};
#endif
