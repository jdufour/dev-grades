#ifndef DEF_CLASS
#define DEF_CLASS

#include <algorithm>
#include "TestOnSkills.hpp"
#include "TestOnSkillsWB.hpp"
#include "TP.hpp"
#include "Note.hpp"

class Year;
class Class{
	public:
		Class(Year const* const year, std::vector<std::shared_ptr<Student> > const& students, std::string const& class_id);
		Class(Year const* const year, IOFiles& r);
		virtual ~Class();
		/*{Forbidden*/
		Class() = delete;
		Class(Class&& c) = delete;
		Class(Class const&) = delete;
		Class& operator=(Class const&) = delete;
		/*}*/

		void edit(std::string const& class_id){ class_id_ = class_id; }

		bool student_remove(unsigned int const& s);
		void student_add(std::shared_ptr<Student> const& student);

		void add_evaluation(std::shared_ptr<Evaluation> const& eval);
		void remove_evaluation(unsigned int const& e);
		void sort_evaluations();

		void add_grade(unsigned int const& s, unsigned int const& e, std::shared_ptr<GenericGrade> const& grade);
		void add_grade(std::shared_ptr<Student> const& student, std::shared_ptr<Evaluation> const& eval, std::shared_ptr<GenericGrade> const& grade);
		bool remove_grade(std::shared_ptr<Student> const& student, std::shared_ptr<Evaluation> const& eval);

		void update_class();
		void compute_year_average();
		void compute_student_average();

		void save(IOFiles& w) const;
		unsigned int summary(bool const& full_summary);
		void summary(Latex& latex, bool const& full_summary);
		unsigned int student_feedback();

		unsigned int const& get_nstudents() const { return nstudents_; }
		unsigned int const& get_nevals() const { return nevals_; }
		std::string const& get_id() const { return class_id_; }
		std::string const& get_io_path() const;

		std::vector<std::shared_ptr<Student> > get_students(){ return students_; }
		std::shared_ptr<Evaluation> get_evaluation(unsigned int const& e){ return evals_[e]; }
		std::shared_ptr<Student> get_student(unsigned int const& s){ return students_[s]; }

		std::vector<std::string> list_evaluations(unsigned int const& n_char) const;
		std::vector<std::string> list_students() const;
		std::vector<std::string> list_average() const;

		std::shared_ptr<GenericGrade> operator()(unsigned int const& s, unsigned int const& e)
		{ assert(s<nstudents_ && e<nevals_); return class_grades_[s+e*nstudents_]; }
		std::shared_ptr<GenericGrade> operator()(unsigned int const& s, unsigned int const& e) const
		{ assert(s<nstudents_ && e<nevals_); return class_grades_[s+e*nstudents_]; }

	private:
		Year const* const year_;
		std::string const uuid_;
		std::string class_id_;
		unsigned int nstudents_;
		std::vector<std::shared_ptr<Student> > students_;
		unsigned int nevals_;
		std::vector<std::shared_ptr<Evaluation> > evals_;
		std::shared_ptr<GenericGrade>* class_grades_;
		double year_average_;
		double year_average_raw_;
		double year_average_grades_without_rounding_;

		static bool sort_by_date(std::shared_ptr<Evaluation> const& a, std::shared_ptr<Evaluation> const& b);
		static bool sort_by_name(std::shared_ptr<Student> const& a, std::shared_ptr<Student> const& b);

		void update_class_grades_array();
};
#endif
