#ifndef DEF_TESTWB
#define DEF_TESTWB

#include "Evaluation.hpp"

class TestWB: public Evaluation{
	public:
		TestWB() = default;
		TestWB(IOFiles& r);
		virtual ~TestWB() = default;
		/*{Forbidden*/
		TestWB(TestWB&&) = delete;
		TestWB(TestWB const&) = delete;
		TestWB& operator=(TestWB const&) = delete;
		/*}*/

		virtual void edit(std::string const& title, Date const& date, double const& weight, double const& deducted_points, double const& bonus_coef, double const& max_bonus_point, Vector<double> const& max_points);
		virtual void save(IOFiles& w) const override;
		void compute_evaluation_stats() override;
		void summary(Latex& latex, std::string const& class_id) const override;
		void student_feedback(Latex& latex, std::shared_ptr<GenericGrade> const& grade) const override;

		std::vector<std::string> list_evaluation_criteria() const override;

		virtual unsigned int get_n_criteria_to_fill() const override { return n_criteria_+1; }
		unsigned int get_n_criteria() const override { return n_criteria_+1; }
		double get_max_criteria_to_fill(unsigned int const& i) const override { return i?max_points_(i-1):max_bonus_point_; }
		double const& get_deducted_points() const { return deducted_points_; };
		Vector<double> const& get_max_points() const { return max_points_; }
		double const& get_bonus_coef() const { return bonus_coef_; }
		double const& get_max_bonus_point() const { return max_bonus_point_; }

		unsigned int const& get_type() const override { return TestWB::TYPE; }
		static const constexpr unsigned int TYPE = 0;
		static const constexpr char* NAME = "Test avec bonus";

		class Grade: public GenericGrade{
			public:
				Grade(std::shared_ptr<Student> const& student_ptr, std::shared_ptr<Evaluation> const& eval_ptr, unsigned int const& n_exercise);
				Grade(std::shared_ptr<Student> const& student_ptr, std::shared_ptr<Evaluation> const& eval_ptr, IOFiles& r);
				virtual ~Grade() = default;
				/*{Forbidden*/
				Grade(Grade&&) = delete;
				Grade(Grade const&) = delete;
				Grade& operator=(Grade const&) = delete;
				/*}*/

				void compute_grade() override;
				virtual void reset_grade(unsigned int const& n) override { points_.set(n,0); }
				virtual std::string get_result_info() const override;
				std::string get_fname() const override { return "testwb-" + eval_->get_date().yyyymmdd('-') + "-" + student_->get_mail_name(); }
				std::string print_grade() const override { return my::tostring(grade_,1) + " (" + my::tostring(grade_raw_,2) + ")"; }
				double const& get_grade_raw() const { return grade_raw_; }

				virtual double const& operator()(unsigned int const& i) const override {
					if(i){ return points_(i-1); }
					else { return bonus_point_; }
				}
				virtual double& operator()(unsigned int const& i) override {
					if(i){ return points_(i-1); }
					else { return bonus_point_; }
				}
				double const& get_bonus_point() const { return bonus_point_; }

				void save(IOFiles& w) const override;

				std::string summary() const;

			private:
				double bonus_point_ = 0.0;
				double grade_raw_   = 0.0;
		};

	protected:
		double class_average_raw_              =  0.0;//!< class average without deduction, bonus nor rounding
		double class_average_without_rounding_ =  0.0;//!< class average with deduction and bonus but no rounding
		double deducted_points_                =  0.0;
		double bonus_coef_                     =  0.5;
		double max_bonus_point_                = 10.0;//!< maximum nomber of points for the bonus test
		double average_bonus_point_            =  0.0;
		unsigned int nbenefit_bonus_           =  0;  //!< number of students that benefit from the bonus
		Vector<double> max_points_;
		Vector<double> average_point_per_exercise_;
};
#endif
