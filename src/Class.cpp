#include "Class.hpp"
#include "Year.hpp"

Class::Class(Year const* const year, std::vector<std::shared_ptr<Student> > const& students, std::string const& class_id):
	year_(year),
	uuid_(UUID()()),
	class_id_(class_id),
	nstudents_(0),
	students_(students),
	nevals_(0),
	class_grades_(NULL)
{
	std::sort(students_.begin(),students_.end(),sort_by_name);
	nstudents_ = students_.size();
	for(auto const& s:students_){ s->set_class(this); }
}

Class::Class(Year const* const year, IOFiles& r):
	year_(year),
	uuid_(r.read<std::string>()),
	class_id_(r.read<std::string>()),
	nstudents_(r.read<unsigned int>()),
	nevals_(r.read<unsigned int>()),
	class_grades_(NULL)
{
	std::shared_ptr<Student> student;
	for(unsigned int i(0);i<nstudents_;i++){
		student = std::make_shared<Student>(r);
		student->set_class(this);
		students_.push_back(student);
	}
	for(unsigned int i(0);i<nevals_;i++){
		switch(r.read<unsigned int>()){
			case Test::TYPE:
				evals_.push_back(std::make_shared<Test>(r));
				break;
			case TestWB::TYPE:
				evals_.push_back(std::make_shared<TestWB>(r));
				break;
			case TestOnSkills::TYPE:
				evals_.push_back(std::make_shared<TestOnSkills>(r));
				break;
			case TestOnSkillsWB::TYPE:
				evals_.push_back(std::make_shared<TestOnSkillsWB>(r));
				break;
			case Note::TYPE:
				evals_.push_back(std::make_shared<Note>(r));
				break;
			case TP::TYPE:
				evals_.push_back(std::make_shared<TP>(r));
				break;
		}
	}

	/*Load grades*/
	std::shared_ptr<Evaluation> eval;
	std::shared_ptr<GenericGrade> grade;
	std::string e_uuid;
	std::string s_uuid;
	for(unsigned int i(0);i<nstudents_*nevals_;i++){
		student = NULL;
		eval = NULL;
		r>>s_uuid;
		if(s_uuid != std::string("NULL")){
			r>>e_uuid;
			for(unsigned int j(0);j<nstudents_;j++){
				if(s_uuid == students_[j]->get_uuid()){
					student = students_[j];
					j = nstudents_;
				}
			}
			for(unsigned int j(0);j<nevals_;j++){
				if(e_uuid == evals_[j]->get_uuid()){
					eval = evals_[j];
					j = nevals_;
				}
			}
			if(eval && student){
				switch(eval->get_type()){
					case Test::TYPE:
						grade = std::make_shared<Test::Grade>(student,eval,r);
						break;
					case TestWB::TYPE:
						grade = std::make_shared<TestWB::Grade>(student,eval,r);
						break;
					case TestOnSkills::TYPE:
						grade = std::make_shared<TestOnSkills::Grade>(student,eval,r);
						break;
					case TestOnSkillsWB::TYPE:
						grade = std::make_shared<TestOnSkillsWB::Grade>(student,eval,r);
						break;
					case Note::TYPE:
						grade = std::make_shared<Note::Grade>(student,eval,r);
						break;
					case TP::TYPE:
						grade = std::make_shared<TP::Grade>(student,eval,r);
						std::dynamic_pointer_cast<TP::Grade>(grade)->load_reports(std::dynamic_pointer_cast<TP::Grade>(grade),r);
						break;
				}
				student->push_back(grade);
				eval->push_back(grade);
			} else {
				if(!eval){ std::cerr<<__PRETTY_FUNCTION__<<": no evaluation found to link to "<<e_uuid<<std::endl; }
				if(!student){ std::cerr<<__PRETTY_FUNCTION__<<": no student found to link to "<<s_uuid<<std::endl; }
			}
		}
	}
	for(auto const& e:evals_){ e->compute_evaluation_stats(); }
	update_class();
}

Class::~Class(){
	if(class_grades_){ delete[] class_grades_; }
}

std::string const& Class::get_io_path() const { return year_->get_io_path(); }

void Class::save(IOFiles& w) const {
	w<<uuid_;
	w.write("Classe",class_id_);
	w.write("nombre d'étudiants",nstudents_);
	w<<nevals_;
	for(auto const& s:students_){ s->save(w); }
	for(auto const& e:evals_){
		w<<e->get_type();
		e->save(w);
	}
	if(class_grades_){
		for(unsigned int i(0);i<nstudents_*nevals_;i++){
			if(class_grades_[i]){ class_grades_[i]->save(w); }
			else { w<<std::string("NULL"); }
		}
	} else { std::cerr<<__PRETTY_FUNCTION__<<": no grade to save for "<<class_id_<<std::endl; }
}

void Class::update_class(){
	compute_student_average();
	compute_year_average();
	update_class_grades_array();
}

void Class::compute_student_average(){
	for(auto& s:students_){ s->compute_average(); }
}

void Class::compute_year_average(){
	year_average_ = 0;
	year_average_raw_ = 0;
	year_average_grades_without_rounding_ = 0;
	unsigned int nregular_students_(nstudents_);
	for(auto& s:students_){
		if(s->get_average(true)){
			year_average_ += s->get_average(true);
			year_average_raw_ += s->get_average_raw();
			year_average_grades_without_rounding_ += s->get_average_grades_without_rounding();
		} else { nregular_students_--; }
	}
	year_average_ /= nregular_students_;
	year_average_raw_ /= nregular_students_;
	year_average_grades_without_rounding_ /= nregular_students_;
}

/*{!Handle students*/
bool Class::student_remove(unsigned int const& s){
	if(s<nstudents_){
		for(auto const& e:evals_){ e->remove_grade_for_student(students_[s]); }
		students_.erase(students_.begin()+s);
		nstudents_--;
		compute_year_average();
		update_class_grades_array();
		return true;
	}
	return false;
}

void Class::student_add(std::shared_ptr<Student> const& student){
	students_.push_back(student);
	nstudents_++;
	std::sort(students_.begin(),students_.end(),sort_by_name);
	update_class_grades_array();
}

std::vector<std::string> Class::list_students() const {
	std::vector<std::string> tmp(nstudents_);
	unsigned int i(0);
	for(auto const& s:students_){ tmp[i++] = s->get_name(true); }
	return tmp;
}

bool Class::sort_by_name(std::shared_ptr<Student> const& a, std::shared_ptr<Student> const& b){
	return my::compare_string_locale(a->get_lastname(),b->get_lastname())<0;
}
/*}*/

/*{!Handle grades*/
void Class::add_grade(unsigned int const& s, unsigned int const& e, std::shared_ptr<GenericGrade> const& grade){
	students_[s]->push_back(grade);
	evals_[e]->push_back(grade);
	class_grades_[s+e*nstudents_] = students_[s]->get_grade(evals_[e]);
}

void Class::add_grade(std::shared_ptr<Student> const& student, std::shared_ptr<Evaluation> const& eval, std::shared_ptr<GenericGrade> const& grade){
	student->push_back(grade);
	eval->push_back(grade);
	update_class_grades_array();
}

bool Class::remove_grade(std::shared_ptr<Student> const& student, std::shared_ptr<Evaluation> const& eval){
	bool er(eval->remove_grade_for_student(student));
	bool sr(student->remove_grade_for_evaluation(eval));
	if(er && sr){
		update_class();
		return true;
	}
	std::cerr<<__PRETTY_FUNCTION__<<": removing grade for student " + student->get_name(true) + " for evaluation " + eval->get_title() + " failed"<<std::endl;
	return false;
}

void Class::update_class_grades_array(){
	if(class_grades_){ delete[] class_grades_; }
	class_grades_ = new std::shared_ptr<GenericGrade>[nstudents_*nevals_];
	for(unsigned int i(0);i<nstudents_;i++){
		for(unsigned int j(0);j<nevals_;j++){
			class_grades_[i+j*nstudents_] = students_[i]->get_grade(evals_[j]);
		}
	}
}
/*}*/

/*{!Handle evaluation*/
std::vector<std::string> Class::list_average() const {
	std::vector<std::string> output;
	for(auto const& s:students_){ output.push_back(my::tostring(s->get_average(true),1)); }
	return output;
}

std::vector<std::string> Class::list_evaluations(unsigned int const& n_char) const {
	std::vector<std::string> output;
	std::string s_tmp;
	for(auto const& e:evals_){
		s_tmp = e->get_title();
		if(s_tmp.size() > n_char){ s_tmp = my::truncate_utf8_string_after_n_char(s_tmp,n_char) + '~'; }
		if(e->get_type() != TP::TYPE){ s_tmp += "\n" + e->get_date().ddmmyyyy(); }
		else { s_tmp += "\n"; }
		s_tmp += "\n" + my::tostring(e->get_class_average(),2)+" ["+my::tostring(e->get_weight())+"]";
		output.push_back(s_tmp);
	}
	return output;
}

void Class::add_evaluation(std::shared_ptr<Evaluation> const& eval){
	evals_.push_back(eval);
	nevals_++;
	sort_evaluations();
}

void Class::remove_evaluation(unsigned int const& e){
	if(nevals_){
		evals_.erase(evals_.begin()+e);
		nevals_--;
		update_class();
	}
}

void Class::sort_evaluations(){
	std::sort(evals_.begin(),evals_.end(),sort_by_date);
	update_class_grades_array();
}

bool Class::sort_by_date(std::shared_ptr<Evaluation> const& a, std::shared_ptr<Evaluation> const& b){
	return a->get_date()<b->get_date();
}
/*}*/

unsigned int Class::summary(bool const& full_summary){
	Latex latex(year_->get_io_path(),class_id_,"article");

	std::string tmp("\\usepgfplotslibrary{statistics}");
	tmp += "\\newcommand{\\vertical}[1]{\\rotatebox{90}{#1}}";
	tmp += "\\title{Physique}";
	tmp += "\\author{"+class_id_+"}";
	latex.begindocument(Latex::header+tmp);
	latex.command("maketitle");
	summary(latex,full_summary);
	if(latex.enddocument(true,true)){
		std::cerr<<__PRETTY_FUNCTION__<<": compiling the year summary for the class '"<<class_id_<<"' failed"<<std::endl;
		return 1;
	}
	return 0;
}

void Class::summary(Latex& latex, bool const& full_summary){
	latex.begin_center();
	std::vector<double> grades_for_histogram;
	std::string tmp("l|S[table-format=1.3]|S[table-format=1.3]|S[table-format=1.3]|");
	for(unsigned int i(0);i<nevals_;i++){ tmp += "|S[table-format=1.3]"; }
	latex.begin_tabular(tmp);
	tmp = "Prénom / Nom & \\multicolumn{3}{c||}{Moyenne}";
	for(auto& e:evals_){ tmp += " & \\vertical{" + e->get_title() + "}"; }
	latex += tmp + "\\\\\\toprule";
	tmp = "Pondération & {Finale} & {Brute} & {Pure}";
	for(auto& e:evals_){ tmp += "& " + my::tostring(e->get_weight()) + " "; }
	latex += tmp + "\\\\\\midrule";
	unsigned int i(0);
	for(auto const& s:students_){
		if(s->is_rounding_unfavorable()){
			latex.command("rowcolor{red!30}"); 
		} else if(i%2){
			latex.command("rowcolor{gray!30}"); 
		}
		tmp = s->get_name(false);
		if(s->get_average(false)>0){
			if(s->is_rounding_unfavorable()){ std::cerr<<s->get_average(true)<<">"<<s->get_average(false)<<" "<<my::tostring(s->get_average(true),1)<<std::endl; }
			tmp += "&" + my::tostring(s->get_average(true),1);
			tmp += "&" + my::tostring(s->get_average_raw(),3);
			tmp += "&" + my::tostring(s->get_average_grades_without_rounding(),3);
			grades_for_histogram.push_back(s->get_average(true));
		} else { tmp += "&&&"; }
		for(unsigned int j(0);j<nevals_;j++){
			tmp += " & ";
			if((*this)(i,j) && (*this)(i,j)->is_recorded()){ tmp += my::tostring( (*this)(i,j)->get_grade(),1); }
		}
		latex += tmp + "\\\\";
		i++;
	}
	latex+= "\\midrule";
	tmp = "Moyenne &";
	tmp+= my::tostring(year_average_,3) + "&" + my::tostring(year_average_raw_,3)+ "&" + my::tostring(year_average_grades_without_rounding_,3);
	for(auto const& e:evals_){ tmp += " & " +  my::tostring(e->get_class_average(),3); }
	latex += tmp + "\\\\\\bottomrule";
	latex.end_tabular();
	latex.end_center();
	latex.command("vfill");
	latex.begin_center();
	latex.begin_tikzpicture();
	latex.begin_axis("ybar, ymin=0, xmin=0.75, xmax=6.25");
	latex.histogram(grades_for_histogram,11,0.75,6.25);
	latex.end_axis();
	latex.end_tikzpicture();
	latex.end_center();

	if(full_summary){
		for(auto& e:evals_){
			latex.command("mynewpage");
			e->summary(latex,class_id_);
		}
	}
}

unsigned int Class::student_feedback(){
	Latex latex(year_->get_io_path(),class_id_+"-student-summary","article");
	latex.begindocument(Latex::header);
	for(auto const& s:students_){
		s->compute_average();
		latex.section(true,s->get_name(false));
		latex.begin_tabular("p{3cm}|llll|p{7cm}");
		latex +="Évaluation & Date & Coef. & Note & (non-arrondie) & Compléments\\\\\\toprule";
		for(auto const& g:s->get_grades()){
			latex += g->get_eval()->get_title() + " & ";
			latex += my::tostring(g->get_eval()->get_date()) + " & ";
			latex += my::tostring(g->get_eval()->get_weight()) + " & ";
			if(g->is_recorded()){
				latex += my::tostring(g->get_grade(),1) + " & ";
				latex += my::tostring(g->get_grade_without_rounding(),3) + " & ";
			} else {
				latex += "(" + my::tostring(g->get_grade(),1) + ") & ";
				latex += "(" + my::tostring(g->get_grade_without_rounding(),3) + ") & ";
			}
			latex+= g->get_result_info() + "\\\\";
		}
		latex +=  "\\bottomrule";
		latex += " Moyenne &&& " + my::tostring(s->get_average(false),1) + "&"+ my::tostring(s->get_average_grades_without_rounding(),3);
		if(s->is_rounding_unfavorable()){ latex += " & Arrondis défavorables"; }
		latex.end_tabular();
	}
	if(latex.enddocument(true,true)){
		std::cerr<<__PRETTY_FUNCTION__<<": compiling the student summary for the class '"<<class_id_<<"' failed"<<std::endl;
		return 1;
	}
	return 0;
}
