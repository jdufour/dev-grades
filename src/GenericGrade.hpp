#ifndef DEF_GENERICGRADE
#define DEF_GENERICGRADE

#include "Latex.hpp"
#include "Vector.hpp"
#include "Global.hpp"
#include "Date.hpp"

class Evaluation;//Evaluation.hpp is included here to solve compilation dependence in GenericGrade.cpp 
class Student;//Student.hpp is included here to solve compilation dependence in GenericGrade.cpp

class GenericGrade{
	public:
		GenericGrade(std::shared_ptr<Student> const& student, std::shared_ptr<Evaluation> const& e, unsigned int const& n);
		virtual ~GenericGrade() = default;
		/*{Forbidden*/
		GenericGrade() = delete;
		GenericGrade(GenericGrade&&) = delete;
		GenericGrade(GenericGrade const&) = delete;
		GenericGrade& operator=(GenericGrade const&) = delete;
		/*}*/

		std::string get_student_name(bool const& firstname_first) const;
		std::string const& get_title() const;
		Date const& get_date() const;
		unsigned int const& get_type() const;
		double const& get_weight() const;
		double const& get_grade() const;
		double const& get_grade_without_rounding() const;
		std::shared_ptr<Student> const get_student() const;
		std::shared_ptr<Evaluation> const get_eval() const;

		virtual void compute_grade() = 0;
		virtual std::string get_fname() const = 0;
		virtual std::string print_grade() const = 0;
		virtual std::string get_result_info() const = 0;
		virtual void reset_grade(unsigned int const& n) = 0;
		/*TODO:(urgent) the removal of these two operators will only affect NCinterface*/
		virtual double const& operator()(unsigned int const& i) const { return points_(i); }
		virtual double& operator()(unsigned int const& i){ return points_(i); }
		virtual void save(IOFiles& w) const;
		virtual std::string write_mail_body() const;
		unsigned int send_mail() const;
		unsigned int student_feedback(std::string const& io_path, std::string const& fname) const;
		double const& get_point(unsigned int const& i) const { return points_(i); }

		void set_catch_up(bool const& catch_up){ catch_up_ = catch_up; }
		void set_recorded(bool const& recorded){ recorded_ = recorded; }
		bool const& is_catch_up() const { return catch_up_; }
		bool const& is_recorded() const { return recorded_; }

	protected:
		GenericGrade(std::shared_ptr<Student> const& s, std::shared_ptr<Evaluation> const& e, IOFiles& r);

		std::shared_ptr<Student> const student_;
		std::shared_ptr<Evaluation> const eval_;
		Vector<double> points_;
		double grade_without_rounding_ = 0.0;  //<! grade without rounding
		double grade_                  = 0.0;  //<! final grade that officially counts
		bool catch_up_                 = false;//<! if the grade was set during a catch up evaluation
		bool recorded_                 = true; //<! if the grade should be taken into account when computing any average
};
#endif
