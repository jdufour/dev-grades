#include "TestOnSkillsWB.hpp"

/*{TestOnSkillsWB*/
const constexpr unsigned int TestOnSkillsWB::TYPE;
const constexpr char* TestOnSkillsWB::NAME;

TestOnSkillsWB::TestOnSkillsWB(IOFiles& r):
	TestWB(r),
	OnSkills(r)
{}

void TestOnSkillsWB::edit(std::string const& title, Date const& date, double const& weight, double const& deducted_points, double const& bonus_coef, double const& max_bonus_point, Vector<double> const& max_points){
	TestWB::edit(title, date, weight, deducted_points, bonus_coef, max_bonus_point, max_points);
	set_n_exercise(n_criteria_);
	for(auto const& e:evaluation_results_){ 
		std::dynamic_pointer_cast<TestOnSkillsWB::Grade>(e)->set_n_exercise(this);
	}
}

void TestOnSkillsWB::save(IOFiles& w) const {
	TestWB::save(w);
	OnSkills::save(w);
}

void TestOnSkillsWB::compute_evaluation_stats(){
	TestWB::compute_evaluation_stats();
	OnSkills::compute_evaluation_stats(evaluation_results_);
}

void TestOnSkillsWB::student_feedback(Latex& latex, std::shared_ptr<GenericGrade> const& grade) const {
	latex.command("mynewpage");
	std::shared_ptr<TestOnSkillsWB::Grade> grade_ptr(std::dynamic_pointer_cast<TestOnSkillsWB::Grade>(grade));
	latex+="\\noindent";
	double bonus_point(grade_ptr->get_bonus_point()/max_bonus_point_*bonus_coef_);
	double total_point(0);
	for(unsigned int i(0);i<n_criteria_;i++){ total_point += grade_ptr->get_point(i); }
	latex.subsection(true,
			grade_ptr->get_student_name(false) 
			+ ": " + grade_ptr->get_title()
			+ " (" + grade_ptr->get_date().yyyymmdd('.') + ") $\\longrightarrow$ " 
			+ my::tostring(grade_ptr->get_grade())
			);

	latex.begin_minipage("0.6\\linewidth");
	latex.begin_itemize();
	if(deducted_points_){
		latex.item("Votre note non arrondie et sans bonus ni déduction est: " + my::tostring(grade_ptr->get_grade_raw(),2) );
		latex.item(my::tostring(deducted_points_) + (deducted_points_<2.0?" point a été déduit du test.":" points ont été déduits du test."));
	} else {
		latex.item("Votre note non arrondie et sans bonus est: " + my::tostring(grade_ptr->get_grade_raw(),2) );
	}
	latex.item("Le bonus vous rapporte " + my::tostring(bonus_point,3) + " sur " + my::tostring(bonus_coef_) + " point.");
	latex.item("La note finale se calcule via:" );
	latex+=("$$" + my::tostring(grade_ptr->get_grade_without_rounding(),2) + "=\\dfrac"
			"{" + my::tostring(total_point) + "}"
			"{" + my::tostring(max_points_.sum()) + (deducted_points_?"-" + my::tostring(deducted_points_):"") + "}"
			"\\cdot 5 + " + my::tostring(bonus_point,3) + " + 1$$");
	latex.end_itemize();
	latex.end_minipage();

	latex.command("hfill");
	latex.begin_minipage("0.3\\linewidth");
	latex.begin_tikzpicture();
	latex.begin_axis("ybar, ymin=0, xmin=0.75, xmax=6.25, scale=0.4");
	latex.histogram(grades_for_histogram_,11,0.75,6.25);
	latex.command("draw[red,ultra thick](axis cs:" + my::tostring(grade_ptr->get_grade()) + ",0) -- ++(axis direction cs:0,30);");
	latex.end_axis();
	latex.end_tikzpicture();
	latex.end_minipage();

	unsigned int i(0);
	for(auto const& exo:grade_ptr->get_exercises()){
		latex.subsubsection(true,"Exercice " + my::tostring(i+1) + " (" + my::tostring(grade_ptr->get_point(i),3) + "/" + my::tostring(max_points_(i)) + ")");
		exo->student_feedback(latex,grade_ptr->get_point(i),max_points_(i),all_points_for_histogram_[i+1],grade_ptr->is_catch_up());
		i++;
	}
}
/*}*/

/*{Grade*/
TestOnSkillsWB::Grade::Grade(std::shared_ptr<Student> const& student_ptr, std::shared_ptr<Evaluation> const& eval_ptr):
	TestWB::Grade(student_ptr,eval_ptr,std::dynamic_pointer_cast<OnSkills>(eval_ptr)->get_n_exercise()),
	GradeOnSkills(std::dynamic_pointer_cast<OnSkills>(eval_ptr))
{}

TestOnSkillsWB::Grade::Grade(std::shared_ptr<Student> const& student_ptr, std::shared_ptr<Evaluation> const& eval_ptr, IOFiles& r):
	TestWB::Grade(student_ptr,eval_ptr,r),
	GradeOnSkills(std::dynamic_pointer_cast<OnSkills>(eval_ptr),r)
{}

void TestOnSkillsWB::Grade::save(IOFiles& w) const {
	TestWB::Grade::save(w);
	for(auto const& e:exercise_){ e->save(w); }
}

void TestOnSkillsWB::Grade::compute_grade(){
	Vector<double> max_points(std::dynamic_pointer_cast<TestOnSkillsWB>(eval_)->get_max_points());
	for(unsigned int i(0);i<exercise_.size();i++){ points_(i) = compute_point(i,max_points); }
	TestWB::Grade::compute_grade();
}

void TestOnSkillsWB::Grade::reset_grade(unsigned int const& n){
	TestWB::Grade::reset_grade(n);
	GradeOnSkills::reset_grade(std::dynamic_pointer_cast<OnSkills>(eval_));
}

std::string TestOnSkillsWB::Grade::get_exercise_point_info(unsigned int const& i) const {
	return "Exo " 
		+ my::tostring(i+1) + "\n" 
		+ my::tostring(points_(i),2) + "/" 
		+ my::tostring(eval_->get_max_criteria_to_fill(i+1));
}
/*}*/
