#ifndef DEF_TEST
#define DEF_TEST

#include "Evaluation.hpp"

class Test: public Evaluation{
	public:
		Test() = default;
		Test(IOFiles& r);
		~Test() = default;
		/*{Forbidden*/
		Test(Test&&) = delete;
		Test(Test const&) = delete;
		Test& operator=(Test const&) = delete;
		/*}*/

		virtual void edit(std::string const& title, Date const& date, double const& weight, double const& deducted_points, Vector<double> const& max_points);
		virtual void save(IOFiles& w) const override;
		void compute_evaluation_stats() override;
		void summary(Latex& latex, std::string const& class_id) const override;
		void student_feedback(Latex& latex, std::shared_ptr<GenericGrade> const& grade) const override;

		virtual unsigned int get_n_criteria_to_fill() const override { return n_criteria_; }
		double get_max_criteria_to_fill(unsigned int const& i) const override { return max_points_(i); }
		std::vector<std::string> list_evaluation_criteria() const override;

		double const& get_deducted_points() const { return deducted_points_; };
		Vector<double> const& get_max_points() const { return max_points_; }

		unsigned int const& get_type() const override { return Test::TYPE; }
		static const constexpr unsigned int TYPE = 1;
		static const constexpr char* NAME = "Test";

		class Grade: public GenericGrade{
			public:
				Grade(std::shared_ptr<Student> const& student_ptr, std::shared_ptr<Evaluation> const& eval_ptr, unsigned int const& n_exercise);
				Grade(std::shared_ptr<Student> const& student_ptr, std::shared_ptr<Evaluation> const& eval_ptr, IOFiles& r);
				virtual ~Grade() = default;
				/*{Forbidden*/
				Grade(Grade&&) = delete;
				Grade(Grade const&) = delete;
				Grade& operator=(Grade const&) = delete;
				/*}*/

				void compute_grade() override;
				virtual void reset_grade(unsigned int const& n) override { points_.set(n,0); }
				virtual std::string get_result_info() const override ;
				std::string get_fname() const override { return "test-" + eval_->get_date().yyyymmdd('-') + "-" + student_->get_mail_name(); }
				std::string print_grade() const override { return my::tostring(grade_,1) + " (" + my::tostring(grade_raw_,2) + ")"; }
				double const& get_grade_raw() const { return grade_raw_; }

				std::string summary() const;

			private:
				double grade_raw_   = 0.0;
		};

	protected:
		double class_average_raw_              = 0.0;//!< class average without deduction nor rounding
		double class_average_without_rounding_ = 0.0;//!< class average with deduction but no rounding
		double deducted_points_                = 0.0;//!< number of point deducted from the total
		Vector<double> max_points_;
		Vector<double> average_point_per_exercise_;
};
#endif
