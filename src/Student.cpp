#include "Student.hpp"
#include "Year.hpp" //could be removed when Year::FIRST_SEMESTER isn't used

Student::Student(std::string const& firstname, std::string const& lastname, std::string const& email, bool const& in_group_A, bool const& is_man):
	class_ptr_(NULL),
	uuid_(UUID()()),
	firstname_(firstname),
	lastname_(lastname),
	email_(email),
	in_group_A_(in_group_A),
	is_man_(is_man)
{}

Student::Student(IOFiles& r):
	class_ptr_(NULL),
	uuid_(r.read<std::string>()),
	firstname_(r.read<std::string>()),
	lastname_(r.read<std::string>()),
	email_(r.read<std::string>()),
	in_group_A_(r.read<bool>()),
#ifdef UPDATE_FROM_V1_1
	is_man_(true)
#else
	is_man_(r.read<bool>())
#endif
{}

void Student::save(IOFiles& w) const {
	w<<uuid_<<firstname_<<lastname_<<email_<<in_group_A_<<is_man_;
}

void Student::edit(std::string const& firstname, std::string const& lastname, std::string const& email, bool const& in_group_A, bool const& is_man){
	firstname_ = firstname;
	lastname_ = lastname;
	email_ = email;
	in_group_A_ = in_group_A;
	is_man_ = is_man;
}

void Student::push_back(std::shared_ptr<GenericGrade> const& grade){ personal_grades_.push_back(grade); }

bool Student::remove_grade_for_evaluation(std::shared_ptr<Evaluation> const& eval_ptr){
	for(unsigned int i(0);i<personal_grades_.size();i++){
		if(personal_grades_[i]->get_eval().get() == eval_ptr.get()){
			personal_grades_.erase(personal_grades_.begin()+i);
			return true;
		}
	}
	return false;
}

std::shared_ptr<GenericGrade> Student::get_grade(std::shared_ptr<Evaluation> const& eval_ptr) const {
	for(auto const& grade_ptr:personal_grades_){
		if(grade_ptr->get_eval().get() == eval_ptr.get()) { return grade_ptr; }
	}
	return NULL;
}

double Student::get_average(bool consider_rounding) const {
	if(consider_rounding){
		return std::max(average_, my::round_nearest(average_grades_without_rounding_,2));
	}
	return average_;
}

void Student::compute_average(){
	average_grades_without_rounding_ = 0.0;
	average_raw_ = 0.0;
	average_ = 0.0;
	if(personal_grades_.size()){
		double total_weight(0.0);
		if(Year::FIRST_SEMESTER){
			double tmp_average_grades_without_rounding(0.0);
			double tmp_average_raw(0.0);
			double tmp_average(0.0);
			double tmp_total_weight(0.0);
			for(auto const& g:personal_grades_){
				if(g->get_type() == TP::TYPE){ g->set_recorded(true); }
				else {
					if(g->is_recorded()){
						tmp_average_raw += g->get_weight()*g->get_grade();
						tmp_average_grades_without_rounding += g->get_weight()*g->get_grade_without_rounding();
						tmp_total_weight += g->get_weight();
					}
				}
			}
			if(tmp_total_weight > 0.0){
				tmp_average_grades_without_rounding /= tmp_total_weight;
				tmp_average_raw /= tmp_total_weight;
				tmp_average = my::round_nearest(tmp_average_raw,2);
			}

			for(auto const& g:personal_grades_){
				if(g->is_recorded()){
					average_raw_ += g->get_weight()*g->get_grade();
					average_grades_without_rounding_ += g->get_weight()*g->get_grade_without_rounding();
					total_weight += g->get_weight();
				}
			}
			if(total_weight > 0.0){
				average_grades_without_rounding_ /= total_weight;
				average_raw_ /= total_weight;
				average_ = my::round_nearest(average_raw_,2);
			}

			if(tmp_average_raw > average_raw_){
				average_grades_without_rounding_ = tmp_average_grades_without_rounding;
				average_raw_ = tmp_average_raw;
				average_ = tmp_average;
				for(auto const& g:personal_grades_){
					if(g->get_type() == TP::TYPE){ g->set_recorded(false); }
				}
			}
		} else {
			for(auto const& g:personal_grades_){
				if(g->get_type() == TP::TYPE){ g->set_recorded(true); }
				if(g->is_recorded()){
					total_weight += g->get_weight();
					average_raw_ += g->get_weight()*g->get_grade();
					average_grades_without_rounding_ += g->get_weight()*g->get_grade_without_rounding();
				}
			}
			if(total_weight > 0.0){
				average_grades_without_rounding_ /= total_weight;
				average_raw_ /= total_weight;
				average_ = my::round_nearest(average_raw_,2);
			}
		}
	}
}
