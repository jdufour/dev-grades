#include "Test.hpp"

/*{Test*/
const constexpr unsigned int Test::TYPE;
const constexpr char* Test::NAME;

Test::Test(IOFiles& r):
	Evaluation(r),
	class_average_raw_(r.read<double>()),
	deducted_points_(r.read<double>()),
	max_points_(r.read<Vector<double> >())
{}

void Test::save(IOFiles& w) const {
	Evaluation::save(w);
	w
		<<class_average_raw_
		<<deducted_points_
		<<max_points_;
}

std::vector<std::string> Test::list_evaluation_criteria() const {
	std::vector<std::string> tmp(n_criteria_);
	for(unsigned int i(0);i<n_criteria_;i++){
		tmp[i] = "Exo " + my::tostring(i+1) + "\n"
			+ my::tostring(average_point_per_exercise_(i),2) + "/" + my::tostring(max_points_(i));
	}
	return tmp;
}

void Test::edit(std::string const& title, Date const& date, double const& weight, double const& deducted_points, Vector<double> const& max_points){
	title_ = title;
	date_ = date;
	weight_ = weight;
	deducted_points_ = deducted_points;
	max_points_ = max_points;
	//This action will delete all records of the previous results
	if(n_criteria_ != max_points_.size()){
		n_criteria_ = max_points_.size();
		for(auto& g:evaluation_results_){
			g->reset_grade(max_points_.size());
		}
	}
}

void Test::compute_evaluation_stats(){
	nfails_ = 0;
	nrecorded_ = 0;
	class_average_ = 0.0;
	class_average_raw_ = 0.0;
	class_average_without_rounding_ = 0.0;
	average_point_per_exercise_.set(n_criteria_,0.0);

	std::shared_ptr<Test::Grade> grade_ptr;
	for(auto const& g:evaluation_results_){
		grade_ptr = std::dynamic_pointer_cast<Test::Grade>(g);
		if(grade_ptr->is_recorded()){
			for(unsigned int i(0);i<n_criteria_;i++){
				average_point_per_exercise_(i) += grade_ptr->get_point(i);
			}
			class_average_ += grade_ptr->get_grade();
			class_average_raw_ += grade_ptr->get_grade_raw();
			class_average_without_rounding_ += grade_ptr->get_grade_without_rounding();
			if(grade_ptr->get_grade()<4){ nfails_++; }
			nrecorded_++;
		}
	}
	if(nrecorded_){
		class_average_ /= nrecorded_;
		class_average_raw_ /= nrecorded_;
		class_average_without_rounding_ /= nrecorded_;
		average_point_per_exercise_ /= nrecorded_;
	}

	grades_for_histogram_.clear();
	if(all_points_for_histogram_){ delete[] all_points_for_histogram_; }
	all_points_for_histogram_ = new std::vector<double>[n_criteria_];
	for(unsigned int i(0);i<n_criteria_;i++){
		all_points_for_histogram_[i] = std::vector<double>(nrecorded_);
	}

	unsigned int j(0);
	for(auto const& g:evaluation_results_){
		grade_ptr = std::dynamic_pointer_cast<Test::Grade>(g);
		if(grade_ptr->is_recorded()){
			for(unsigned int i(0);i<n_criteria_;i++){
				all_points_for_histogram_[i][j] = 100.0*grade_ptr->get_point(i)/max_points_(i);
			}
			grades_for_histogram_.push_back(grade_ptr->get_grade());
			j++;
		}
	}
}

void Test::summary(Latex& latex, std::string const& class_id) const {
	std::string tmp("{l|");
	for(unsigned int i(0);i<n_criteria_;i++){ tmp += "|S[table-format=2.3]"; }
	tmp += "||S[table-format=1.3]|S[table-format=1.3]|S[table-format=1.3]}";

	latex.subsection(true,class_id + ": " + title_);
	latex.begin_center();
	latex.begin_tabular(tmp);
	latex+="\\toprule";
	latex+="Nom / Prénom & \\multicolumn{" + my::tostring(max_points_.size()) + "}{c||}{Points} & \\multicolumn{3}{c}{Notes}\\\\";
	tmp = my::tostring(nfails_) + "/" + my::tostring(nrecorded_) + " & " ;
	for(unsigned int i(0);i<max_points_.size();i++){ tmp += my::tostring(max_points_(i)) + " & "; }
	latex+= tmp + "\\multicolumn{3}{c}{$\\dfrac{5(\\text{P})}{" + my::tostring(max_points_.sum()) + "-" + my::tostring(deducted_points_) + "}+1$} \\\\\\midrule";
	bool gray_line(false);
	for(auto const& e:evaluation_results_){
		if(gray_line){ latex.command("rowcolor{gray!30}"); }
		gray_line = !gray_line;
		latex += std::dynamic_pointer_cast<Test::Grade>(e)->summary();
	}
	latex+="\\midrule";
	tmp = "Moyennes & ";
	for(unsigned int i(0);i<n_criteria_;i++){
		tmp += my::tostring(average_point_per_exercise_(i),3) + " & ";
	}
	tmp += my::tostring(class_average_raw_,3) + " & " + my::tostring(class_average_without_rounding_,3) + " & " + my::tostring(class_average_,3);
	latex += tmp + "\\\\\\bottomrule";
	latex.end_tabular();
	latex.end_center();

	latex.begin_center();
	for(unsigned int i(0);i<n_criteria_;i++){
		latex.begin_tikzpicture();
		latex.begin_axis("ybar, ymin=0, xmin=0, xmax=100, scale=0.4, title={Exo " + my::tostring(i+1) + "}");
		latex.histogram(all_points_for_histogram_[i],5,0,100);
		latex.end_axis();
		latex.end_tikzpicture();
	}
	latex.end_center();

	latex.begin_center();
	latex.begin_tikzpicture();
	latex.begin_axis("ybar, ymin=0, xmin=0.75, xmax=6.25");
	latex.histogram(grades_for_histogram_,11,0.75,6.25);
	latex.end_axis();
	latex.end_tikzpicture();
	latex.end_center();
}

void Test::student_feedback(Latex& latex, std::shared_ptr<GenericGrade> const& grade) const {
	std::shared_ptr<Test::Grade> grade_ptr(std::dynamic_pointer_cast<Test::Grade>(grade));
	double total_point(0);
	for(unsigned int i(0);i<n_criteria_;i++){ total_point += grade_ptr->get_point(i); }

	latex.begin_minipage("\\linewidth");
	latex.subsection(true,
			grade_ptr->get_student_name(false) 
			+ ": " + grade_ptr->get_title()
			+ " (" + grade_ptr->get_date().yyyymmdd('.') + ") $\\longrightarrow$ " 
			+ my::tostring(grade_ptr->get_grade())
			);

	latex.begin_minipage("0.30\\linewidth");
	latex.begin_itemize();
	if(deducted_points_){
		latex.item("Note sans déduction: " + my::tostring(grade_ptr->get_grade_raw(),2) );
		latex.item(my::tostring(deducted_points_) + (deducted_points_<2.0?" point a été déduit du test.":" points ont été déduits du test."));
	}
	latex.item("Note finale non arrondie:" );
	latex+=("$$\\dfrac{" + my::tostring(total_point) + "}{"
			+ my::tostring(max_points_.sum())
			+ (deducted_points_?"-" + my::tostring(deducted_points_):"")
			+ "}\\cdot 5 + 1 = "
			+ my::tostring(grade_ptr->get_grade_without_rounding(),2) + "$$");
	latex.end_itemize();
	latex.end_minipage();
	latex.command("hfill");

	latex.begin_minipage("0.65\\linewidth");
	latex.begin_center();
	for(unsigned int i(0);i<n_criteria_;i++){
		latex.begin_tikzpicture();
		latex.begin_axis("ybar, ymin=0, xmin=0, xmax=100, scale=0.4, title={Exo " + my::tostring(i+1) + ": " + my::tostring(grade_ptr->get_point(i)) + "/" + my::tostring(max_points_(i)) + "}");
		latex.histogram(all_points_for_histogram_[i],5,0,100);
		latex+="\\draw[red,ultra thick](axis cs:" + my::tostring(grade_ptr->get_point(i)/max_points_(i)*100) + ",0) -- ++(axis direction cs:0,30);";
		latex.end_axis();
		latex.end_tikzpicture();
	}
	latex.end_center();
	latex.end_minipage();
	latex.command("vspace{5mm}");
	latex.end_minipage();
}
/*}*/

/*{Grade*/
Test::Grade::Grade(std::shared_ptr<Student> const& student_ptr, std::shared_ptr<Evaluation> const& eval_ptr, unsigned int const& n_exercise):
	GenericGrade(student_ptr,eval_ptr,n_exercise)
{}

Test::Grade::Grade(std::shared_ptr<Student> const& student_ptr, std::shared_ptr<Evaluation> const& eval_ptr, IOFiles& r):
	GenericGrade(student_ptr,eval_ptr,r)
{ compute_grade(); }

void Test::Grade::compute_grade(){
	std::shared_ptr<Test> grade_ptr(std::dynamic_pointer_cast<Test>(eval_));
	unsigned int max_points(grade_ptr->get_max_points().sum());
	if(max_points){
		grade_raw_ = 5.0*points_.sum()/max_points + 1.0;
		grade_without_rounding_ = 5.0*points_.sum()/(max_points-grade_ptr->get_deducted_points()) + 1.0;
	} else { grade_raw_ = grade_without_rounding_ = 0.0; }
	grade_ = std::min(my::round_nearest(grade_without_rounding_,2),6.0);
}

std::string Test::Grade::summary() const {
	std::string tmp(student_->get_name(false) + " & ");
	if(recorded_){
		for(unsigned int i(0);i<points_.size();i++){
			tmp += my::tostring(points_(i),3) + " & ";
		}
		tmp += my::tostring(grade_raw_,3) + " & ";
		tmp += my::tostring(grade_without_rounding_,3) + " & ";
		tmp += my::tostring(grade_);
		if(grade_>my::round_nearest(grade_raw_,2)){ tmp += "*"; }
	} else { tmp += std::string(points_.size()+2,'&'); }
	return tmp + "\\\\";
}

std::string Test::Grade::get_result_info() const {
	std::shared_ptr<Test> eval_ptr(std::dynamic_pointer_cast<Test>(eval_));
	std::string stmp("Points: ");
	for(unsigned int i(0);i<points_.size();i++){
		stmp += my::tostring(points_(i),3) + "/" + my::tostring(eval_ptr->get_max_criteria_to_fill(i)) + (i+1==points_.size()?"":", ");
	}
	return stmp;
}
/*}*/
