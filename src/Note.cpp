#include "Note.hpp"

const constexpr unsigned int Note::TYPE;
const constexpr char* Note::NAME;

Note::Note(IOFiles& r):
	Evaluation(r)
{}

std::vector<std::string> Note::list_evaluation_criteria() const {
	return { "Note\n" + my::tostring(class_average_,2) + "/6" };
}

void Note::edit(std::string const& title, Date const& date, double const& weight){
	title_ = title;
	date_ = date;
	weight_ = weight;
}

void Note::compute_evaluation_stats(){
	nfails_ = 0;
	nrecorded_ = 0;
	class_average_ = 0.0;
	class_average_without_rounding_ = 0.0;
	grades_for_histogram_.clear();
	for(auto const& g:evaluation_results_){
		if(g->is_recorded()){
			class_average_ += g->get_grade();
			class_average_without_rounding_ += g->get_grade_without_rounding();
			if(g->get_grade()<4){ nfails_++; }
			nrecorded_++;
			grades_for_histogram_.push_back(g->get_grade());
		}
	}
	if(nrecorded_){
		class_average_ /= nrecorded_;
		class_average_without_rounding_ /= nrecorded_;
	}
}

void Note::summary(Latex& latex, std::string const& class_id) const {
	std::string tmp("{l||S[table-format=1.3]|S[table-format=1.3]}");

	latex.subsection(true,class_id + ": " + title_);
	latex.begin_center();
	latex.begin_tabular(tmp);
	latex+="\\toprule";
	latex+="Nom / Prénom & \\multicolumn{2}{c}{Notes}\\\\";
	tmp = my::tostring(nfails_) + "/" + my::tostring(nrecorded_) + " & 6 ";
	latex+= tmp + "\\\\\\midrule";
	bool gray_line(false);
	std::shared_ptr<Note::Grade> grade_ptr;
	for(auto const& e:evaluation_results_){
		if(gray_line){ latex.command("rowcolor{gray!30}"); }
		gray_line = !gray_line;
		latex += std::dynamic_pointer_cast<Note::Grade>(e)->summary();
	}
	latex+="\\midrule";
	latex += "Moyennes & " + my::tostring(class_average_without_rounding_,3) + " & " + my::tostring(class_average_,3) + "\\\\\\bottomrule";;
	latex.end_tabular();
	latex.end_center();

	latex.begin_center();
	latex.begin_tikzpicture();
	latex.begin_axis("ybar, ymin=0, xmin=0.75, xmax=6.25");
	latex.histogram(grades_for_histogram_,11,0.75,6.25);
	latex.end_axis();
	latex.end_tikzpicture();
	latex.end_center();
}

void Note::student_feedback(Latex& latex, std::shared_ptr<GenericGrade> const& grade_ptr) const {
	latex.command("mynewpage");
	latex+="\\noindent";
	latex.subsection(true,
			grade_ptr->get_student_name(false) 
			+ ": " + grade_ptr->get_title() 
			+ " (" + grade_ptr->get_date().yyyymmdd('.') + ") $\\longrightarrow$ " 
			+ my::tostring(grade_ptr->get_grade())
			);

	latex.begin_itemize();
	latex.item("Votre note est: " + grade_ptr->print_grade() + " (la note entre parenthèse n'est pas arrondie)" );
	latex.end_itemize();
}

Note::Grade::Grade(std::shared_ptr<Student> const& student, std::shared_ptr<Evaluation> const& eval):
	GenericGrade(student,eval,1)
{}

Note::Grade::Grade(std::shared_ptr<Student> const& student, std::shared_ptr<Evaluation> const& eval, IOFiles& r):
	GenericGrade(student,eval,r)
{ compute_grade(); }

void Note::Grade::compute_grade(){
	grade_without_rounding_ = points_(0);
	grade_ = my::round_nearest(grade_without_rounding_,2);
}

std::string Note::Grade::summary() const {
	std::string tmp(student_->get_name(false) + " & ");
	if(recorded_){
		tmp += my::tostring(points_(0)) + " & " + my::tostring(grade_);
	} else { tmp += "&"; }
	return tmp+"\\\\";
}
