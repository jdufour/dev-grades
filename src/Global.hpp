#ifndef DEF_GLOBAL
#define DEF_GLOBAL
#include <memory>

inline double JDBIN_VERSION = 0;
inline double CURRENT_JDBIN_VERSION = 1.5;

struct uistring {
	uistring(unsigned int const& i, std::string const& t):
		idx(i), text(t){}

	unsigned int idx;
	std::string text;
};

inline bool sort_uistring(uistring const& a, uistring const& b){
	return a.idx < b.idx;
}

inline bool sort_uistring_ptr(std::shared_ptr<uistring> const& a, std::shared_ptr<uistring> const& b){
	return sort_uistring(*a,*b);
}
#endif
