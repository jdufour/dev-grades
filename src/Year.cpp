#include "Year.hpp"

const std::vector<unsigned int> Year::TYPES = {
	Test::TYPE,
	TestWB::TYPE,
	TestOnSkills::TYPE,
	TestOnSkillsWB::TYPE,
	Note::TYPE,
	TP::TYPE
};
const std::vector<std::string> Year::NAMES = {
	Test::NAME,
	TestWB::NAME,
	TestOnSkills::NAME,
	TestOnSkillsWB::NAME,
	Note::NAME,
	TP::NAME
};
bool Year::FIRST_SEMESTER;

Year::Year(std::string const& io_path, Date const& start, Date const& mid, Date const& end):
	io_path_(my::ensure_trailing_slash(io_path)),
	start_(start),
	mid_(mid),
	end_(end)
{
	Time today;
	today_.set(today.day(),today.month(),today.year());
}

Year::Year(std::string const& io_path, std::string const& jdfile):
	io_path_(my::ensure_trailing_slash(io_path))
{
	Time today;
	today_.set(today.day(),today.month(),today.year());

	IOFiles r(jdfile,false,false);
	if(r.is_open()){
#if defined(UPDATE_FROM_V1_1) or defined(UPDATE_FROM_V1_2) or defined(UPDATE_FROM_V1_3) or defined(UPDATE_FROM_V1_4)
		r>>start_>>mid_>>end_>>nclasses_;
#else
		r>>JDBIN_VERSION>>start_>>mid_>>end_>>nclasses_;
#endif
		FIRST_SEMESTER = today_<mid_;
		for(unsigned int i(0);i<nclasses_;i++){
			classes_.push_back(std::make_shared<Class>(this,r));
		}
		r>>loading_msg_;
	}
}

void Year::save() const {
	Time t;
	IOFiles w(io_path_+"jdbin/"+t.date()+".jdbin",true,false);
	w.write("Version du fichier jdbin",CURRENT_JDBIN_VERSION);
	w.write("Début du premier semestre",start_);
	w.write("Début du deuxième semestre",mid_);
	w.write("Fin du deuxième semestre",end_);
	w.write("Nombre de classes",nclasses_);
	for(auto const& c:classes_){ c->save(w); }
	w<<"Chargement complet de la sauvegarde datée du " + t.date(" ",":") + " (jdbin:v" + my::tostring(CURRENT_JDBIN_VERSION) + ")";
}

unsigned int Year::add_class(std::string const& filename, std::string const& class_id){
	if(class_id == ""){
		std::cerr<<__PRETTY_FUNCTION__<<": l'identifiant est manquant"<<std::endl;
		return 1;
	}
	for(auto const& c:classes_){
		if(class_id == c->get_id()){
			std::cerr<<__PRETTY_FUNCTION__<<": l'identifiant '"<<class_id<<"' existe déjà"<<std::endl;
			return 2;
		}
	}
	if(filename.find(".txt") == std::string::npos){
		std::cerr<<__PRETTY_FUNCTION__<<": l'extension du fichier doit être '.txt'"<<std::endl;
		return 3;
	}
	std::ifstream file(filename, std::ios::in);
	if(!file.is_open() || file.bad()){
		std::cerr<<__PRETTY_FUNCTION__<<": le fichier '"<<filename<<"' n'a pas pu être ouvert"<<std::endl;
		return 4;
	}
	std::vector<std::shared_ptr<Student> > students;
	std::string tmps;
	while(std::getline(file,tmps)){
		if(tmps.size()){
			std::vector<std::string> tmp(my::string_split(tmps,';'));
			switch(tmp.size()){
				case 4: students.push_back(std::make_shared<Student>(tmp[3],tmp[2],"",(tmp[0]=="A"),(tmp[1]=="h"))); break;
				case 5: students.push_back(std::make_shared<Student>(tmp[3],tmp[2],tmp[4],(tmp[0]=="A"),(tmp[1]=="h"))); break;
				default:
						std::cerr<<__PRETTY_FUNCTION__<<": le fichier contenant la liste d'étudiants n'a pas le bon format"<<std::endl;
						std::cerr<<__PRETTY_FUNCTION__<<": l'entrée '"<<tmps<<"' n'as pas le bon format"<<std::endl;
						file.close();
						return 5;
						break;
			}
		}
	}
	file.close();
	classes_.push_back(std::make_shared<Class>(this,students,class_id));
	sort_classes();
	nclasses_++;
	return 0;
}

void Year::remove_class(unsigned int const& i){
	classes_.erase(classes_.begin()+i);
	nclasses_--;
}

unsigned int Year::summary(bool const& full_summary) const {
	Latex latex(io_path_,get_years()+(full_summary?"-full-summary":"-short-summary"),"article","12pt");

	//latex.package("geometry","a4paper,margin=1cm,nofoot,nohead");
	//latex.package("hyperref");
	//latex.package("graphicx");
	latex.command("newcommand{\\vertical}[1]{\\rotatebox{90}{#1}}");
	latex.command("title{Physique}");
	latex.command("author{"+get_years()+"}");
	latex.begindocument(Latex::header);
	if(full_summary){
		latex.command("maketitle");
		latex.command("tableofcontents");
		latex.command("mynewpage");
	} else { latex.command("pagenumbering{gobble}"); }
	for(auto const& c:classes_){
		latex.section(!full_summary,c->get_id());
		c->summary(latex,full_summary);
	}
	if(full_summary){ /*needs two compilation*/
		unsigned int tmp(latex.enddocument(true,false));
		return (tmp==0?latex.enddocument(true,true):tmp);
	} else { return latex.enddocument(true,true); }
}

std::vector<std::string> Year::get_classes_list() const {
	std::vector<std::string> tmp(classes_.size());
	for(unsigned int i(0);i<classes_.size();i++){
		tmp[i] = classes_[i]->get_id();
	}
	return tmp;
}

unsigned int Year::get_max_nstudents() const {
	unsigned int max_student(0);
	for(auto const& c:classes_){
		if(max_student < c->get_nstudents()){ max_student = c->get_nstudents(); }
	}
	return max_student;
}

bool Year::sort_by_name(std::shared_ptr<Class> const& a, std::shared_ptr<Class> const& b){
	return a->get_id()<b->get_id();
}

void Year::csv(std::string const& class_id) const {
	for(auto const& c:classes_){
		if(c->get_id() == class_id){
			IOFiles csv(io_path_+class_id+".csv",true,false);
			for(auto const& s:c->get_students()){
				csv<<s->get_lastname()<<","<<s->get_firstname()<<","<<s->get_average_raw()<<IOFiles::endl;
			}
		}
	}
}
