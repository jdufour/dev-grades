#include "NCInterface.hpp"
#include "Directory.hpp"
#include "Parseur.hpp"
#include <sys/file.h>

std::string get_time(){
	time_t rawtime;
	time( &rawtime );
	struct tm *info(localtime(&rawtime));
	char tmp[20];
	std::strftime(tmp,20,"%Y/%m/%d %H:%M:%S",info);
	return tmp;
}

bool can_start(std::string const& pid){
	int pid_file = open(("/tmp/"+pid).c_str(), O_CREAT | O_RDWR, 0666);
	int rc = flock(pid_file, LOCK_EX | LOCK_NB);
	if(rc){
		if(EWOULDBLOCK == errno){ return false; }
		std::cerr<<__PRETTY_FUNCTION__<<": unkown error with pid locking file"<<std::endl;
		return false;
	}
	return true;
}

int main(int argc, char* argv[]){
	std::cerr<<"START: "<<get_time()<<std::endl;
	if(can_start("jdgrades.pid")){
		Parseur P(argc,argv,{"help", "backup", "config_mail", "debug", "force_display", "full", "save", "summary" });
		if(P.find("help")){
			std::cout<<"Voici les options possibles:"<<std::endl<<std::endl<<
				"     -help: affiche cette aide"<<std::endl<<std::endl<<
				"     -backup: permet de charger une sauvarde antérieure à celle par défaut"<<std::endl<<
				"     -config_mail: configure le mail pour envoi aux élèves"<<std::endl<<
				"     -csv [class_id]: génère un fichier '.csv' contenant les moyenne de la classe nommée 'cladd_id'"<<std::endl<<
				"     -debug: ajoute à l'interface une barre d'information utile au débuggage"<<std::endl<<
				"     -force_display: ne vérifie pas si les dimensions du terminal sont suffisantes pour un affichage complet"<<std::endl<<
				"     -full: à utiliser en conjonction avec '-summary', génère un '.pdf' plus complet "<<std::endl<<
				"     -path [chemin]: chemin vers le répertoire de sauvegarde/chargement (contient les '.pdf' et le répertoire de données)"<<std::endl<<
				"     -save: effectue une sauvegarde automatique en fin d'exécution"<<std::endl<<
				"     -summary: génère un '.pdf' unique contenant les résultats des élèves pour toutes les classes"<<std::endl<<std::endl;
		} else {
			std::string io_path;
			Directory dir;

			unsigned int i(0);
			if(P.find("path",i)){
				io_path = my::ensure_trailing_slash(my::trim(P.get<std::string>(i)));
				dir.search_files_ext(".jdbin",io_path+"jdbin/",false,true,false);
				if(dir.size()){
					dir.sort();
					std::string jdfile;
					if(P.find("backup")){
						unsigned int file_number;
						for(unsigned int d(0);d<dir.size();d++){ std::cout<<d<<": "<<dir[d]<<std::endl; }
						do {
							file_number = my::get_number<unsigned int>("Entrez le numéro de la sauvegarde à sélectionner?",0,dir.size()-1);
							std::cout<<"Fichier sélectionné :"<<dir[file_number]<<std::endl;
						} while ( !my::get_yn("Voulez-vous charger ce fichier?","o","n"));
						jdfile = dir[file_number];
					} else { jdfile = dir.last(); }

					Year year(io_path,jdfile);
					Latex::header =
						"\\usepackage[a4paper,margin=1cm]{geometry}\n"
						"\\usepackage[frenchb]{babel}\n"
						"\\usepackage[T1]{fontenc}\n"
						"\\usepackage[utf8]{inputenc}\n"
						"\\usepackage{siunitx}\n"
						"\\usepackage{amsmath}\n"
						"\\usepackage{booktabs}\n"
						"\\usepackage[table]{xcolor}\n" //not usefull in Class.cpp
						"\\usepackage{pgfplots}\n"//not usefull in Class.cpp
						"\\usepgfplotslibrary{statistics}\n"//not usefull in Class.cpp
						"\\pagenumbering{gobble}\n"
						"\\makeatletter\n"
						"\\newcommand*{\\mynewpage}{\n"
						"\\clearpage\n"
						"\\unless\\ifodd\\c@page\\hbox{}\\newpage\\fi\n"
						"}\n"
						"\\makeatother\n";

					if(P.find("summary")){
						std::cout<<std::endl<<
							"+------------------------------------------+"<<std::endl<<
							"|   Génère un résumé de tous les tests     |"<<std::endl<<
							"|   sur l'année pour toutes les classes.   |"<<std::endl<<
							"+------------------------------------------+"<<std::endl<<std::endl;
						year.summary(P.find("full"));
					} else if (P.find("csv",i)) {
						year.csv(P.get<std::string>(i));
					} else {
						NCInterface nci(&year,P.find("debug"),P.find("force_display"));
						if(P.find("config_mail")){ nci.config_mail(); }
						if( nci.is_ready() ) { nci.class_select(); }
					}
					if(P.find("save")){ year.save(); } //To automatically save data when the program exists
				} else { std::cerr<<"Le répertoire proposé n'existe pas ou ne contient pas de classe."<<std::endl; }
			} else  {
				std::cout<<"Aucune option pertinente détectée. Pour plus d'information, exécutez le programme avec l'option '-help'"<<std::endl;
				std::cout<<std::endl<<
					"+-------------+"<<std::endl<<
					"|  Bienvenue  |"<<std::endl<<
					"+-------------+"<<std::endl<<std::endl;
				std::cout<<
					"Aucun chemin spécifié. Pour démarrer, donnez les dates de début de chaque"<<std::endl<<
					"semestre et la date de fin d'année. Fournissez ensuite un répertoire dans"<<std::endl<<
					"lequel les données seront sauvegardées. Pour éviter tout conflit, préférez"<<std::endl<<
					"un répertoire vide. Sélectionnez ensuite un fichier pour une classe contenant"<<std::endl<<
					"la liste des étudiants (les classes additionnelles pourront être ajoutées"<<std::endl<<
					"ultérieurement). Le nom du fichier doit avoir une extension '.txt'. Chaque"<<std::endl<<
					"ligne du fichier contient:"<<std::endl<<std::endl<<
					"     Groupe;Genre;Nom;Prénom;email"<<std::endl<<std::endl<<
					"Le fichier doit respecter les conventions suivantes:"<<std::endl<<
					"     + pas de ligne vide"<<std::endl<<
					"     + le groupe du TP peut être 'A' ou 'B'"<<std::endl<<
					"     + le genre peut être 'h' ou 'f'"<<std::endl<<
					"     + pas d'espace autour des ';'"<<std::endl<<
					"     + l'email est optionnel"<<std::endl<<std::endl<<
					"Donnez ensuite un nom/code à la classe"<<std::endl;

				std::cout<<std::endl<<
					"+------------------------------+"<<std::endl<<
					"|   Étape 1 (Fixer les dates)  |"<<std::endl<<
					"+------------------------------+"<<std::endl<<std::endl;
				Date start;
				std::cout<<"Entrez la date de début du premier semestre (jj.mm.aaaa) "<<std::endl;
				do { std::cin>>start; }
				while (!start.valid());
				Date mid;
				std::cout<<"Entrez la date de début du deuxième semestre (jj.mm.aaaa) "<<std::endl;
				do { std::cin>>mid; }
				while (!mid.valid() || mid<start);
				Date end;
				std::cout<<"Entrez la date de fin du deuxième semestre (jj.mm.aaaa) "<<std::endl;
				do { std::cin>>end; }
				while (!end.valid() || end<mid);

				Linux command;
				bool need_to_select_io_path(true);
				std::cout<<std::endl<<
					"+----------------------------------+"<<std::endl<<
					"|   Étape 2 (choix du répertoire)  |"<<std::endl<<
					"+----------------------------------+"<<std::endl<<std::endl;
				do {
					dir.set();
					io_path = command.return_absolute(my::ensure_trailing_slash(my::trim(my::get_string("Choisissez un répertoire de sauvegarde"))));

					if(dir.list_all_files(io_path,false,false,false)){
						std::cout<<"Voici le répertoire choisi:"<<std::endl<<std::endl<<
							io_path<<std::endl<<std::endl;
						if(dir.size()){
							std::cout<<"Ce répertoire contient les documents suivants:"<<std::endl<<std::endl;
							dir.print(std::cout);
							std::cout<<std::endl<<"Il est conseillé de choisir un répertoire vide."<<std::endl;
						}
						need_to_select_io_path = !(my::get_yn("Initialiser ce répertoire?","o","n"));
					} else { std::cerr<<"Choisissez un répertoire existant"<<std::endl; }
				} while (need_to_select_io_path);
				command.mkdir((io_path+"jdbin").c_str());
				Year year(io_path,start,mid,end);

				std::string file_path;
				std::cout<<std::endl<<
					"+--------------------------------------------+"<<std::endl<<
					"|   Étape 3 (sélection de listes de classe)  |"<<std::endl<<
					"+--------------------------------------------+"<<std::endl<<std::endl;
				do {
					do{
						dir.set();
						file_path=command.return_absolute(my::ensure_trailing_slash(my::trim(my::get_string("Donnez le répertoire contenant les fichiers '.txt' des listes de classe:"))));
						if(dir.search_files_ext(".txt",file_path,false,false,false)){
							if(!dir.size()){
								std::cerr<<"Le répertoire ne contient pas de fichier '.txt'"<<std::endl;
							}
						}
					} while (!dir.size());
					dir.sort();

					std::cout<<
						"Voici le répertoire choisi:"<<std::endl<<std::endl<<
						file_path<<std::endl<<std::endl<<
						"Il contient les fichier '.txt' suivants:"<<std::endl<<std::endl;
					for(unsigned int d(0);d<dir.size();d++){ std::cout<<d<<": "<<dir[d]<<std::endl; }

					do {
						unsigned int file_number(my::get_number<unsigned int>("Entrez le numéro du fichier à sélectionner?",0,dir.size()-1));
						std::cout<<"Fichier sélectionné :"<<dir[file_number]<<std::endl;

						std::string class_id(my::get_string("Entrez le nom de la classe"));
						if(my::get_yn("Voulez-vous ajouter cette classe?","o","n")){ year.add_class(dir[file_number],class_id); }
					} while (my::get_yn("Voulez-vous ajouter une autre classe dans ce répertoire?","o","n"));

				} while (my::get_yn("Voulez-vous changer de répertoire?","o","n"));

				if(my::get_yn("Voulez-vous sauvegarder? (Si vous ne sauvegardez pas, les configurations seront perdues)","o","n")){
					year.save();
					std::cout<<"Pour recharger ces classes, exécutez ce programme avec la commande:"<<std::endl<<std::endl<<
						"./jdgrades -path "<<io_path<<std::endl<<std::endl;
				} else { std::cout<<"Aucune configuration sauvegardée"<<std::endl; }
			}
		}
	} else {
		std::cout<<
			"Ce programme est déjà en cours d'exécution. Pour éviter de corrompre"<<std::endl<<
			"des fichiers, cette instance doit arrêter. (Tapez 'Enter' pour quitter)."<<std::endl;
		getchar();
	}
	std::cerr<<"STOP: "<<get_time()<<std::endl;
	return 0;
}
