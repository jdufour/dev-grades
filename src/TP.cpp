#include "TP.hpp"
#include "Year.hpp" //could be removed when Year::FIRST_SEMESTER isn't used

/*{TP*/
const constexpr unsigned int TP::TYPE;
const constexpr char* TP::NAME;

TP::TP(Date date){
	title_ = "TP";
	date_ = date;
	weight_ = 1;
	n_criteria_ = 7;
	average_points_.set(n_criteria_,0.0);
}

TP::TP(IOFiles& r):
	Evaluation(r)
{
	unsigned int tmp;
	for(unsigned int c(0);c<4;c++){
		r>>tmp;
		for(unsigned int j(0);j<tmp;j++){
			all_indicators_[c].push_back(
					std::make_shared<TP::Indicator>(
					std::make_shared<uistring>(j,r.read<std::string>())
					)
				);
		}
	}
	r>>tmp;
	for(unsigned int i(0);i<tmp;i++){
		add_session(std::make_shared<TP::Session>(this,r));
	}
}

void TP::save(IOFiles& w) const {
	Evaluation::save(w);
	for(unsigned int i(0);i<4;i++){
		w<<(unsigned int)(all_indicators_[i].size());
		for(auto const& ind:all_indicators_[i]){ w<<ind->get_text(); }
	}
	w<<(unsigned int)(sessions_.size());
	for(auto const& s:sessions_){ s->save(w); }
}

unsigned int TP::set_indicators(std::string const& filename){
	if(filename.find(".txt") == std::string::npos){
		std::cerr<<__PRETTY_FUNCTION__<<": l'extension du fichier doit être '.txt'"<<std::endl;
		return 1;
	}
	std::ifstream file(filename, std::ios::in);
	if(!file.is_open() || file.bad()){
		std::cerr<<__PRETTY_FUNCTION__<<": le fichier '"<<filename<<"' n'a pas pu être ouvert"<<std::endl;
		return 2;
	}
	std::string tmps;
	unsigned int tmpi;
	unsigned int i(0);
	while(std::getline(file,tmps)){
		if(tmps.size()){
			std::vector<std::string> tmp(my::string_split(tmps,'|'));
			if(tmp.size()==2){
				my::string2type(tmp[0],tmpi);
				if(tmpi>0 && tmpi<5){ all_indicators_[tmpi-1].push_back(
						std::make_shared<TP::Indicator>(
							std::make_shared<uistring>(i++,tmp[1])
							)
						);
				} else {
					std::cerr<<__PRETTY_FUNCTION__<<": le critère "<<tmpi<<" n'existe pas"<<std::endl;
					file.close();
					return 3;
				}
			} else {
				std::cerr<<__PRETTY_FUNCTION__<<": le fichier contenant la liste des critères n'a pas le bon format"<<std::endl;
				std::cerr<<__PRETTY_FUNCTION__<<": l'entrée '"<<tmps<<"' n'as pas le bon format"<<std::endl;
				file.close();
				return 4;
			}
		}
	}
	file.close();
	return 0;
}

std::vector<std::string> TP::list_evaluation_criteria() const {
	return {"Crit 1", "Crit 2", "Crit 3", "Crit 4"};
}

std::vector<std::string> TP::list_sessions() const{
	std::vector<std::string> tmp;
	for(auto const& s:sessions_){ tmp.push_back(s->get_title(true,false)); }
	return tmp;
}

void TP::compute_evaluation_stats(){
	nfails_ = 0;
	nrecorded_ = 0;
	class_average_ = 0.0;
	average_points_.set(n_criteria_,0);
	grades_for_histogram_.clear();
	for(auto const& g:evaluation_results_){
		std::shared_ptr<TP::Grade> grade_ptr(std::dynamic_pointer_cast<TP::Grade>(g));
		if(grade_ptr->is_recorded()){
			for(unsigned int i(0);i<n_criteria_;i++){ 
				average_points_(i) += grade_ptr->get_point(i);
			}
			grades_for_histogram_.push_back(grade_ptr->get_grade());
			class_average_ += grade_ptr->get_grade();
			if(grade_ptr->get_grade()<4){ nfails_++; }
			nrecorded_++;
		}
	}
	if(nrecorded_){
		class_average_ /= nrecorded_;
		average_points_ /= nrecorded_;
	}
}

void TP::summary(Latex& latex, std::string const& class_id) const {
	std::string tmp("l|");
	for(unsigned int i(0);i<n_criteria_;i++){ tmp += "|S[table-format=1.3]"; }
	tmp += "||S[table-format=1.3]";

	latex.subsection(true,class_id + ": " + title_);
	latex.begin_center();
	latex.begin_tabular(tmp);
	latex+="\\toprule";
	latex+="Prénom / Nom & \\multicolumn{" + my::tostring(n_criteria_-1) + "}{c|}{Critères} & {Rapport} & {Notes} \\\\\\midrule";
	bool gray_line(false);
	for(auto const& e:evaluation_results_){
		if(gray_line){ latex.command("rowcolor{gray!30}"); }
		gray_line = !gray_line;
		latex += std::dynamic_pointer_cast<TP::Grade>(e)->summary();
	}
	latex+="\\midrule";
	tmp = "Moyenne & ";
	for(unsigned int i(0);i<n_criteria_;i++){
		tmp += my::tostring(average_points_(i),3) + " & ";
	}
	latex+= tmp  + my::tostring(class_average_,3) +"\\\\\\bottomrule";
	latex.end_tabular();
	latex.end_center();

	latex.begin_center();
	latex.begin_tikzpicture();
	latex.begin_axis("ybar,ymin=0, xmin=0.75, xmax=6.25");
	latex.histogram(grades_for_histogram_,11,0.75,6.25);
	latex.end_axis();
	latex.end_tikzpicture();
	latex.end_center();
}

void TP::student_feedback(Latex& latex, std::shared_ptr<GenericGrade> const& grade) const {
	latex.command("mynewpage");
	std::shared_ptr<TP::Grade> grade_ptr(std::dynamic_pointer_cast<TP::Grade>(grade));
	latex.section(true,grade_ptr->get_student_name(false));
	if(grade_ptr->get_warning_count(0) || grade_ptr->get_warning_count(1)){
		latex.section(true,"Avertissements");
		for(unsigned int c(0);c<2;c++){
			if(grade_ptr->get_warning_count(c)){
				latex.subsection(true,"Critère " + my::tostring(c+5));
				latex.begin_itemize();
				for(auto const& w:grade_ptr->get_warnings()){
					if(w.get_criteria()==c){
						latex.item(w.get_date().ddmmyyyy() + ": " + w.get_comment());
					}
				}
				latex.end_itemize();
			}
		}
	}

	for(auto const& r:grade_ptr->get_reports()){ r->feedback(latex,false); }
}

bool TP::remove_grade_for_student(std::shared_ptr<Student> const& student){
	std::shared_ptr<TP::Report> report_ptr;
	for(auto const& s:sessions_){
		report_ptr = s->get_report_ptr(student);
		if(report_ptr){ s->remove_report(report_ptr); }
	}
	return Evaluation::remove_grade_for_student(student);
}

std::vector<std::string> TP::list_indicators(unsigned int const& criteria) const {
	std::vector<std::string> tmp;
	for(auto const& ind:all_indicators_[criteria]){ tmp.push_back(my::tostring(ind->get_idx()) + ". " + ind->get_text()); }
	return tmp;
}

std::vector<std::shared_ptr<TP::Indicator> > const& TP::get_indicators(unsigned int const& criteria) const {
	return all_indicators_[criteria];
}

std::shared_ptr<TP::Indicator> TP::get_indicator_ptr_matching(unsigned int const& criteria, std::string const& text) const{
	for(auto const& ind:all_indicators_[criteria]){ if(ind->get_text() == text){ return ind; } }
	std::cerr<<__PRETTY_FUNCTION__<<": the indicator '"<<text<<"' wasn't found"<<std::endl;
	return NULL;
}

std::string const& TP::get_indicator_text(unsigned int const& criteria, unsigned int const& idx) const {
	return all_indicators_[criteria][idx]->get_text();
}

std::shared_ptr<TP::Session> TP::get_session_byuuid(std::string const& uuid) const {
	for(auto const& s:sessions_){ if(s->get_uuid() == uuid){ return s; } }
	return NULL;
}

bool TP::add_session(std::shared_ptr<TP::Session> const& session_ptr){
	for(auto const& s:sessions_){
		if(s->get_title(true,true) == session_ptr->get_title(true,true)){
			std::cerr<<__PRETTY_FUNCTION__<<": cannot add two sessions with the same date, title and group"<<std::endl;
			return false;
		}
	}
	unsigned int idx(sessions_.size());
	for(unsigned int i(0);i<idx;i++){
		if(sessions_[i]->get_date() > session_ptr->get_date()){ idx = i; }
	}
	sessions_.insert(sessions_.begin()+idx,session_ptr);
	return true;
}

bool TP::remove_session(std::shared_ptr<TP::Session> const& session_ptr){
	for(auto const& r:session_ptr->get_reports()){
		for(auto const& g:evaluation_results_){
			if(std::dynamic_pointer_cast<TP::Grade>(g)->remove_report(r)){ break; }
		}
	}

	for(unsigned int i(0);i<sessions_.size();i++){
		if(*sessions_[i] == *session_ptr){
			sessions_.erase(sessions_.begin()+i);
			return true;
		}
	}
	std::cerr<<__PRETTY_FUNCTION__<<": could not remove the session from TP"<<std::endl;
	return false;
}

bool TP::add_indicator(unsigned int const& criteria, unsigned int const& idx, std::string const& text){
	unsigned int size(all_indicators_[criteria].size());
	if(idx<size+1){
		std::shared_ptr<TP::Indicator> indicator_ptr(std::make_shared<TP::Indicator>(std::make_shared<uistring>(idx,text)));
		for(auto const& ind:all_indicators_[criteria]){
			if(ind->get_text() == text){
				std::cerr<<__PRETTY_FUNCTION__<<": cannot have two identical indicators: "<<text<<std::endl;
				return false;
			}
		}

		for(auto const& s:sessions_){ s->add_indicator(criteria,indicator_ptr,0.0); }
		all_indicators_[criteria].insert(all_indicators_[criteria].begin()+idx,indicator_ptr);
		for(unsigned int i(idx+1);i<size+1;i++){ all_indicators_[criteria][i]->set_idx(i); }
	}
	return true;
}

bool TP::edit_indicator(unsigned int const& criteria, unsigned int const& idx, unsigned int const& newidx, std::string const& text){
	for(unsigned int i(0);i<all_indicators_[criteria].size();i++){
		if(i != idx && all_indicators_[criteria][i]->get_text() == text){
			std::cerr<<__PRETTY_FUNCTION__<<": cannot have two identical indicators: "<<text<<std::endl;
			return false;
		}
	}
	all_indicators_[criteria][idx]->set_text(text);
	if(idx != newidx){
		/*TODO:(current) test indicator reordering (seems ok)*/
		if(newidx > idx){
			for(unsigned int i(idx+1);i<newidx+1;i++){ all_indicators_[criteria][i]->set_idx(i-1); }
		} else {
			for(unsigned int i(newidx);i<idx;i++){ all_indicators_[criteria][i]->set_idx(i+1); }
		}
		all_indicators_[criteria][idx]->set_idx(newidx);
		std::sort(all_indicators_[criteria].begin(),all_indicators_[criteria].end(),sort_indicator_ptr);

		std::shared_ptr<TP::Report::Criteria> criteria_ptr;
		for(auto const& g:evaluation_results_){
			for(auto const& r:std::dynamic_pointer_cast<TP::Grade>(g)->get_reports()){
				criteria_ptr = r->get(criteria);
				if(criteria_ptr){ criteria_ptr->sort_indicators(); }
			}
		}
		for(auto const& s:sessions_){ s->sort_indicators(criteria); }
	}
	return true;
}

bool TP::remove_indicator(unsigned int const& criteria, unsigned int const& idx){
	for(auto const& s:sessions_){
		if(!s->remove_indicator(criteria,all_indicators_[criteria][idx])){
			std::cerr<<__PRETTY_FUNCTION__<<": cannot remove indicator the indicator"<<std::endl;
			std::cerr<<__PRETTY_FUNCTION__<<": '"<<all_indicators_[criteria][idx]->get_text()<<"'"<<std::endl;
			std::cerr<<__PRETTY_FUNCTION__<<": from the session"<<std::endl;
			std::cerr<<__PRETTY_FUNCTION__<<": '"<<s->get_title(true,true)<<"'"<<std::endl;
			return false;
		}
	}
	all_indicators_[criteria].erase(all_indicators_[criteria].begin()+idx);
	for(unsigned int i(idx);i<all_indicators_[criteria].size();i++){
		all_indicators_[criteria][i]->set_idx(i);
	}
	return true;
}
/*}*/

/*{Session*/
TP::Session::Session(Date const& date, bool const& for_group_A, std::string const& title):
	uuid_(UUID()()),
	date_(date),
	for_group_A_(for_group_A),
	title_(title)
{}

TP::Session::Session(TP* tp, IOFiles& r):
	uuid_(r.read<std::string>()),
	date_(r),
	for_group_A_(r.read<bool>()),
	title_(r.read<std::string>())
{
	unsigned int tmp;
	std::shared_ptr<TP::Indicator> indicator_ptr;
	for(unsigned int c(0);c<4;c++){
		r>>selected_[c]>>thresholds_[c]>>tmp;
		for(unsigned int i(0);i<tmp;i++){
			/*needs to use this intermediate variable otherwise the jdbin file is
			 *not read in the proper order.*/
			indicator_ptr = tp->get_indicator_ptr_matching(c,r.read<std::string>());
			indicators_[c].push_back(
					std::make_shared<TP::Session::Indicator>(
						indicator_ptr,
						JDBIN_VERSION<1.5?1:r.read<unsigned int>()
						)
					);
		}
	}
	if(JDBIN_VERSION<1.5){
		for(unsigned int c(0);c<4;c++){
			if(selected_[c]){
				for(auto const& A:tp->get_indicators(c)){
					bool found(false);
					for(auto const& B:indicators_[c]){
						if(*A == *B){ found = true; }
					}
					if(!found){
						indicators_[c].push_back(std::make_shared<TP::Session::Indicator>(A,0));
					}
				}
			}
		}
	}
}

bool TP::Session::edit(std::shared_ptr<TP> const tp_ptr, bool const& for_group_A, Date const& date, std::string const& title){
	for(auto const& s:tp_ptr->get_sessions()){
		if(s->get_uuid() != uuid_ && s->get_title(false,false) == title && s->get_date() == date && s->for_group_A() == for_group_A){
			std::cerr<<__PRETTY_FUNCTION__<<": cannot add two sessions with the same date, title and group"<<std::endl;
			std::cerr<<__PRETTY_FUNCTION__<<": +uuid  "<<s->get_uuid()<<" <-> "<<uuid_ <<std::endl;
			std::cerr<<__PRETTY_FUNCTION__<<": +tile  "<<s->get_title(false,false)<<" <-> "<<title<<std::endl;
			std::cerr<<__PRETTY_FUNCTION__<<": +date  "<<s->get_date()<<" <-> "<<date<<std::endl;
			std::cerr<<__PRETTY_FUNCTION__<<": +group "<<s->for_group_A()<<" <-> "<<for_group_A<<std::endl;
			return false;
		}
	}
	date_ = date;
	title_ = title;
	for_group_A_ = for_group_A;
	return true;
}

void TP::Session::save(IOFiles& w) const {
	w<<uuid_<<date_<<for_group_A_<<title_;
	for(unsigned int c(0);c<4;c++){
		w<<selected_[c]<<thresholds_[c]<<(unsigned int)(indicators_[c].size());
		for(auto const& ind:indicators_[c]){ ind->save(w); }
	}
}

std::vector<std::shared_ptr<Student> > TP::Session::get_students() const {
	std::vector<std::shared_ptr<Student> > tmp;
	for(auto const& r:reports_){ tmp.push_back(r->get_student()); }
	return tmp;
}

std::vector<std::string> TP::Session::list_students() const {
	std::vector<std::string> tmp;
	for(auto const& r:reports_){ tmp.push_back(r->get_student()->get_name(true)); }
	return tmp;
}

std::shared_ptr<TP::Report> TP::Session::get_report_ptr(std::shared_ptr<Student> const& student) const {
	for(auto const& r:reports_){
		if(r->get_student().get() == student.get()){ return r; }
	}
	return NULL;
}

std::vector<std::shared_ptr<TP::Session::Indicator> > const& TP::Session::get_indicators(unsigned int const& criteria) const {
	return indicators_[criteria];
}

std::shared_ptr<TP::Session::Indicator> TP::Session::get_indicator_ptr_matching(unsigned int const& criteria, std::string const& text) const {
	if(selected_[criteria]){
		for(auto const& ind:indicators_[criteria]){
			if(ind->get_text() == text){ return ind; }
		}
		std::cerr<<__PRETTY_FUNCTION__<<": the indicator '"<<text<<"' is not set for the criteria "<<criteria<<" of this session"<<std::endl;
	} else { std::cerr<<__PRETTY_FUNCTION__<<": this criteria is not set for this session"<<std::endl; }
	return NULL;
}

void TP::Session::add(unsigned int const& criteria, std::vector<std::shared_ptr<TP::Session::Indicator> > const& indicators){
	if(!selected_[criteria]){
		selected_[criteria] = true;
		indicators_[criteria] = indicators;
		for(auto const& r:reports_){ r->add(criteria); }
	} else { std::cerr<<__PRETTY_FUNCTION__<<": the same criteria cannot be added twice to the same session"<<std::endl; }
}

void TP::Session::remove(unsigned int const& criteria){
	if(selected_[criteria]){
		for(auto const& r:reports_){ r->remove(criteria); }
		thresholds_[criteria] = 0;
		selected_[criteria] = false;
	} else { std::cerr<<__PRETTY_FUNCTION__<<": the criteria " + my::tostring(criteria) + " cannot be remove because it doesn't exist"<<std::endl; }
}

bool TP::Session::add_indicator(unsigned int const& criteria, std::shared_ptr<TP::Indicator> const& indicator_ptr, double const& half_weight){
	if(selected_[criteria]){
		unsigned int idx(0);
		for(auto const& ind:indicators_[criteria]){
			if(*ind == *indicator_ptr){
				std::cerr<<__PRETTY_FUNCTION__<<": cannot add twice the same indicator '"<<indicator_ptr->get_text()<<"'"<<std::endl;
				return false;
			}
			if(ind->get_idx() < indicator_ptr->get_idx()){ idx++; }
		}
		/*TODO:(current) check ordering*/
		indicators_[criteria].insert(
				indicators_[criteria].begin()+idx,
				std::make_shared<TP::Session::Indicator>(indicator_ptr,half_weight)
				);
		return true;
	}
	std::cerr<<__PRETTY_FUNCTION__<<": criteria not selected for this session"<<std::endl;
	return false;
}

bool TP::Session::remove_indicator(unsigned int const& criteria, std::shared_ptr<TP::Indicator> const& indicator_ptr){
	if(selected_[criteria]){
		unsigned int idx(0);
		for(auto const& ind:indicators_[criteria]){
			if(*ind == *indicator_ptr){
				for(auto const& r:reports_){ r->get(criteria)->remove_indicator(indicator_ptr); }
				indicators_[criteria].erase(indicators_[criteria].begin()+idx); 
				return true;
			}
			idx++;
		}
		std::cerr<<__PRETTY_FUNCTION__<<": the indicator '" + indicator_ptr->get_text() + "' cannot be remove"<<std::endl;
		return false;
	} 
	std::cerr<<__PRETTY_FUNCTION__<<": criteria not selected for this session"<<std::endl;
	return true;
}

bool TP::Session::set_indicator(unsigned int const& criteria, std::shared_ptr<TP::Indicator> const& indicator_ptr, double const& half_weight){
	if(selected_[criteria]){
		std::shared_ptr<TP::Report::Criteria> criteria_ptr;
		for(auto const& ind:indicators_[criteria]){
			if(*ind == *indicator_ptr){
				for(auto const& r:reports_){ 
					criteria_ptr = r->get(criteria);
					if(criteria_ptr){
						if(half_weight>0.0){
							criteria_ptr->add_indicator(std::make_shared<TP::Report::Criteria::Indicator>(ind,0)); 
						} else { 
							criteria_ptr->remove_indicator(indicator_ptr); 
						}
					}
				}
				ind->set_half_weight(0);
				return true;
			}
		}
		std::cerr<<__PRETTY_FUNCTION__<<": the indicator '" + indicator_ptr->get_text() + "' cannot be set"<<std::endl;
		return false;
	}
	return true;
}

void TP::Session::compute_grade() const {
	for(auto const& r:reports_){ r->compute_grade(); }
}

void TP::Session::sort_indicators(unsigned int const& criteria){
	std::sort(indicators_[criteria].begin(),indicators_[criteria].end(),sort_indicator_ptr);
}

std::string TP::Session::get_result_str(unsigned int const& criteria) const {
	std::string tmp("Crit "+my::tostring(criteria+1));
	unsigned v(0);
	unsigned int nv(0);
	std::shared_ptr<TP::Report::Criteria> criteria_ptr;
	for(auto const& r:reports_){
		criteria_ptr = r->get(criteria);
		if(criteria_ptr){
			if(criteria_ptr->is_validated()){ v++; }
			else { nv++; }
		}
	}
	return tmp + "\n" + my::tostring(v) + "/" + my::tostring(v+nv);
}

bool TP::Session::add_report(std::shared_ptr<TP::Report> const& report_ptr){
	for(auto const& r:reports_){
		if(*r == *report_ptr){
			std::cerr<<__PRETTY_FUNCTION__<<": cannot add report with identical uuid "<<report_ptr->get_uuid()<<std::endl;
			return false;
		}
	}
	reports_.push_back(report_ptr);
	return true;
}

bool TP::Session::remove_report(std::shared_ptr<TP::Report> const& report_ptr){
	unsigned int tmp(reports_.size());
	for(unsigned int i(0);i<tmp;i++){
		if(*reports_[i] == *report_ptr){
			reports_.erase(reports_.begin()+i);
			return true;
		}
	}
	std::cerr<<__PRETTY_FUNCTION__<<": cannot remove report with uuid "<<report_ptr->get_uuid()<<std::endl;
	return false;
}

unsigned int TP::Session::feedback() const {
	if(reports_[0]){
		Class const* class_ptr(reports_[0]->get_student()->get_class());
		std::string io_path(class_ptr->get_io_path());
		std::string title(class_ptr->get_id() + "-TP-session-" + date_.yyyymmdd('-'));

		Latex latex(io_path,title,"article");
		latex.begindocument(Latex::header);

		for(auto const& r:reports_){
			latex.section(true, r->get_student()->get_name(true));
			r->feedback(latex,true);
			latex.command("mynewpage");
		}

		if(latex.enddocument(true,true)){
			std::cerr<<__PRETTY_FUNCTION__<<": compiling the session for dated on the '"<<date_.yyyymmdd('.')<<"' failed"<<std::endl;
			return 1;
		}
	} else {
		std::cerr<<__PRETTY_FUNCTION__<<": There is no report to latex for the session "<<title_<<std::endl;
		return 2;
	}
	return 0;
}

int TP::Session::send_mail() const {
	if(reports_[0]){
		/*TODO:(middle) there is here a need to set set a pointer to Class in Evaluation */
		Class const* class_ptr(reports_[0]->get_student()->get_class());

		IOFiles mail_config(class_ptr->get_io_path()+"mails/mail.jdbin",false,false);
		if(mail_config.is_open()){
			std::string io_path(class_ptr->get_io_path()  + "mails/" + class_ptr->get_id() + "/TP/" + date_.yyyymmdd('-') + "/");

			Linux command;
			command.mkpath(io_path.c_str());

			MailCurlSMTP mail;
			mail.set_account(mail_config);
			mail.set_subject("Évaluation TP: " + date_.yyyymmdd('.'));
			std::string fname;
			unsigned int compilation_fails(0);
			unsigned int mail_fails(0);
			for(auto const& r:reports_){
				fname = "TP-rapport-" + date_.yyyymmdd('-') + "-" +r->get_student()->get_mail_name();
				if(r->feedback(io_path,fname)){ compilation_fails++; }
				else {
					mail.set_body(
							r->get_student()->greetings()
							+ r->write_mail_body()
							+ "Meilleures salutations,\n"
							+ mail.get_signature());
					mail.set_pdf(io_path + fname + ".pdf");
					mail.set_log(io_path + fname + ".log");
					mail.set_to(r->get_student()->get_email());
					if(!mail.send()){ mail_fails++; }
				}
			}
			if(compilation_fails){ std::cerr<<__PRETTY_FUNCTION__<<": "<<compilation_fails<<" compilations have failed"<<std::endl; }
			if(mail_fails){ std::cerr<<__PRETTY_FUNCTION__<<": "<<mail_fails<<" mail have failed"<<std::endl; }
			return compilation_fails+mail_fails*10000;
		}
		std::cerr<<__PRETTY_FUNCTION__<<": the mail is not configured"<<std::endl;
		return -1;
	}
	std::cerr<<__PRETTY_FUNCTION__<<": There is no report to send for the session '"<<title_<<"'"<<std::endl;
	return -2;
}
/*}*/

/*{Report*/
TP::Report::Report(std::shared_ptr<TP::Session> const& session_ptr, std::shared_ptr<TP::Grade> const& grade_ptr):
	uuid_(UUID()()),
	session_(session_ptr),
	grade_(grade_ptr)
{
	for(unsigned int c(0);c<4;c++){
		if(session_->is_selected(c)){
			criteria_[c] = std::make_shared<TP::Report::Criteria>(this,c);
		}
	}
}

TP::Report::Report(std::shared_ptr<TP::Session> const& session_ptr, std::shared_ptr<TP::Grade> const& grade_ptr, IOFiles& r):
	uuid_(r.read<std::string>()),
	session_(session_ptr),
	grade_(grade_ptr)
{
	for(unsigned int c(0);c<4;c++){
		if(r.read<bool>()){
			criteria_[c] = std::make_shared<TP::Report::Criteria>(this,r);
		}
	}
}

void TP::Report::save(IOFiles& w) const {
	w<<uuid_;
	for(unsigned int c(0);c<4;c++){
		if(criteria_[c]){
			w<<true;
			criteria_[c]->save(w);
		} else { w<<false; }
	}
}

void TP::Report::compute_grade() const {
	for(auto const& c:criteria_){ if(c){ c->compute_score(); } }
	grade_->compute_grade();
}

unsigned int TP::Report::get_n_criteria() const {
	unsigned int tmp(0);
	for(auto const& c:criteria_){ if(c){ tmp++; } }
	return tmp;
}

std::shared_ptr<TP::Report::Criteria> TP::Report::get(unsigned int const& criteria) const {
	assert(criteria<4);
	return criteria_[criteria];
}

std::shared_ptr<TP::Report::Criteria> TP::Report::get_by_idx(unsigned int const& idx) const {
	unsigned int i(0);
	for(auto const& c:criteria_){ if(c && i++==idx){ return c; } }
	return NULL;
}

std::shared_ptr<Student> TP::Report::get_student() const {
	return grade_->get_student();
}

unsigned int TP::Report::feedback(std::string const& io_path, std::string const& fname) const {
	Latex latex(io_path,fname,"article");
	latex.begindocument(Latex::header);
	latex.section(true,grade_->get_student()->get_name(true));
	feedback(latex,true);
	if(latex.enddocument(true,true)){
		std::cerr<<__PRETTY_FUNCTION__<<": compiling the report for '"<<grade_->get_student()->get_name(true)<<"' failed"<<std::endl;
		return 1;
	}
	return 0;
}

void TP::Report::feedback(Latex& latex, bool const& print_warnings) const {
	latex.subsection(true,session_->get_title(true,true) + (full_report_?" [" + my::tostring(grade_->get_point(6),2) + "/2]":""));
	for(unsigned int c(0);c<4;c++){
		if(criteria_[c]){ criteria_[c]->feedback(latex,full_report_); }
	}
	if(print_warnings){
		for(auto const& w:grade_->get_warnings(this)){
			latex.subsubsection(true,"Avertissement pour le critère " + my::tostring(w.get_criteria()+5) + ": " + w.get_comment());
		}
	}
}

std::string TP::Report::write_mail_body() const {
	return "Ceci est un mail automatique contenant le résultat du rapport de TP sur\n"
		"'" + session_->get_title(false,false) + "' effectué le '" + session_->get_date().ddmmyyyy() + "'\n\n";
}

int TP::Report::send_mail() const {
	Class const* class_ptr(grade_->get_student()->get_class());
	std::string io_path(class_ptr->get_io_path()  + "mails/" + class_ptr->get_id() + "/TP/" + session_->get_date().yyyymmdd('-') + "/");

	Linux command;
	command.mkpath(io_path.c_str());

	IOFiles mail_config(class_ptr->get_io_path()+"mails/mail.jdbin",false,false);
	if(mail_config.is_open()){
		std::string fname("TP-rapport-" + session_->get_date().yyyymmdd('-') + "-" + grade_->get_student()->get_mail_name());
		if(feedback(io_path,fname)){
			std::cerr<<__PRETTY_FUNCTION__<<": the pdf did not compile"<<std::endl;
			return 1;
		} else {
			MailCurlSMTP mail;
			mail.set_account(mail_config);
			mail.set_subject("Évaluation TP: " + session_->get_date().yyyymmdd('.'));
			mail.set_body(
					grade_->get_student()->greetings()
					+ write_mail_body()
					+ "Meilleures salutations,\n"
					+ mail.get_signature());
			mail.set_pdf(io_path + fname + ".pdf");
			mail.set_log(io_path + fname + ".log");
			mail.set_to(grade_->get_student()->get_email());
			return mail.send()?0:2;
		}
	}
	std::cerr<<__PRETTY_FUNCTION__<<": the mail is not configured"<<std::endl;
	return -1;
}
/*}*/

/*{Criteria*/
TP::Report::Criteria::Criteria(TP::Report* report_ptr, unsigned int const& criteria):
	report_(report_ptr),
	criteria_(criteria),
	comment_("")
{
	for(auto const& ind:report_ptr->get_session()->get_indicators(criteria_)){
		if(ind->get_half_weight()>0){
			indicators_.push_back(
					std::make_shared<TP::Report::Criteria::Indicator>(ind, 0)
					);
		}
	}
}

TP::Report::Criteria::Criteria(TP::Report* report_ptr, IOFiles& r):
	report_(report_ptr),
	criteria_(r.read<unsigned int>()),
#if defined(UPDATE_FROM_V1_1) or defined(UPDATE_FROM_V1_2) 
	n_good_(r.read<unsigned int>()),
	validated_(r.read<bool>()),
#endif
	comment_(r.read<std::string>())
{
	unsigned int tmp(r.read<unsigned int>());
	std::shared_ptr<TP::Session::Indicator> indicator_ptr;
	for(unsigned int i(0);i<tmp;i++){
		/*needs to use this intermediate variable otherwise the jdbin file is
		 *not read in the proper order.*/
		indicator_ptr = report_ptr->get_session()->get_indicator_ptr_matching(criteria_,r.read<std::string>());
		indicators_.push_back(
				std::make_shared<TP::Report::Criteria::Indicator>(
					indicator_ptr, r.read<unsigned int>()
					)
				);
	}
	compute_score();
}

void TP::Report::Criteria::save(IOFiles& w) const {
	w<<criteria_<<comment_<<(unsigned int)(indicators_.size());
	for(auto const& ind:indicators_){ ind->save(w); }
}

void TP::Report::Criteria::add_indicator(std::shared_ptr<TP::Report::Criteria::Indicator> const& indicator_ptr){
	unsigned int i(0);
	for(auto const& ind:indicators_){
		if(ind->get_idx() > indicator_ptr->get_idx()){ break; }
		else { i++; }
	}
	indicators_.insert(indicators_.begin()+i,indicator_ptr);
}

void TP::Report::Criteria::remove_indicator(std::shared_ptr<TP::Indicator> const& indicator_ptr){
	for(unsigned int i(0);i<indicators_.size();i++){
		if(*indicators_[i] == *indicator_ptr){
			indicators_.erase(indicators_.begin()+i);
			i = indicators_.size();
		}
	}
}

void TP::Report::Criteria::sort_indicators(){
	std::sort(indicators_.begin(),indicators_.end(),sort_indicator_ptr);
}

void TP::Report::Criteria::compute_score(){
	n_good_ = 0;
	n_limit_ = 0;
	n_bad_ = 0;
	score_ = 0.0;
	evaluated_ = false;
	validated_ = true;
	double total_weight(0.0);
	for(auto const& ind:indicators_){
		if(ind->get_mark()){
			total_weight += 2*ind->get_half_weight();
			score_ += ind->get_score();
			switch(ind->get_mark()){
				case 1:
					n_bad_++;
					validated_ = false;
					evaluated_ = true;
					break;
				case 2:
					n_limit_++;
					evaluated_ = true;
					break;
				case 3:
					n_good_++;
					evaluated_ = true;
					break;
			}
		}
	}
	validated_ = (evaluated_ && validated_)
		?(n_good_>=report_->get_threshold(criteria_))
		:false;
	if(total_weight){ score_ /= total_weight; }
	else { score_ = 0.0; }
}

void TP::Report::Criteria::feedback(Latex& latex, bool const& full_report) const {
	latex.subsubsection(true,"Critère " + my::tostring(criteria_+1) + ": " + get_result_str(true,full_report));
	if(evaluated_){
		if(!full_report){
			latex+="Conditions de validation:";
			latex.begin_itemize();
			latex.item("0: Insuffisant");
			latex.item(my::tostring(report_->get_threshold(criteria_))+": Bien");
			latex.end_itemize();
		}
		latex.begin_center();
		latex.begin_tabular("p{13cm}||ccc" + std::string(full_report?" || c" :""));
		latex+="\\toprule";
		latex+="Indicateurs & Insuf. & Limite & Bien " + std::string(full_report? " & Score " :"") +" \\\\\\midrule";
		std::string tmp;
		unsigned int c(0);
		for(auto const& ind:indicators_){
			switch(ind->get_mark()){
				case 1:
					tmp = ind->get_text() + "&X&&";
					break;
				case 2:
					tmp = ind->get_text() + "&&X&";
					break;
				case 3:
					tmp = ind->get_text() + "&&&X";
					break;
				default:
					tmp = "";
			}
			if(tmp != ""){
				if(full_report){
					tmp += "&" + my::tostring(ind->get_score()) + "/" + my::tostring(2*ind->get_half_weight());
				}
				if(++c%2){ latex.command("rowcolor{gray!30}"); }
				latex += tmp + "\\\\";
			}
		}
		latex+="\\bottomrule";
		latex.end_tabular();
		latex.end_center();
		if(comment_ != ""){
			latex.textbf("Commentaire(s): ");
			latex += comment_ ;
		}
	}
}

std::string TP::Report::Criteria::get_info() const {
	std::string tmp("Crit " + my::tostring(criteria_+1) + "\n");
	tmp += my::tostring(n_good_) + "/" +  my::tostring(report_->get_session()->get_threshold(criteria_)) +"\n";
	tmp += get_result_str(false,false);
	return tmp;
}

std::string TP::Report::Criteria::get_result_str(bool const& for_pdf, bool const& full_report) const {
	return full_report
		?"$\\SI{" + my::tostring(get_score()*100,2) + "}{\\percent} \\cdot \\SI{0.5}{pt} = \\SI{" + my::tostring(get_score()*0.5,2) + "}{pt}$"
		:(evaluated_?(validated_?"Validé":"Échoué"):(for_pdf?"Non évalué":"  NE"));
}
/*}*/

/*{Grade*/
TP::Grade::Grade(std::shared_ptr<Student> const& student_ptr, std::shared_ptr<Evaluation> const& eval_ptr):
	GenericGrade(student_ptr,eval_ptr,7)
{ compute_grade(); }

TP::Grade::Grade(std::shared_ptr<Student> const& student_ptr, std::shared_ptr<Evaluation> const& eval_ptr, IOFiles& r):
	GenericGrade(student_ptr,eval_ptr,r)
{}

/*Cannot add this in the constructior because Report contains a pointer to the Grade*/
void TP::Grade::load_reports(std::shared_ptr<TP::Grade> const& grade_ptr, IOFiles& r){
	unsigned int size(r.read<unsigned int>());
	std::shared_ptr<TP> tp_ptr(std::dynamic_pointer_cast<TP>(eval_));
	std::shared_ptr<TP::Session> session_ptr;
	std::string uuid;
	for(unsigned int i(0);i<size;i++){
		r>>uuid;
		session_ptr = tp_ptr->get_session_byuuid(uuid);
		if(session_ptr){ add_report(std::make_shared<TP::Report>(session_ptr, grade_ptr, r)); }
		else { std::cerr<<__PRETTY_FUNCTION__<<": no session with uuid "<<uuid<<std::endl; }
	}
	r>>warning_count_[0]>>warning_count_[1];
	size = warning_count_[0]+warning_count_[1];
	for(unsigned int i(0);i<size;i++){
		warnings_.push_back(Warning(tp_ptr->get_session_byuuid(r.read<std::string>()),r));
	}
	compute_grade();
}

void TP::Grade::save(IOFiles& w) const {
	GenericGrade::save(w);
	w<<(unsigned int)(reports_.size());
	for(auto const& r:reports_){
		w<<r->get_session()->get_uuid();
		r->save(w);
	}
	w<<warning_count_[0]<<warning_count_[1];
	for(auto const& warning:warnings_){ warning.save(w); }
}

bool TP::Grade::add_report(std::shared_ptr<TP::Report> const& report_ptr){
	for(auto const& r:reports_){
		if(*r->get_session() == *report_ptr->get_session()){
			std::cerr<<__PRETTY_FUNCTION__<<": cannot add two reports on the same session"<<std::endl;
			return false;
		}
		if(*r == *report_ptr){
			std::cerr<<__PRETTY_FUNCTION__<<": cannot add twice the same rapport to a grade"<<std::endl;
			return false;
		}
	}
	if(report_ptr->get_session()->add_report(report_ptr)){
		unsigned int tmp(reports_.size());
		if(tmp){
			for(unsigned int i(0);i<tmp;i++){
				if(reports_[i]->get_session()->get_date() > report_ptr->get_session()->get_date()){ tmp = i; }
			}
		}
		reports_.insert(reports_.begin()+tmp,report_ptr);
		return true;
	}
	return false;
}

bool TP::Grade::remove_report(std::shared_ptr<TP::Report> const& report_ptr){
	for(unsigned int i(0);i<reports_.size();i++){
		if(reports_[i].get() == report_ptr.get()){
			reports_.erase(reports_.begin()+i);
			return true;
		}
	}
	std::cerr<<__PRETTY_FUNCTION__<<": cannot remove the report with uuid '"<<report_ptr->get_uuid()<<"' from grade"<<std::endl;
	return false;
}

void TP::Grade::compute_grade(){
	points_.set(7,0.0);
	unsigned int n_validated_criteria(0);
	for(unsigned int w(0);w<2;w++){
		if(warning_count_[w]<3){
			points_(4+w) = 0.5;
			n_validated_criteria++;
		}
	}
	std::shared_ptr<TP::Report::Criteria> criteria_ptr;
	for(auto const& r:reports_){
		if(n_validated_criteria < 6){
			for(unsigned int c(0);c<4;c++){
				if(!points_(c)){
					criteria_ptr = r->get(c);
					if(criteria_ptr && criteria_ptr->is_validated()){
						points_(c) = 0.5;
						n_validated_criteria++;
					}
				}
			}
		} else {
			bool has_all_criteria(true);
			for(unsigned int c(0);c<4;c++){
				if(!r->get(c)){ has_all_criteria = false; }
			}
			r->set_full_report(has_all_criteria);
			if(has_all_criteria){
				double score(0);
				for(unsigned int c(0);c<4;c++){
					score += 0.5*r->get(c)->get_score();
				}
				points_(6) = std::max(points_(6),score);
			}
		}
	}

	if(Year::FIRST_SEMESTER){
		grade_without_rounding_ = 0;
		for(unsigned int i(0);i<6;i++){ grade_without_rounding_ += points_(i); }
		grade_without_rounding_ = grade_without_rounding_*5.0/3.0+1;
	} else {
		grade_without_rounding_ = 1;
		for(unsigned int i(0);i<6;i++){ grade_without_rounding_ += points_(i); }
		if(grade_without_rounding_>=4){ grade_without_rounding_ += points_(6); }
	}
	grade_ = my::round_nearest(grade_without_rounding_,2);
}

std::vector<std::string> TP::Grade::list_reports() const {
	std::vector<std::string> tmp;
	for(auto const& r:reports_){
		tmp.push_back(r->get_session()->get_title(false,true));
	}
	return tmp;
}

std::vector<std::string> TP::Grade::list_sessions() const {
	std::vector<std::string> tmp;
	for(auto const& r:reports_){ tmp.push_back(r->get_session()->get_title(true,true)); }
	return tmp;
}

void TP::Grade::add_warning(std::shared_ptr<Session> const& session_ptr, unsigned int const& criteria, std::string const& comment){
	unsigned int tmp(warnings_.size());
	if(tmp){
		for(unsigned int i(0);i<tmp;i++){
			if( warnings_[i].get_criteria() > criteria || warnings_[i].get_date() > session_ptr->get_date()){ tmp = i; }
		}
	}
	warnings_.insert(warnings_.begin()+tmp,Warning(session_ptr,criteria,comment));
	warning_count_[criteria]++;
}

void TP::Grade::edit_warning(unsigned int const& w, std::shared_ptr<Session> const& session_ptr, unsigned int const& criteria, std::string const& comment){
	remove_warning(w);
	add_warning(session_ptr,criteria,comment);
}

void TP::Grade::remove_warning(unsigned int const& w){
	if(w<warnings_.size()){
		warning_count_[warnings_[w].get_criteria()]--;
		warnings_.erase(warnings_.begin()+w);
	}
}

std::vector<TP::Grade::Warning> TP::Grade::get_warnings(Report const* const r) const {
	return get_warnings(r->get_session().get());
}

std::vector<TP::Grade::Warning> TP::Grade::get_warnings(Session const* const s) const {
	std::vector<Warning> warnings;
	for(auto const& w:warnings_){
		if(*s == *w.get_session()){ warnings.push_back(w); }
	}
	return warnings;
}

std::vector<std::string> TP::Grade::list_warnings() const {
	std::vector<std::string> tmp;
	for(auto const& w:warnings_){
		tmp.push_back( "Critère" + std::string(w.get_criteria()?" 6: (":" 5: (") + w.get_date().ddmmyyyy() + ") " + w.get_comment());
	}
	return tmp;
}

std::string TP::Grade::get_warning_status() const {
	std::string tmp("Validé: ");
	tmp += my::tostring(2*(points_(0)+points_(1)+points_(2)+points_(3)));
	tmp += "/4    Avertissement 5: "+ my::tostring(warning_count_[0]);
	tmp += "/3    Avertissement 6: "+ my::tostring(warning_count_[1]);
	tmp += "/3    Rapport final "   + my::tostring(points_(6),2);
	return tmp;
}

std::string TP::Grade::get_result_info() const {
	unsigned int tmp(0);
	for(unsigned int i(0);i<6;i++){ tmp += points_(i); }
	std::string stmp("Critères obligatoires: " + my::tostring(tmp) + "/6");
	if(points_(6)){ stmp += "\\\\ &&&&& Rapport complet: " + my::tostring(points_(6)) + "/4"; }
	return stmp;
}

std::string TP::Grade::summary() const {
	std::string tmp(student_->get_name(false) + " & ");
	if(recorded_){
		for(unsigned int i(0);i<points_.size();i++){
			tmp += my::tostring(points_(i),i<6?1:3) + " &";
		}
	} else { tmp += std::string(points_.size(),'&'); }
	return tmp + my::tostring(grade_) + "\\\\";
}
/*}*/

/*{Warning*/
TP::Grade::Warning::Warning(std::shared_ptr<Session> const& session_ptr, unsigned int const& criteria, std::string const& comment):
	session_(session_ptr),
	criteria_(criteria),
	comment_(comment)
{}

TP::Grade::Warning::Warning(std::shared_ptr<Session> const& session_ptr, IOFiles& r):
	session_(session_ptr),
	criteria_(r.read<unsigned int>()),
	comment_(r.read<std::string>())
{}

void TP::Grade::Warning::save(IOFiles& w) const { w<<session_->get_uuid()<<criteria_<<comment_; }

void TP::Grade::Warning::set(std::shared_ptr<Session> const& session_ptr, unsigned int const& criteria, std::string const& comment){
	session_ = session_ptr;
	criteria_ = criteria;
	comment_ = comment;
}
/*}*/
