#ifndef DEF_STUDENT
#define DEF_STUDENT

#include "GenericGrade.hpp"
#include "Rand.hpp"

class Class;
class Student{
	public:
		Student(std::string const& firstname, std::string const& lastname, std::string const& email, bool const& group_A, bool const& is_man);
		Student(IOFiles& r);
		virtual ~Student() = default;
		/*{Forbidden*/
		Student() = delete;
		Student(Student&&) = delete;
		Student(Student const&) = delete;
		Student& operator=(Student&) = delete;
		/*}*/

		void save(IOFiles& w) const;
		void push_back(std::shared_ptr<GenericGrade> const& grade);
		bool remove_grade_for_evaluation(std::shared_ptr<Evaluation> const& eval);
		void edit(std::string const& firstname, std::string const& lastname, std::string const& email, bool const& group_A, bool const& is_man);
		void compute_average();

		std::string const& get_uuid() const { return uuid_; }
		Class const* get_class() const { return class_ptr_; }
		void set_class(Class* class_ptr){ class_ptr_ = class_ptr; }

		std::string get_name(bool const& firstname_first) const { return firstname_first?(firstname_ + " " + lastname_):(lastname_ + " " + firstname_); }
		std::string get_mail_name() const { return my::replace_in_string(email_.substr(0,email_.find('@')),'.','-'); }
		std::string const& get_lastname() const { return lastname_; }
		std::string const& get_firstname() const { return firstname_; }
		std::string const& get_email() const { return email_; }
		bool const& in_group_A() const { return in_group_A_; }
		bool const& is_man() const { return is_man_; }
		double get_average(bool consider_rounding) const;
		double const& get_average_raw() const { return average_raw_; }
		double const& get_average_grades_without_rounding() const { return average_grades_without_rounding_; }
		std::string greetings() const { return (is_man_?"Cher ":"Chère ") + firstname_  + ",\n\n"; }
		bool is_rounding_unfavorable(){ return average_ < my::round_nearest(average_grades_without_rounding_,2); }

		std::shared_ptr<GenericGrade> get_grade(std::shared_ptr<Evaluation> const& eval_ptr) const;
		std::vector<std::shared_ptr<GenericGrade> > const& get_grades() const { return personal_grades_; }
		unsigned int get_ngrades() const { return personal_grades_.size(); }

		bool operator==(Student const& s) const { return uuid_ == s.uuid_; }

	private:
		Class* class_ptr_;
		std::string const uuid_;
		std::string firstname_;
		std::string lastname_;
		std::string email_;
		bool in_group_A_;
		bool is_man_;

		std::vector<std::shared_ptr<GenericGrade> > personal_grades_;
		double average_                         = 0.0; //!< final average (the one that enters the report)
		double average_raw_                     = 0.0; //!< final average without rounding
		double average_grades_without_rounding_ = 0.0;//!< final average without rounding based on the non-rounded grades
};
#endif
