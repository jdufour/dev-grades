#ifndef DEF_TP
#define DEF_TP

#include "Evaluation.hpp"

class TP: public Evaluation{
	public:
		TP(Date date);
		TP(IOFiles& r);
		~TP() = default;
		/*{Forbidden*/
		TP() = delete;
		TP(TP&&) = delete;
		TP(TP const&) = delete;
		TP& operator=(TP const&) = delete;
		/*}*/
		class Indicator;
		class Session;
		class Report;
		class Grade;

		void set_weight(double const& weight){ weight_ = weight; }

		void compute_evaluation_stats() override;
		void save(IOFiles& w) const override;
		void summary(Latex& latex, std::string const& class_id) const override;
		void student_feedback(Latex& latex, std::shared_ptr<GenericGrade> const& grade) const override;
		bool remove_grade_for_student(std::shared_ptr<Student> const& student) override;

		double get_max_criteria_to_fill(unsigned int const& i) const override { return max_criterion_[i]; }
		unsigned int get_n_criteria_to_fill() const override { return 0; }
		std::vector<std::string> list_evaluation_criteria() const override;
		std::vector<std::string> list_indicators(unsigned int const& criteria) const;
		std::vector<std::string> list_sessions() const;

		unsigned int get_n_sessions() const { return sessions_.size(); }
		std::vector<std::shared_ptr<TP::Session> > const& get_sessions() const { return sessions_; }
		std::shared_ptr<TP::Session> get_session_byuuid(std::string const& uuid) const;
		std::shared_ptr<TP::Session> get_session(unsigned int const& s) const { return sessions_[s]; }
		bool add_session(std::shared_ptr<TP::Session> const& session_ptr);
		bool remove_session(std::shared_ptr<TP::Session> const& session_ptr);

		unsigned int set_indicators(std::string const& filename);
		std::vector<std::shared_ptr<TP::Indicator> > const& get_indicators(unsigned int const& criteria) const;
		std::shared_ptr<TP::Indicator> get_indicator_ptr_matching(unsigned int const& criteria, std::string const& text) const;
		std::string const& get_indicator_text(unsigned int const& criteria, unsigned int const& idx) const;

		bool add_indicator(unsigned int const& criteria, unsigned int const& idx, std::string const& text);
		bool edit_indicator(unsigned int const& criteria, unsigned int const& idx, unsigned int const& newidx, std::string const& text);
		bool remove_indicator(unsigned int const& criteria, unsigned int const& idx);

		unsigned int const& get_type() const override { return TYPE; }
		static const constexpr unsigned int TYPE = 2;
		static const constexpr char* NAME = "TP\n(critérié)";

	private:
		unsigned int max_criterion_[7] = {1,1,1,1,1,1,4};
		std::vector<std::shared_ptr<TP::Indicator> > all_indicators_[4];
		std::vector<std::shared_ptr<TP::Session> > sessions_;
		Vector<double> average_points_;
};

class TP::Session{
	public:
		Session(Date const& date, bool const& for_group_A, std::string const& title);
		Session(TP* tp, IOFiles& r);
		~Session() = default;
		bool operator==(Session const& s) const { return uuid_ == s.uuid_; }
		/*{Forbidden*/
		Session() = delete;
		Session(Session&&) = delete;
		Session(Session const&) = delete;
		Session& operator=(Session const&) = delete;
		/*}*/
		class Indicator;

		bool edit(std::shared_ptr<TP> const tp_ptr, bool const& for_group_A, Date const& date, std::string const& title);

		void save(IOFiles& w) const;
		unsigned int feedback() const;
		int send_mail() const;
		void compute_grade() const;

		std::string get_result_str(unsigned int const& criteria) const;
		std::string const& get_uuid() const { return uuid_; }
		bool const& for_group_A() const { return for_group_A_; }
		Date const& get_date() const { return date_; }
		std::string get_title(bool const& withgroup, bool const& withdate) const
		{ return (withdate?date_.ddmmyyyy() + ": ":"") + (withgroup?(for_group_A_?"(A) ":"(B) "):"") + title_; }

		std::vector<std::shared_ptr<Student> > get_students() const;
		std::vector<std::string> list_students() const;

		/*handle criteria*/
		bool const& is_selected(unsigned int const& criteria) const { return selected_[criteria]; }
		void add(unsigned int const& criteria, std::vector<std::shared_ptr<TP::Session::Indicator> > const& indicators);
		void remove(unsigned int const& criteria);

		unsigned int const& get_threshold(unsigned int const& criteria) const { return thresholds_[criteria]; }
		void set_threshold(unsigned int const& criteria, unsigned int const& threshold){
			/*if threshold = 0, criteria is not evaluated*/
			thresholds_[criteria] = threshold;
		}

		/*handle indicator for given criteria*/
		bool add_indicator(unsigned int const& criteria, std::shared_ptr<TP::Indicator> const& indicator_ptr, double const& weight);
		bool remove_indicator(unsigned int const& criteria, std::shared_ptr<TP::Indicator> const& indicator_ptr);
		bool set_indicator(unsigned int const& criteria, std::shared_ptr<TP::Indicator> const& indicator_ptr, double const& weight);
		void sort_indicators(unsigned int const& criteria);

		std::vector<std::shared_ptr<TP::Session::Indicator> > const& get_indicators(unsigned int const& criteria) const;
		std::shared_ptr<TP::Session::Indicator> get_indicator_ptr_matching(unsigned int const& criteria, std::string const& text) const;

		/*handle report*/
		bool add_report(std::shared_ptr<TP::Report> const& report_ptr);
		bool remove_report(std::shared_ptr<TP::Report> const& report_ptr);

		std::vector<std::shared_ptr<TP::Report> > const& get_reports() const { return reports_; }
		std::shared_ptr<TP::Report> get_report_ptr(std::shared_ptr<Student> const& student_ptr) const;

	private:
		std::string const uuid_;
		Date date_;
		bool for_group_A_;
		std::string title_;
		bool selected_[4] = {false,false,false,false};
		unsigned int thresholds_[4] = {0,0,0,0};
		std::vector<std::shared_ptr<TP::Session::Indicator> > indicators_[4];
		std::vector<std::shared_ptr<TP::Report> > reports_;
};

class TP::Report{
	public:
		Report(std::shared_ptr<TP::Session> const& session_ptr, std::shared_ptr<TP::Grade> const& grade_ptr);
		Report(std::shared_ptr<TP::Session> const& session_ptr, std::shared_ptr<TP::Grade> const& grade_ptr, IOFiles& r);
		~Report() = default;
		bool operator==(Report const& r) const { return uuid_ == r.uuid_; }
		/*{Forbidden*/
		Report() = delete;
		Report(Report&&) = delete;
		Report(Report const&) = delete;
		Report& operator=(Report const&) = delete;
		/*}*/
		class Criteria;

		std::string const& get_uuid() const { return uuid_; }

		void save(IOFiles& w) const;
		unsigned int feedback(std::string const& io_path, std::string const& fname) const;
		void feedback(Latex& latex, bool const& print_warnings) const;
		std::string write_mail_body() const;
		int send_mail() const;
		void compute_grade() const;

		void set_full_report(bool const& full_report){ full_report_ = full_report; }
		bool const& is_full_report() const { return full_report_; }

		unsigned int get_n_criteria() const;
		void add(unsigned int const& criteria){ assert(criteria<4); criteria_[criteria] = std::make_shared<TP::Report::Criteria>(this,criteria); }
		void remove(unsigned int const& criteria){ assert(criteria<4); criteria_[criteria] = NULL; }
		std::shared_ptr<TP::Report::Criteria> get(unsigned int const& criteria) const;
		std::shared_ptr<TP::Report::Criteria> get_by_idx(unsigned int const& idx) const;

		unsigned int const& get_threshold(unsigned int const& criteria) const { return session_->get_threshold(criteria); }
		std::shared_ptr<TP::Session> get_session() const { return session_; }
		std::shared_ptr<Student> get_student() const;

	private:
		std::string const uuid_;
		std::shared_ptr<TP::Session> const session_;
		std::shared_ptr<TP::Grade> const grade_;
		std::shared_ptr<TP::Report::Criteria> criteria_[4] = {NULL,NULL,NULL,NULL};
		bool full_report_ = false;
};

class TP::Report::Criteria{
	public:
		Criteria(TP::Report* report_ptr, unsigned int const& criteria);
		Criteria(TP::Report* report_ptr, IOFiles& r);
		~Criteria() = default;
		/*{Forbidden*/
		Criteria() = delete;
		Criteria(Criteria&&) = delete;
		Criteria(Criteria const&) = delete;
		Criteria& operator=(Criteria const&) = delete;
		/*}*/
		class Indicator;

		void save(IOFiles& w) const;
		void feedback(Latex& latex, bool const& full_report) const;
		void compute_score();
		double get_score() const { return score_; }

		unsigned int const& get_criteria() const { return criteria_; }

		bool const& is_validated() const { return validated_; }
		std::string get_result_str(bool const& for_pdf, bool const& full_report) const;
		std::string get_info() const;

		void set_comment(std::string const& comment) { comment_ = comment; }
		std::string const& get_comment() const { return comment_; }

		std::shared_ptr<TP::Report::Criteria::Indicator> get_indicator(unsigned int const& i){ return indicators_[i]; }
		std::vector<std::shared_ptr<TP::Report::Criteria::Indicator> > const& get_indicators() const { return indicators_; }
		void set_indicators(std::vector<std::shared_ptr<TP::Report::Criteria::Indicator> > const& indicators){ indicators_ = indicators; }
		void add_indicator(std::shared_ptr<TP::Report::Criteria::Indicator> const& indicator_ptr);
		void remove_indicator(std::shared_ptr<TP::Indicator> const& indicator_ptr);
		void sort_indicators();

	private:
		TP::Report* const report_;
		unsigned int criteria_;
		unsigned int n_good_  = 0;
		unsigned int n_limit_ = 0;
		unsigned int n_bad_   = 0;
		double score_ = 0.0;
		bool evaluated_ = false;
		bool validated_ = false;
		std::vector<std::shared_ptr<TP::Report::Criteria::Indicator> > indicators_;
		std::string comment_;
};

class TP::Grade: public GenericGrade{
	public:
		Grade(std::shared_ptr<Student> const& student, std::shared_ptr<Evaluation> const& eval);
		Grade(std::shared_ptr<Student> const& student, std::shared_ptr<Evaluation> const& eval, IOFiles& r);
		virtual ~Grade() = default;
		/*{Forbidden*/
		Grade() = delete;
		Grade(Grade&&) = delete;
		Grade(Grade const&) = delete;
		Grade& operator=(Grade const&) = delete;
		/*}*/
		class Warning;

		void load_reports(std::shared_ptr<TP::Grade> const& grade_ptr, IOFiles& r);

		void compute_grade() override;
		void reset_grade(unsigned int const& n) override { /*TODO:(middle) what to do with this*/ (void)(n); }
		std::string get_fname() const override { return "TP-rapports-" + student_->get_mail_name(); }
		std::string get_result_info() const override;
		std::string print_grade() const override { return my::tostring(grade_,1); }
		std::string summary() const;

		void save(IOFiles& w) const override;

		bool add_report(std::shared_ptr<TP::Report> const& report_ptr);
		bool remove_report(std::shared_ptr<TP::Report> const& report_ptr);

		std::vector<std::shared_ptr<TP::Report> > get_reports() const { return reports_; }
		std::shared_ptr<TP::Report> get_report(unsigned int const& idx) const { return idx<reports_.size()?reports_[idx]:NULL; }
		std::vector<std::string> list_reports() const;
		std::vector<std::string> list_sessions() const;

		void add_warning(std::shared_ptr<Session> const& session_ptr, unsigned int const& criteria, std::string const& comment);
		void edit_warning(unsigned int const& w, std::shared_ptr<Session> const& session_ptr, unsigned int const& criteria, std::string const& comment);
		void remove_warning(unsigned int const& w);
		Warning const& get_warning(unsigned int const& i) const { return warnings_[i]; }
		std::vector<Warning> const& get_warnings() const { return warnings_; }
		std::vector<Warning> get_warnings(Session const* const s) const;
		std::vector<Warning> get_warnings(Report const* const r) const;
		std::vector<std::string> list_warnings() const;
		std::string get_warning_status() const;
		unsigned int const& get_warning_count(unsigned int const& criteria) const { return warning_count_[criteria]; }

	private:
		std::vector<std::shared_ptr<TP::Report> > reports_;
		std::vector<Warning> warnings_;
		unsigned int warning_count_[2] = {0,0};
};

class TP::Grade::Warning{
	public:
		Warning(std::shared_ptr<Session> const& session_ptr, unsigned int const& criteria, std::string const& comment);
		Warning(std::shared_ptr<Session> const& session_ptr, IOFiles& r);
		Warning(Warning&&) = default;
		Warning(Warning const&) = default;
		Warning& operator=(Warning const&) = default;
		~Warning() = default;
		/*{Forbidden*/
		Warning() = delete;
		/*}*/

		void save(IOFiles& w) const;
		void set(std::shared_ptr<Session> const& session_ptr, unsigned int const& criteria, std::string const& comment);

		unsigned int const& get_criteria() const { return criteria_; }
		std::string const& get_comment() const { return comment_; }
		Date get_date() const { return session_->get_date(); }
		std::shared_ptr<TP::Session> get_session() const { return session_; }

	private:
		std::shared_ptr<TP::Session> session_;
		unsigned int criteria_;
		std::string comment_;
};

class TP::Indicator{
	public:
		Indicator(std::shared_ptr<uistring> const& indicator_ptr):
			uistring_ptr_(indicator_ptr){}
		virtual ~Indicator() = default;
		bool operator==(Indicator const& other) const
		{ return uistring_ptr_->text == other.uistring_ptr_->text; }
		/*{Forbidden*/
		Indicator(Indicator const&) = delete;
		Indicator(Indicator&&) = delete;
		Indicator& operator=(Indicator const&) = delete;
		Indicator() = delete;
		/*}*/

		virtual void save(IOFiles& w) const { w<<uistring_ptr_->text; }

		void set_idx(unsigned int const& idx){ uistring_ptr_->idx = idx; }
		void set_text(std::string const& text){ uistring_ptr_->text = text; }

		std::string const& get_text() const { return uistring_ptr_->text; }
		unsigned int const& get_idx() const { return uistring_ptr_->idx; }
		
	protected:
		friend TP::Session::Indicator;
		friend TP::Report::Criteria::Indicator;
		std::shared_ptr<uistring> uistring_ptr_;
};

class TP::Session::Indicator: public TP::Indicator{
	public:
		Indicator(std::shared_ptr<TP::Indicator> const& indicator_ptr, unsigned int const& half_weight):
			TP::Indicator(indicator_ptr->uistring_ptr_),
			half_weight_(half_weight){}
		~Indicator() override = default;
		/*{Forbidden*/
		Indicator() = delete;
		Indicator(Indicator const&) = delete;
		Indicator(Indicator&&) = delete;
		Indicator& operator=(Indicator const&) = delete;
		/*}*/

		void save(IOFiles& w) const override {
			TP::Indicator::save(w);
			w<<half_weight_;
		}

		void set_half_weight(unsigned int const& half_weight){ half_weight_ = half_weight; }
		unsigned int const& get_half_weight() const { return half_weight_; }

	private:
		friend TP::Report::Criteria::Indicator;
		unsigned int half_weight_;
};

class TP::Report::Criteria::Indicator: public TP::Indicator{
	public:
		Indicator(std::shared_ptr<TP::Session::Indicator> const& indicator_ptr, double const& mark):
			TP::Indicator(indicator_ptr->uistring_ptr_),
			mark_(mark),
			half_weight_(&indicator_ptr->half_weight_){}
		~Indicator() override = default;
		/*{Forbidden*/
		Indicator() = delete;
		Indicator(Indicator const&) = delete;
		Indicator(Indicator&&) = delete;
		Indicator& operator=(Indicator const&) = delete;
		/*}*/

		void save(IOFiles& w) const override {
			TP::Indicator::save(w);
			w<<mark_;
		}

		void set_mark(double const& mark){ mark_ = mark; }
		unsigned int const& get_mark() const { return mark_; }

		void up(){ mark_ = (mark_<3?mark_+1:0); }
		void down(){ mark_ = (mark_?mark_-1:3); }

		std::string get_status() const {
			switch(mark_){
				case 0: return "NE   "; break;
				case 1: return "  I  "; break;
				case 2: return "   L "; break;
				case 3: return "    B"; break;
				default: return "?????";break;
			}
		}
		unsigned int get_score() const { return mark_?((*half_weight_)*(mark_-1)):0; }
		double get_half_weight() const { return *half_weight_; }

	private:
		unsigned int mark_;
		unsigned int const* const half_weight_;
};

inline bool sort_indicator_ptr(std::shared_ptr<TP::Indicator> const& a, std::shared_ptr<TP::Indicator> const& b)
{ return a->get_idx() < b->get_idx(); }
#endif
