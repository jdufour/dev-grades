#ifndef DEF_NOTE
#define DEF_NOTE

#include "Evaluation.hpp"

class Note: public Evaluation{
	public:
		Note() = default;
		Note(IOFiles& r);
		~Note() = default;
		/*{Forbidden*/
		Note(Note&&) = delete;
		Note(Note const&) = delete;
		Note& operator=(Note const&) = delete;
		/*}*/

		void edit(std::string const& title, Date const& date, double const& weight);
		void compute_evaluation_stats() override;
		void summary(Latex& latex, std::string const& class_id) const override;
		void student_feedback(Latex& latex, std::shared_ptr<GenericGrade> const& grade) const override;

		unsigned int get_n_criteria_to_fill() const override { return 1; }
		double get_max_criteria_to_fill(unsigned int const& i) const override { (void)(i); return 6; }
		std::vector<std::string> list_evaluation_criteria() const override;

		unsigned int const& get_type() const override { return TYPE; }
		static const constexpr unsigned int TYPE = 3;
		static const constexpr char* NAME = "Note";

		class Grade: public GenericGrade{
			public:
				Grade(std::shared_ptr<Student> const& student, std::shared_ptr<Evaluation> const& eval);
				Grade(std::shared_ptr<Student> const& student, std::shared_ptr<Evaluation> const& eval, IOFiles& r);
				virtual ~Grade() = default;
				/*{Forbidden*/
				Grade(Grade&&) = delete;
				Grade(Grade const&) = delete;
				Grade& operator=(Grade const&) = delete;
				/*}*/

				void compute_grade() override;
				void reset_grade(unsigned int const& n) override { points_.set(n,0); }
				std::string get_result_info() const override { return ""; }
				std::string get_fname() const override { return "note-" + eval_->get_date().yyyymmdd('-') + "-" + student_->get_mail_name(); }
				std::string print_grade() const override { return my::tostring(grade_,1) + " (" + my::tostring(grade_without_rounding_,2) + ")"; }

				std::string summary() const;
		};

	private:
		double class_average_without_rounding_ = 0.0;//!< class average with deduction but no rounding
};
#endif
