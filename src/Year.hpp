#ifndef DEF_YEAR
#define DEF_YEAR

#include "Class.hpp"

class Year{
	public:
		Year(std::string const& io_path, Date const& start, Date const& mid, Date const& end);
		Year(std::string const& io_path, std::string const& jdfile);
		virtual ~Year() = default;
		/*{Forbidden*/
		Year(Year&&) = delete;
		Year(Year const&) = delete;
		Year& operator=(Year) = delete;
		/*}*/
		void save() const;

		unsigned int add_class(std::string const& filename, std::string const& class_id);
		void remove_class(unsigned int const& i);
		void sort_classes(){ std::sort(classes_.begin(),classes_.end(),sort_by_name); }

		unsigned int summary(bool const& full_summary) const;
		void csv(std::string const& class_id) const;

		std::string const& get_io_path() const { return io_path_; }
		std::string const& get_loading_msg() const { return loading_msg_; }
		Date const& get_start() const { return start_; }
		Date const& get_today() const { return today_; }
		Date const& get_end() const { return end_; }
		std::string get_years() const { return my::tostring(start_.year()) + "-" + my::tostring(end_.year()); }
		std::string get_date() const { return today_.ddmmyyyy('.'); }
		std::string get_semester() const { return FIRST_SEMESTER?"Automne":"Printemps"; }
		unsigned int const& get_nclasses() const { return nclasses_; }
		std::shared_ptr<Class> get_class(unsigned int const& i){ return classes_[i]; }
		std::vector<std::string> get_classes_list() const;
		unsigned int get_max_nstudents() const;

		static const std::vector<unsigned int> TYPES;
		static const std::vector<std::string> NAMES;
		static bool FIRST_SEMESTER;

	private:
		std::string loading_msg_ = "Aucun fichier de note chargé";
		/*TODO:(high) manage date differently*/
		std::string io_path_ = "";
		Date start_;
		Date mid_;
		Date end_;
		Date today_;
		unsigned int nclasses_ = 0;
		std::vector<std::shared_ptr<Class> > classes_;

		static bool sort_by_name(std::shared_ptr<Class> const& a, std::shared_ptr<Class> const& b);
};
#endif
