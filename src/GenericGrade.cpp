#include "GenericGrade.hpp"
#include "Student.hpp"
#include "Evaluation.hpp"
#include "Class.hpp"

GenericGrade::GenericGrade(std::shared_ptr<Student> const& student, std::shared_ptr<Evaluation> const& eval, unsigned int const& n):
	student_(student),
	eval_(eval),
	points_(n,0),
	grade_(666.6)
{}

GenericGrade::GenericGrade(std::shared_ptr<Student> const& student, std::shared_ptr<Evaluation> const& eval, IOFiles& r):
	student_(student),
	eval_(eval),
	points_(r.read<Vector<double> >()),
	grade_(r.read<double>()),
	catch_up_(r.read<bool>()),
	recorded_(r.read<bool>())
{}

void GenericGrade::save(IOFiles& w) const {
	w<<student_->get_uuid()<<eval_->get_uuid()<<points_<<grade_<<catch_up_<<recorded_;
}

std::string GenericGrade::get_student_name(bool const& firstname_first) const { return student_->get_name(firstname_first); }

std::string const& GenericGrade::get_title() const { return eval_->get_title(); }

Date const& GenericGrade::get_date() const { return eval_->get_date(); }

unsigned int const& GenericGrade::get_type() const { return eval_->get_type(); }

double const& GenericGrade::get_weight() const { return eval_->get_weight(); }

double const& GenericGrade::get_grade() const { return grade_; }

double const& GenericGrade::get_grade_without_rounding() const { return grade_without_rounding_; }

std::shared_ptr<Student> const GenericGrade::get_student() const { return student_; }

std::shared_ptr<Evaluation> const GenericGrade::get_eval() const { return eval_; }

unsigned int GenericGrade::student_feedback(std::string const& io_path, std::string const& fname) const {
	Latex latex(io_path,fname,"article");
	latex.begindocument(Latex::header);
	eval_->student_feedback(latex, student_->get_grade(eval_));
	if(latex.enddocument(true,true)){
		std::cerr<<__PRETTY_FUNCTION__<<": compiling the student feedback for '"<<student_->get_name(true)<<"' failed"<<std::endl;
		return 1;
	} 
	return 0;
}

std::string GenericGrade::write_mail_body() const {
	return "Ceci est un mail automatique contenant le résultat de l'évaluation\n"
		"'" + eval_->get_title() + "' du '" + eval_->get_date().ddmmyyyy()+"'\n\n";
}

unsigned int GenericGrade::send_mail() const {
	Class const* class_ptr(student_->get_class());

	IOFiles r(class_ptr->get_io_path()+"mails/mail.jdbin",false,false);
	if(r.is_open()){
		std::string io_path(class_ptr->get_io_path()  + "mails/" + class_ptr->get_id() + "/" + eval_->get_date().yyyymmdd('-') + "/");

		Linux command;
		command.mkpath(io_path.c_str());
		student_feedback(io_path,get_fname());

		//MailCurlSMTP mail;
		//mail.set_account(r);
		//mail.set_subject("Évaluation TP: " + date_.yyyymmdd('.'));
		//mail.set_body( e->get_mail() )
		//mail.set_pdf(io_path + fname + ".pdf");
		//mail.set_log(io_path + fname + ".log");
		//mail.set_to(r->get_student()->get_email());
		//mail.send();
		return 0;
	}
	std::cerr<<__PRETTY_FUNCTION__<<": the mail is not configured"<<std::endl;
	return 1;
}
