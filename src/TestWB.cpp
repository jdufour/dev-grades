#include "TestWB.hpp"

/*{TestWB*/
const constexpr unsigned int TestWB::TYPE;
const constexpr char* TestWB::NAME;

TestWB::TestWB(IOFiles& r):
	Evaluation(r),
	class_average_raw_(r.read<double>()),
	class_average_without_rounding_(r.read<double>()),
	deducted_points_(r.read<double>()),
	bonus_coef_(r.read<double>()),
	max_bonus_point_(r.read<double>()),
	nbenefit_bonus_(r.read<unsigned int>()),
	max_points_(r.read<Vector<double> >())
{}

void TestWB::save(IOFiles& w) const {
	Evaluation::save(w);
	w
		<<class_average_raw_
		<<class_average_without_rounding_
		<<deducted_points_
		<<bonus_coef_
		<<max_bonus_point_
		<<nbenefit_bonus_
		<<max_points_;
}

std::vector<std::string> TestWB::list_evaluation_criteria() const {
	std::vector<std::string> tmp(n_criteria_+1);
	tmp[0] = "Bonus\n"
		+ my::tostring(average_bonus_point_,2) + "/"  + my::tostring(max_bonus_point_);
	for(unsigned int i(0);i<n_criteria_;i++){
		tmp[i+1] = "Exo " + my::tostring(i+1) + "\n"
			+ my::tostring(average_point_per_exercise_(i),2) + "/" + my::tostring(max_points_(i));
	}
	return tmp;
}

void TestWB::edit(std::string const& title, Date const& date, double const& weight, double const& deducted_points, double const& bonus_coef, double const& max_bonus_point, Vector<double> const& max_points){
	title_ = title;
	date_ = date;
	weight_ = weight;
	deducted_points_ = deducted_points;
	bonus_coef_ = bonus_coef;
	max_bonus_point_ = max_bonus_point;
	max_points_ = max_points;
	//This action will delete all records of the previous results
	if(n_criteria_ != max_points_.size()){
		n_criteria_ = max_points_.size();
		for(auto& g:evaluation_results_){
			g->reset_grade(max_points_.size());
		}
	}
}

void TestWB::compute_evaluation_stats(){
	nfails_ = 0;
	nrecorded_ = 0;
	nbenefit_bonus_ = 0.0;
	class_average_ = 0.0;
	class_average_raw_ = 0.0;
	class_average_without_rounding_ = 0.0;
	average_bonus_point_ = 0.0;
	average_point_per_exercise_.set(n_criteria_,0.0);

	std::shared_ptr<TestWB::Grade> grade_ptr;
	for(auto const& g:evaluation_results_){
		grade_ptr = std::dynamic_pointer_cast<TestWB::Grade>(g);
		if(grade_ptr->is_recorded()){
			average_bonus_point_ += grade_ptr->get_bonus_point();
			for(unsigned int i(0);i<n_criteria_;i++){
				average_point_per_exercise_(i) += grade_ptr->get_point(i);
			}
			class_average_ += grade_ptr->get_grade();
			class_average_raw_ += grade_ptr->get_grade_raw();
			class_average_without_rounding_ += grade_ptr->get_grade_without_rounding();
			if(grade_ptr->get_grade()>my::round_nearest(grade_ptr->get_grade_raw(),2)){ nbenefit_bonus_++; }
			if(grade_ptr->get_grade()<4){ nfails_++; }
			nrecorded_++;
		}
	}
	if(nrecorded_){
		class_average_ /= nrecorded_;
		class_average_raw_ /= nrecorded_;
		class_average_without_rounding_ /= nrecorded_;
		average_bonus_point_ /= nrecorded_;
		average_point_per_exercise_ /= nrecorded_;
	}

	grades_for_histogram_.clear();
	if(all_points_for_histogram_){ delete[] all_points_for_histogram_; }
	all_points_for_histogram_ = new std::vector<double>[n_criteria_+1];
	for(unsigned int i(0);i<n_criteria_+1;i++){
		all_points_for_histogram_[i] = std::vector<double>(nrecorded_);
	}

	unsigned int j(0);
	for(auto const& g:evaluation_results_){
		grade_ptr = std::dynamic_pointer_cast<TestWB::Grade>(g);
		if(grade_ptr->is_recorded()){
			all_points_for_histogram_[0][j] = 100.0*grade_ptr->get_bonus_point()/max_bonus_point_;
			for(unsigned int i(0);i<n_criteria_;i++){
				all_points_for_histogram_[i+1][j] = 100.0*grade_ptr->get_point(i)/max_points_(i);
			}
			grades_for_histogram_.push_back(grade_ptr->get_grade());
			j++;
		}
	}
}

void TestWB::summary(Latex& latex, std::string const& class_id) const {
	std::string tmp("{l|");
	for(unsigned int i(0);i<=n_criteria_;i++){ tmp += "|S[table-format=2.3]"; }
	tmp += "||S[table-format=1.3]|S[table-format=1.3]|S[table-format=1.3]}";

	latex.subsection(true,class_id + ": " + title_);
	latex.begin_center();
	latex.begin_tabular(tmp);
	latex+="\\toprule";
	tmp = max_points_.size()?"\\multicolumn{" + my::tostring(max_points_.size()) + "}{c||}{Points} & ":"";
	latex+="Nom / Prénom & {Bonus} & " + tmp + " \\multicolumn{3}{c}{Notes}\\\\";
	tmp = my::tostring(nfails_) + "/" + my::tostring(nrecorded_) + " +" + my::tostring(nbenefit_bonus_) + " & " + my::tostring(max_bonus_point_) + " & ";
	for(unsigned int i(0);i<max_points_.size();i++){ tmp += my::tostring(max_points_(i)) + " & "; }
	latex+= tmp + "\\multicolumn{3}{c}{$\\dfrac{5(\\text{P})}{" + my::tostring(max_points_.sum())+"-"+my::tostring(deducted_points_) + "}+"+my::tostring(bonus_coef_) + "(\\text{B})+1$} \\\\\\midrule";
	bool gray_line(false);
	for(auto const& e:evaluation_results_){
		if(gray_line){ latex.command("rowcolor{gray!30}"); }
		gray_line = !gray_line;
		latex += std::dynamic_pointer_cast<TestWB::Grade>(e)->summary();
	}
	latex+="\\midrule";
	tmp = "Moyennes & " + my::tostring(average_bonus_point_,3) + " & ";
	for(unsigned int i(0);i<n_criteria_;i++){
		tmp += my::tostring(average_point_per_exercise_(i),3) + " & ";
	}
	tmp += my::tostring(class_average_raw_,3) + " & " + my::tostring(class_average_without_rounding_,3) + " & " + my::tostring(class_average_,3);
	latex += tmp + "\\\\\\bottomrule";
	latex.end_tabular();
	latex.end_center();

	latex.begin_center();
	for(unsigned int i(0);i<n_criteria_+1;i++){
		latex.begin_tikzpicture();
		latex.begin_axis("ybar, ymin=0, xmin=0, xmax=100, scale=0.4,title={"+(i?"Exo "+my::tostring(i):"Bonus")+"}");
		latex.histogram(all_points_for_histogram_[i],5,0,100);
		latex.end_axis();
		latex.end_tikzpicture();
	}
	latex.end_center();

	latex.begin_center();
	latex.begin_tikzpicture();
	latex.begin_axis("ybar, ymin=0, xmin=0.75, xmax=6.25");
	latex.histogram(grades_for_histogram_,11,0.75,6.25);
	latex.end_axis();
	latex.end_tikzpicture();
	latex.end_center();
}

void TestWB::student_feedback(Latex& latex, std::shared_ptr<GenericGrade> const& grade) const {
	std::shared_ptr<TestWB::Grade> grade_ptr(std::dynamic_pointer_cast<TestWB::Grade>(grade));
	double bonus_point(grade_ptr->get_bonus_point()/max_bonus_point_*bonus_coef_);
	double total_point(0);
	for(unsigned int i(0);i<n_criteria_;i++){ total_point += grade_ptr->get_point(i); }

	latex.begin_minipage("\\linewidth");
	latex.subsection(true, 
			grade_ptr->get_student_name(false) 
			+ ": " + grade_ptr->get_title()
			+ " (" + grade_ptr->get_date().yyyymmdd('.') + ") $\\longrightarrow$ " 
			+ my::tostring(grade_ptr->get_grade())
			);

	latex.begin_minipage("0.30\\linewidth");
	latex.begin_itemize();
	if(deducted_points_){
		latex.item("Note non arrondie, sans bonus ni déduction: " + my::tostring(grade_ptr->get_grade_raw(),2) );
		latex.item(my::tostring(deducted_points_) + (deducted_points_<2.0?" point a été déduit du test.":" points ont été déduits du test."));
	} else {
		latex.item("Note non arrondie, sans bonus: " + my::tostring(grade_ptr->get_grade_raw(),2) );
	}
	latex.item("Bonus " + my::tostring(bonus_point,3) + "/" + my::tostring(bonus_coef_) + " point.");
	latex.item("Note finale non arrondie:" );
	latex+=("$$\\dfrac{" + my::tostring(total_point) + "}{"
			+ my::tostring(max_points_.sum())
			+ (deducted_points_?"-" + my::tostring(deducted_points_):"")
			+ "}\\cdot 5 + " + my::tostring(bonus_point,3) + " + 1 = "
			+ my::tostring(grade_ptr->get_grade_without_rounding(),2) + "$$");
	latex.end_itemize();
	latex.end_minipage();
	latex.command("hfill");

	latex.begin_minipage("0.65\\linewidth");
	latex.begin_center();
	for(unsigned int i(0);i<n_criteria_+1;i++){
		latex.begin_tikzpicture();
		if(i){
			latex.begin_axis("ybar, ymin=0, xmin=0, xmax=100, scale=0.4, title={Exo " + my::tostring(i) + ": " + my::tostring(grade_ptr->get_point(i-1)) + "/" + my::tostring(max_points_(i-1)) + "}");
			latex.histogram(all_points_for_histogram_[i],5,0,100);
			latex+="\\draw[red,ultra thick](axis cs:" + my::tostring(grade_ptr->get_point(i-1)/max_points_(i-1)*100) + ",0) -- ++(axis direction cs:0,30);";
		} else {
			latex.begin_axis("ybar,ymin=0, xmin=0, xmax=100, scale=0.4,title={Bonus: " + my::tostring(grade_ptr->get_bonus_point()/max_bonus_point_*100) + "\\%}");
			latex.histogram(all_points_for_histogram_[i],5,0,100);
			latex+="\\draw[red,ultra thick](axis cs:" + my::tostring(grade_ptr->get_bonus_point()/max_bonus_point_*100) + ",0) -- ++(axis direction cs:0,30);";
		}
		latex.end_axis();
		latex.end_tikzpicture();
	}
	latex.end_center();
	latex.end_minipage();
	latex.command("vspace{5mm}");
	latex.end_minipage();
}
/*}*/

/*{Grade*/
TestWB::Grade::Grade(std::shared_ptr<Student> const& student_ptr, std::shared_ptr<Evaluation> const& eval_ptr, unsigned int const& n_exercise):
	GenericGrade(student_ptr,eval_ptr,n_exercise)
{}

TestWB::Grade::Grade(std::shared_ptr<Student> const& student_ptr, std::shared_ptr< Evaluation> const& eval_ptr, IOFiles& r):
	GenericGrade(student_ptr,eval_ptr,r),
	bonus_point_(r.read<double>())
{ compute_grade(); }

void TestWB::Grade::save(IOFiles& w) const {
	GenericGrade::save(w);
	w<<bonus_point_;
}

void TestWB::Grade::compute_grade(){
	std::shared_ptr<TestWB> grade_ptr(std::dynamic_pointer_cast<TestWB>(eval_));
	unsigned int max_points(grade_ptr->get_max_points().sum());
	if(max_points){
		grade_raw_ = 5.0*points_.sum()/max_points + 1.0;
		grade_without_rounding_ = 5.0*points_.sum()/(max_points-grade_ptr->get_deducted_points()) + grade_ptr->get_bonus_coef()*bonus_point_/grade_ptr->get_max_bonus_point() + 1.0;
	} else { grade_raw_ = grade_without_rounding_ = 0.0; }
	grade_ = std::min(my::round_nearest(grade_without_rounding_,2),6.0);
}

std::string TestWB::Grade::summary() const {
	std::string tmp(student_->get_name(false) + " & ");
	if(recorded_){
		tmp += my::tostring(bonus_point_) + " & ";
		for(unsigned int i(0);i<points_.size();i++){
			tmp += my::tostring(points_(i),3) + " & ";
		}
		tmp += my::tostring(grade_raw_,3) + " & ";
		tmp += my::tostring(grade_without_rounding_,3) + " & ";
		tmp += my::tostring(grade_);
		if(grade_>my::round_nearest(grade_raw_,2)){ tmp += "*"; }
	} else { tmp += std::string(points_.size()+3,'&'); }
	return tmp + "\\\\";
}

std::string TestWB::Grade::get_result_info() const {
	std::shared_ptr<TestWB> eval_ptr(std::dynamic_pointer_cast<TestWB>(eval_));
	std::string stmp("Bonus: " + my::tostring(bonus_point_/eval_ptr->get_max_bonus_point(),3)+" Points: ");
	for(unsigned int i(0);i<points_.size();i++){
		stmp += my::tostring(points_(i),3) + "/" + my::tostring(eval_ptr->get_max_criteria_to_fill(i+1)) + (i+1==points_.size()?"":", ");
	}
	return stmp;
}
/*}*/
