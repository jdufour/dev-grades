#ifndef DEF_TESTONSKILLS
#define DEF_TESTONSKILLS

#include "Test.hpp"
#include "OnSkills.hpp"

class TestOnSkills: public Test, public OnSkills{
	public:
		TestOnSkills() = default;
		TestOnSkills(IOFiles& r);
		~TestOnSkills() = default;
		/*{Forbidden*/
		TestOnSkills(TestOnSkills&&) = delete;
		TestOnSkills(TestOnSkills const&) = delete;
		TestOnSkills& operator=(TestOnSkills const&) = delete;
		/*}*/

		class Grade;

		void edit(std::string const& title, Date const& date, double const& weight, double const& deducted_points, Vector<double> const& max_points) override;
		void save(IOFiles& w) const override;
		void compute_evaluation_stats() override;
		void student_feedback(Latex& latex, std::shared_ptr<GenericGrade> const& grade) const override;
		unsigned int get_n_criteria_to_fill() const override { return 0; }

		unsigned int const& get_type() const override { return TYPE; }
		static const constexpr unsigned int TYPE = 5;
		static const constexpr char* NAME = "Test\n(compétences)";
};

class TestOnSkills::Grade: public Test::Grade, public GradeOnSkills{
	public:
		Grade(std::shared_ptr<Student> const& student, std::shared_ptr<Evaluation> const& eval);
		Grade(std::shared_ptr<Student> const& student, std::shared_ptr<Evaluation> const& eval, IOFiles& r);
		virtual ~Grade() override = default;
		/*{Forbidden*/
		Grade(Grade&&) = delete;
		Grade(Grade const&) = delete;
		Grade& operator=(Grade const&) = delete;
		/*}*/

		void save(IOFiles& w) const override;

		void compute_grade() override;
		void reset_grade(unsigned int const& n) override;

		std::string get_exercise_point_info(unsigned int const& i) const override;
};
#endif
