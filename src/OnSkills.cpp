#include "OnSkills.hpp"

/*{OnSkills*/
OnSkills::OnSkills(IOFiles& r):
	n_exercise_(r.read<unsigned int>())
{
	for(unsigned int i(0);i<n_exercise_;i++){
		os_exercise_.push_back(std::make_shared<OnSkills::Exercise>(r));
	}
}

void OnSkills::save(IOFiles& w) const {
	w<<n_exercise_;
	for(auto const& e:os_exercise_){ e->save(w); }
}

void OnSkills::set_n_exercise(unsigned int const& n_exercise){
	//This action will delete all records of the previous results
	if(n_exercise_ != n_exercise){
		n_exercise_ = n_exercise;
		os_exercise_.clear();
		os_exercise_.resize(n_exercise);
		for(auto& e:os_exercise_){
			e = std::make_shared<OnSkills::Exercise>();
		}
	}
}

void OnSkills::compute_evaluation_stats(std::vector<std::shared_ptr<GenericGrade> > evaluation_results) const {
	for(auto const& e:os_exercise_){ e->set_success(); }
	for(auto const& g:evaluation_results){
		/*TODO:(high) set a warning when a grade is not fully evaluated (some '----' remain)*/
		for(auto const& e:std::dynamic_pointer_cast<GradeOnSkills>(g)->get_exercises()){ 
			e->get_exercise_ptr()->add_to_success(e);
		}
	}
	for(auto const& e:os_exercise_){ e->average_success(evaluation_results.size()); }
}
/*}*/

/*{OnSkills::Exercise*/
OnSkills::Exercise::Exercise(){
	skill_[0] = std::make_shared<Skill>("Compréhension/Analyse");
	skill_[1] = std::make_shared<Skill>("Modélisation");
	skill_[2] = std::make_shared<Skill>("Résolution");
	skill_[3] = std::make_shared<Skill>("Formalisme");
	success_on_skill_[0] = 0;
	success_on_skill_[1] = 0;
	success_on_skill_[2] = 0;
	success_on_skill_[3] = 0;
}

OnSkills::Exercise::Exercise(IOFiles& r){
	sum_weight_ = 0.0;
	for(auto& s:skill_){
		s = std::make_shared<Skill>(r);
		sum_weight_+= s->get_weight();
	}
}

void OnSkills::Exercise::save(IOFiles& w) const {
	for(auto const& s:skill_){ s->save(w); }
}

double OnSkills::Exercise::get_rescaled_weight(unsigned int const& s) const {
	return skill_[s]->get_weight()/sum_weight_;
}

void OnSkills::Exercise::sum_weight(){
	sum_weight_ = 0.0;
	for(auto const& s:skill_){ sum_weight_ += s->get_weight(); }
}

void OnSkills::Exercise::set_success(){ 
	for(unsigned int s(0);s<OnSkills::N_SKILL;s++){
		success_on_skill_[s] = 0; 
	}
}

void OnSkills::Exercise::average_success(unsigned int const& n){ 
	for(unsigned int s(0);s<OnSkills::N_SKILL;s++){
		success_on_skill_[s] /= n; 
	}
}

void OnSkills::Exercise::add_to_success(std::shared_ptr<GradeOnSkills::Exercise> const& exercise_ptr){
	for(unsigned int s(0);s<OnSkills::N_SKILL;s++){
		success_on_skill_[s] += exercise_ptr->get_percentage(s);
	}
}

/*{Skill*/
OnSkills::Exercise::Skill::Skill(std::string const& name):
	weight_(1.0),
	name_(name)
{}

OnSkills::Exercise::Skill::Skill(IOFiles& r):
	weight_(r.read<double>()),
	name_(r.read<std::string>())
{
	unsigned int size(0);
	r>>size;
	for(unsigned int s(0);s<size;s++){
		indicator_.push_back(uistring(s,r.read<std::string>()));
	}
}

void OnSkills::Exercise::Skill::save(IOFiles& w) const {
	w<<weight_<<name_<<(unsigned int)(indicator_.size());
	for(auto const& ind:indicator_){ w<<ind.text; }
}

std::vector<std::string> OnSkills::Exercise::Skill::list_indicators() const {
	std::vector<std::string> tmp;
	for(auto const& ind:indicator_){ tmp.push_back(my::tostring(ind.idx) + ". " + ind.text); }
	return tmp;
}

bool OnSkills::Exercise::Skill::add_indicator(unsigned int const& idx, std::string const& text){
	unsigned int size(indicator_.size());
	if(size){
		for(auto const& ind:indicator_){
			if(ind.text == text){
				std::cerr<<__PRETTY_FUNCTION__<<": cannot have two identical indicators: "<<text<<std::endl;
				return false;
			}
		}
		indicator_.insert(indicator_.begin()+idx,uistring(idx,text));
		for(unsigned int i(idx+1);i<size+1;i++){ indicator_[i].idx = i; }
	} else { indicator_.push_back(uistring(0,text)); }
	return true;
}

bool OnSkills::Exercise::Skill::edit_indicator(unsigned int const& idx, unsigned int const& newidx, std::string const& text){
	for(unsigned int i(0);i<indicator_.size();i++){
		if(i != idx && indicator_[i].text == text){
			std::cerr<<__PRETTY_FUNCTION__<<": cannot have two identical indicators: "<<text<<std::endl;
			return false;
		}
	}
	indicator_[idx].text = text;
	if(idx != newidx){
		if(newidx > idx){
			for(unsigned int i(idx+1);i<newidx+1;i++){ indicator_[i].idx--; }
		} else {
			for(unsigned int i(newidx);i<idx;i++){ indicator_[i].idx++; }
		}
		indicator_[idx].idx = newidx;
		std::sort(indicator_.begin(),indicator_.end(),sort_uistring);
	}
	return true;
}

void OnSkills::Exercise::Skill::remove_indicator(unsigned int const& idx){
	indicator_.erase(indicator_.begin()+idx);
	for(unsigned int i(idx);i<indicator_.size();i++){ indicator_[i].idx = i; }
}
/*}*/
/*}*/

/*{GradeOnSkills*/
GradeOnSkills::GradeOnSkills(std::shared_ptr<OnSkills> const& onskills_ptr){
	for(auto const& e:onskills_ptr->get_exercises()){
		exercise_.push_back(std::make_shared<GradeOnSkills::Exercise>(e));
	}
}

GradeOnSkills::GradeOnSkills(std::shared_ptr<OnSkills> const& onskills_ptr, IOFiles& r){
	for(auto const& e:onskills_ptr->get_exercises()){
		exercise_.push_back(std::make_shared<GradeOnSkills::Exercise>(e,r));
	}
}

void GradeOnSkills::set_n_exercise(OnSkills* onskills_ptr){
	//This action will delete all records of the previous results
	if(exercise_.size() != onskills_ptr->get_n_exercise()){
		exercise_.clear();
		for(auto const& e:onskills_ptr->get_exercises()){
			exercise_.push_back(std::make_shared<GradeOnSkills::Exercise>(e));
		}
	}
}

double GradeOnSkills::compute_point(unsigned int const& i, Vector<double> const& max_points) const {
	double tmp(0);
	for(unsigned  int s(0);s<OnSkills::N_SKILL;s++){ tmp += exercise_[i]->get_weighted_percentage(s); }
	return (tmp>1?1.0:tmp)*max_points(i);
}

void GradeOnSkills::reset_grade(std::shared_ptr<OnSkills> const& onskills_ptr){
	exercise_.clear();
	for(auto const& e:onskills_ptr->get_exercises()){
		exercise_.push_back(std::make_shared<GradeOnSkills::Exercise>(e));
	}
}
/*}*/

/*{GradeOnSkills::Exercise*/
GradeOnSkills::Exercise::Exercise(std::shared_ptr<OnSkills::Exercise> const& exercise):
	exercise_(exercise)
{ for(auto& f:fulfillment_){ f = 6.0; } }

GradeOnSkills::Exercise::Exercise(std::shared_ptr<OnSkills::Exercise> const& exercise, IOFiles& r):
	exercise_(exercise)
{ for(auto& f:fulfillment_){ r>>f; } }

int GradeOnSkills::Exercise::change_fulfillment(unsigned int const& s, int change){
	if(change==0){ return 0; }
	unsigned int tmp(fulfillment_[s]);
	if(change<0){ change = -( (-change)%N_LEVELS_ ) + N_LEVELS_; }
	fulfillment_[s] = (tmp+change)%N_LEVELS_;
	return fulfillment_[s]-tmp;
}

void GradeOnSkills::Exercise::save(IOFiles& w) const {
	for(auto const& f:fulfillment_){ w<<f; }
}

double GradeOnSkills::Exercise::get_percentage(unsigned int const& s) const {
	return fulfillment_[s]+1<N_LEVELS_?1.0*fulfillment_[s]/(N_LEVELS_-3):0;
}

double GradeOnSkills::Exercise::get_weighted_percentage(unsigned int const& s) const {
	return get_percentage(s)*get_exercise_ptr()->get_rescaled_weight(s);
}

std::string GradeOnSkills::Exercise::get_status(unsigned int const& s) const {
	switch(fulfillment_[s]){
		case 0: return "     "; break; //  0%
		case 1: return "X    "; break; // 25%
		case 2: return "XX   "; break; // 50%
		case 3: return "XXX  "; break; // 75%
		case 4: return "XXXX "; break; //100%
		case 5: return "XXXXX"; break; //125%
		case 6: return "-----"; break;
		default:return "?????"; break;
	}
}

void GradeOnSkills::Exercise::student_feedback(Latex& latex, double const& point, double const& maxpoint, std::vector<double> const& histogram, bool const& catch_up){
	double alpha_i(0);
	double alpha_f(0);
	double score(0);
	double scale(1.3);
	std::string color[4] = {"red","blue","green","cyan"};
	latex.begin_minipage("0.1\\linewidth");
	latex.begin_tikzpicture();
	for(unsigned int s(0);s<OnSkills::N_SKILL;s++){
		score = scale*get_percentage(s);
		alpha_f = alpha_i + 360*exercise_->get_rescaled_weight(s);
		if(!my::are_equal(alpha_i,alpha_f)){
			latex+="\\fill["
				+ color[s] + "!20]([shift=(" + my::tostring(alpha_i) + ":"
				+ my::tostring(score) + ")]0,0) arc ("
				+ my::tostring(alpha_i) + ":"
				+ my::tostring(alpha_f) + ":"
				+ my::tostring(score) + ") -- (0,0);";

			latex+="\\draw[dashed,"
				+ color[s] + "]([shift=(" + my::tostring(alpha_i) + ":"
				/*TODO:(high) should have a sqrt of the radius so that the area
				 * is proportional to the success*/
				+ my::tostring(scale*exercise_->get_success(s)) + ")]0,0) arc ("
				+ my::tostring(alpha_i) + ":"
				+ my::tostring(alpha_f) + ":"
				+ my::tostring(scale*exercise_->get_success(s)) + ") coordinate[midway] (id);";

			latex+="\\draw["
				+ color[s] + "]([shift=(" + my::tostring(alpha_i) + ":"
				+ my::tostring(scale) + ")]0,0) arc ("
				+ my::tostring(alpha_i) + ":"
				+ my::tostring(alpha_f) + ":"
				+ my::tostring(scale) + ") coordinate[midway] (id);";

			latex+="\\fill[white] (id) circle (1.5mm) node[" + color[s] + "]{" + my::tostring(s+1) + "};";
			alpha_i = alpha_f;
		}
	}

	latex.begin_axis("xshift=-1.4cm, yshift=3cm, ybar, ymin=0, xmin=0, xmax=100, scale=0.4, title={Exo: " + my::tostring(point,3) + "/" + my::tostring(maxpoint) + "}");
	latex.histogram(histogram,5,0,100);
	latex+="\\draw[red,ultra thick](axis cs:" + my::tostring(point/maxpoint*100) + ",0) -- ++(axis direction cs:0,30);";
	latex.end_axis();
	latex.end_tikzpicture();
	latex.end_minipage();

	latex.command("hfill");
	std::shared_ptr<OnSkills::Exercise::Skill> skill_ptr;
	unsigned int n_indicator;
	latex.begin_minipage("0.80\\linewidth");
	latex.begin_enumerate();
	for(unsigned int s(0);s<OnSkills::N_SKILL;s++){
		skill_ptr = exercise_->get_skill(s);
		n_indicator = skill_ptr->get_n_indicator();
		latex.item(skill_ptr->get_name() + ": " + my::tostring(get_weighted_percentage(s)*maxpoint,3) + "/" + my::tostring(exercise_->get_rescaled_weight(s)*maxpoint,3));
		if(catch_up){ latex.command("vspace{2cm}"); } 
		else if(n_indicator){
			latex.begin_enumerate();
			for(unsigned int i(0);i<n_indicator;i++){ latex.item(skill_ptr->get_indicator_text(i)); }
			latex.end_enumerate();
		}
	}
	latex.end_enumerate();
	latex.end_minipage();
}
/*}*/
