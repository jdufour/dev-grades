#ifndef DEF_ONSKILLS
#define DEF_ONSKILLS

#include "GenericGrade.hpp"

class OnSkills{
	public:
		OnSkills() = default;
		OnSkills(IOFiles& r);
		virtual ~OnSkills() = default;
		/*{Forbidden*/
		OnSkills(OnSkills&&) = delete;
		OnSkills(OnSkills const&) = delete;
		OnSkills& operator=(OnSkills const&) = delete;
		/*}*/

		class Exercise;

		virtual void save(IOFiles& w) const;

		void compute_evaluation_stats(std::vector<std::shared_ptr<GenericGrade> > grade_ptr) const;
		void set_n_exercise(unsigned int const& n_exercise);
		unsigned int const& get_n_exercise() const { return n_exercise_; }
		std::vector<std::shared_ptr<Exercise> > get_exercises() const { return os_exercise_; }

		static unsigned int const N_SKILL = 4;

	protected:
		unsigned int n_exercise_ = 0;
		std::vector<std::shared_ptr<Exercise> > os_exercise_;
};

class GradeOnSkills{
	public:
		GradeOnSkills(std::shared_ptr<OnSkills> const& eval_ptr);
		GradeOnSkills(std::shared_ptr<OnSkills> const& eval_ptr, IOFiles& r);
		virtual ~GradeOnSkills() = default;
		/*{Forbidden*/
		GradeOnSkills() = default;
		GradeOnSkills(GradeOnSkills&&) = delete;
		GradeOnSkills(GradeOnSkills const&) = delete;
		GradeOnSkills& operator=(GradeOnSkills const&) = delete;
		/*}*/

		class Exercise;

		void set_n_exercise(OnSkills* onskills_ptr);
		std::shared_ptr<Exercise> get_exercise_ptr(unsigned int const& i){ return exercise_[i]; }
		std::vector<std::shared_ptr<Exercise> > get_exercises(){ return exercise_; }
		double compute_point(unsigned int const& i, Vector<double> const& max_points) const;
		void reset_grade(std::shared_ptr<OnSkills> const& onskills_ptr);
		virtual void compute_grade() = 0;
		virtual std::string get_exercise_point_info(unsigned int const& i) const = 0;

	protected:
		std::vector<std::shared_ptr<Exercise> > exercise_;
};

class GradeOnSkills::Exercise{
	public:
		Exercise(std::shared_ptr<OnSkills::Exercise> const& exercise);
		Exercise(std::shared_ptr<OnSkills::Exercise> const& exercise, IOFiles& r);
		~Exercise() = default;
		/*{Forbidden*/
		Exercise() = delete;
		Exercise(Exercise&&) = delete;
		Exercise(Exercise const&) = delete;
		Exercise& operator=(Exercise const&) = delete;
		/*}*/

		void save(IOFiles& w) const;

		std::string get_status(unsigned int const& s) const;
		int change_fulfillment(unsigned int const& s, int change);
		double get_percentage(unsigned int const& s) const;
		double get_weighted_percentage(unsigned int const& s) const;
		std::shared_ptr<OnSkills::Exercise> get_exercise_ptr() const { return exercise_; }

		void student_feedback(Latex& latex, double const& point, double const& maxpoint, std::vector<double> const& histogram, bool const& catch_up);

	private:
		unsigned int const N_LEVELS_ = 7;
		unsigned int fulfillment_[OnSkills::N_SKILL];
		std::shared_ptr<OnSkills::Exercise> exercise_;
};

class OnSkills::Exercise{
	public:
		Exercise();
		Exercise(IOFiles& r);
		~Exercise() = default;
		/*{Forbidden*/
		Exercise(Exercise&&) = delete;
		Exercise(Exercise const&) = delete;
		Exercise& operator=(Exercise const&) = delete;
		/*}*/

		class Skill{
			public:
				Skill(std::string const& name);
				Skill(IOFiles& r);
				/*{Forbidden*/
				Skill() = delete;
				Skill(Skill&&) = delete;
				Skill(Skill const&) = delete;
				Skill& operator=(Skill const&) = delete;
				/*}*/
				void save(IOFiles& w) const;

				void set_weight(double const& weight) { weight_ = weight; }
				double const& get_weight() const { return weight_; }

				unsigned int get_n_indicator() const { return indicator_.size(); }
				std::vector<std::string> list_indicators() const;
				std::string const& get_indicator_text(unsigned int const& i) const { return indicator_[i].text; }
				std::string const& get_name() const { return name_; }

				bool add_indicator(unsigned int const& idx, std::string const& text);
				bool edit_indicator(unsigned int const& idx, unsigned int const& newidx, std::string const& text);
				void remove_indicator(unsigned int const& idx);

			private:
				double weight_;
				std::string const name_;
				std::vector<uistring> indicator_;
		};

		void save(IOFiles& w) const;

		std::shared_ptr<Skill> get_skill(unsigned int const& s){ return skill_[s]; }
		double const& get_sum_weight() const { return sum_weight_; }
		double get_rescaled_weight(unsigned int const& s) const;
		void sum_weight();

		void set_success();
		void average_success(unsigned int const& n);
		void add_to_success(std::shared_ptr<GradeOnSkills::Exercise> const& exercise_ptr);
		double const& get_success(unsigned int const& s) const { return success_on_skill_[s]; }

	private:
		std::shared_ptr<Skill> skill_[OnSkills::N_SKILL];
		double sum_weight_ = 0.0;
		double success_on_skill_[OnSkills::N_SKILL];
};
#endif
