#include "NCInterface.hpp"

/*{Constructors*/
NCInterface::NCInterface(Year* year, bool const& debug, bool const& force_display):
	year_(year),
	debug_(debug),
	track_(S_CLASS)
{
	for(unsigned int i(0);i<N_ACTIONS;i++){ select_[i] = 0; }

	setlocale(LC_ALL,"");//needs to come before initscr() to output the correct accents
	initscr();
	unsigned int col1(40);
	unsigned int col2(7);
	unsigned int col3(col1+col2+1);
	unsigned int row1(5);
	unsigned int row2(7);
	unsigned int row3(year_->get_max_nstudents());
	row3 = row3<20?24:row3+4; //to ensure a correct size when all classes are empty
	unsigned int row4(row1+row2+row3+1);
	endwin();

	if(row4 > LINES-row1){
		std::cerr<<__PRETTY_FUNCTION__<<": the terminal window may not be high enough to properly display everyting"<<std::endl;
		is_ready_ = force_display;
		row4 = row1;
	} else { row4 = LINES - row4; }
	if(col3 > COLS-geo_s_crind_[1]){
		std::cerr<<__PRETTY_FUNCTION__<<": the terminal window may not be wide enough to properly display everyting (win 3)"<<std::endl;
		is_ready_ = force_display;
		col3 = geo_s_crind_[1];
	} else { col3 = COLS - col3; }

	/*TODO:(low) may enter endless loop if the keys "window+h" are pressed*/
	if(is_ready_ || my::get_yn("La fenêtre est trop petite, voulez-vous néanmoins exécuter le programme?","o","n")){
		is_ready_ = true;
		initscr();
		noecho();
		curs_set(0);
		start_color();
		init_pair(1,COLOR_BLUE,COLOR_BLACK);
		init_pair(2,COLOR_RED,COLOR_BLACK);
		init_pair(3,COLOR_GREEN,COLOR_BLACK);
		ESCDELAY = 0; //ncurse parameter: sets the time delay between ESC push and the actual action

		win_interactive_[0] = newwin(row1,      0,          0,             col1+col2+2);//classes
		win_interactive_[1] = newwin(row2,      0,          row1,          col1+col2+2);//evaluations+exercises
		win_interactive_[2] = newwin(row3,      col1,       row1+row2,     0);          //students
		win_interactive_[3] = newwin(row3,      0,          row1+row2,     col1+col2+2);//grades+points+criteria
		win_interactive_[4] = newwin(row4,      0,          row1+row2+row3,0);          //forms
		win_interactive_[5] = newwin(row3,      col2,       row1+row2,     col1+1);     //average
		win_help_           = newwin(row1+row2, col1+col2+1,0,             0);          //help

		win_status_ = newwin(1,0,LINES-1,0);

		for(unsigned int i(0);i<nfw_;i++){ keypad(win_interactive_[i],true); }
		keypad(win_help_,true);
		status(year_->get_loading_msg());

		geo_s_crind_[1] = col3 - 4; //so that the displayed indicators are visible
		geo_s_tpses_[1] = col3; //so that the tp sessions can all be displayed nicely
	}
}

NCInterface::~NCInterface(){
	curs_set(1);
	for(auto& w:win_interactive_){ delwin(w); }
	delwin(win_help_);
	delwin(win_status_);
	clear();
	refresh();
	endwin();
}
/*}*/

/*{Handle class*/
/*S_CLASS*/
void NCInterface::class_select(){
	while(keepon_){
		track_ = S_CLASS;
		for(unsigned int i(1);i<nfw_;i++){
			wclear(win_interactive_[i]);
			/*TODO:(middle) set correct header consistently (need to keep the msglist_ for the confirm function)*/
			set_window_frame(win_interactive_[i],msglist_[i],false); //msglistok
		}
		select(year_->get_classes_list(),geo_s_class_);
		switch(track_[0]){
			case S_EVALU[0]: //ENTER
				evaluation_select(year_->get_class(select_[S_CLASS[0]]));
				break;
			case A_CLASS[0]: //F1
				class_add();
				break;
			case E_CLASS[0]: //F2
				class_edit();
				break;
			case R_CLASS[0]: //F3
				class_remove();
				break;
			case L_CLASS[0]: //F4
				class_latex();
				break;
		}
	}
}

/*A_CLASS*/
void NCInterface::class_add(){
	if(confirm("Voulez-vous ajouter une classe?")){
		unsigned int geometry[4];
		geometry[0] = 1;
		geometry[1] = 80;
		geometry[2] = 1;
		geometry[3] = 1;

		std::vector<FIELD*> fields;
		add_text_form(fields,"Liste de classe (chemin du fichier '.txt' )","",geometry);
		geometry[3] += geometry[1]+1;
		geometry[1] = 10;
		add_text_form(fields,"Class ID","",geometry);
		fields.push_back(NULL);

		geometry[0] = 3;
		geometry[1]+= geometry[3];
		geometry[3] = 1;

		mvwprintw(win_interactive_[3], 5, 2,"Sélectionnez ensuite le fichier contenant la liste des étudiants.");
		mvwprintw(win_interactive_[3], 5, 2,"Le nom du fichier doit avoir une extension '.txt'. Chaque ligne");
		mvwprintw(win_interactive_[3], 6, 2,"du fichier contient:");
		mvwprintw(win_interactive_[3], 8, 2,"     Groupe;Genre;Nom;Prénom;email");
		mvwprintw(win_interactive_[3],10, 2,"Le fichier doit respecter les conventions suivantes:");
		mvwprintw(win_interactive_[3],12, 2,"     + pas de ligne vide");
		mvwprintw(win_interactive_[3],13, 2,"     + le groupe du TP peut être 'A' ou 'B'");
		mvwprintw(win_interactive_[3],14, 2,"     + le genre peut être 'h' ou 'f'");
		mvwprintw(win_interactive_[3],15, 2,"     + pas d'espace autour des ';'");
		mvwprintw(win_interactive_[3],16, 2,"     + l'email est optionnel");
		mvwprintw(win_interactive_[3],18, 2,"Donnez ensuite un nom/code à la classe");
		wnoutrefresh(win_interactive_[3]);

		if(fill_form(fields,geometry,"Ajout d'une classe")){
			std::string file_list(Linux().return_absolute(my::trim(field_buffer(fields[1],0))));
			std::string class_id(my::trim(field_buffer(fields[3],0)));
			switch(year_->add_class(file_list, class_id)){
				case 0:
					modified_ = true;
					status("La classe '" + class_id + "' a été ajoutée avec succès");
					break;
				case 1:
					status("La classe n'a pas été ajoutée, veuillez donner un identifant");
					break;
				case 2:
					status("La classe n'a pas été ajoutée, l'indentifiant '" + class_id + "' existe déjà");
					break;
				case 3:
					status("La classe n'a pas été ajoutée, le fichier donné doit avoir une extension '.txt'");
					break;
				case 4:
					status("La classe n'a pas été ajoutée, le fichier donné n'a pas pu être ouvert");
					break;
				case 5:
					status("La classe n'a pas été ajoutée, le fichier donné est mal formaté");
					break;
			}
		} else { status("Pas de nouvelle classe crée"); }
		for(auto& f:fields){ free_field(f); }
	}
}

/*E_CLASS*/
void NCInterface::class_edit(){
	if(year_->get_nclasses()){
		std::shared_ptr<Class> class_ptr(year_->get_class(select_[S_CLASS[0]]));
		if(confirm("Voulez-vous éditer la " + class_ptr->get_id() + "?")){
			unsigned int geometry[4];
			geometry[0] = 1;
			geometry[1] = 10;
			geometry[2] = 1;
			geometry[3] = 1;

			std::vector<FIELD*> fields;
			geometry[1] = 10;
			add_text_form(fields,"Class ID",class_ptr->get_id(),geometry);
			fields.push_back(NULL);

			geometry[0] = 3;
			geometry[1]+= geometry[3];
			geometry[3] = 1;
			if(fill_form(fields,geometry,"Édition d'une classe")){
				class_ptr->edit(my::trim(field_buffer(fields[1],0)));
				year_->sort_classes();
				modified_ = true;
			} else { status("Édition de " + class_ptr->get_id() + " annulée"); }
			for(auto& f:fields){ free_field(f); }
		}
	} else { status("Aucune classe à éditer"); }
}

/*R_CLASS*/
void NCInterface::class_remove(){
	if(year_->get_nclasses()){
		if(confirm("Voulez-vous supprimer la " + year_->get_class(select_[S_CLASS[0]])->get_id() + "?")){
			year_->remove_class(select_[S_CLASS[0]]);
			select_[S_CLASS[0]] = 0;
			modified_ = true;
		}
	} else { status("Aucune classe à supprimer"); }
}

/*L_CLASS*/
void NCInterface::class_latex(){
	if(year_->get_nclasses()){
		if(confirm("Voulez-vous générer un PDF contenant toutes les évaluations de la " + year_->get_class(select_[S_CLASS[0]])->get_id() + "?")){
			processing("Veuillez patienter durant la création du PDF contenant toutes les évaluations de la " + year_->get_class(select_[S_CLASS[0]])->get_id());
			if(year_->get_class(select_[S_CLASS[0]])->summary(true)) { status("Problème durant la compilation"); }
			else { status("PDF crée"); }
		}
	} else { status("Aucune classe à latexer"); }
}
/*}*/

/*{Handle student*/
void NCInterface::student_configure(std::string const& firstname, std::string const& lastname, std::string const& email, bool const& in_group_A, bool const& is_man, std::vector<FIELD*>& fields, unsigned int geometry[]){
	geometry[0] = 1;
	geometry[1] = 20;
	geometry[2] = 1;
	geometry[3] = 1;

	add_text_form(fields,"Prénom",firstname,geometry);
	geometry[3] += geometry[1]+1;
	add_text_form(fields,"Nom",lastname,geometry);
	geometry[3] += geometry[1]+1;
	geometry[1] = 40;
	add_text_form(fields,"Email",email,geometry);
	geometry[3] += geometry[1]+1;
	geometry[1] = 7;
	add_list_form(fields,"Groupe [A-B]",{"B","A"},in_group_A,geometry);
	geometry[3] += geometry[1]+1;
	add_list_form(fields,"Genre [h-f]",{"f","h"},is_man,geometry);
	fields.push_back(NULL);

	geometry[0] = 3;
	geometry[1]+= geometry[3];
	geometry[3] = 1;
}

/*A_STUDE*/
void NCInterface::student_add(std::shared_ptr<Class> const& class_ptr){
	if(confirm("Voulez-vous ajouter un étudiant?")){
		unsigned int geometry[4];
		std::vector<FIELD*> fields;
		student_configure("","","",true,true,fields,geometry);

		if(fill_form(fields,geometry,"Ajout d'un étudiant")){
			class_ptr->student_add(std::make_shared<Student>(
						my::trim(field_buffer(fields[1],0)),
						my::trim(field_buffer(fields[3],0)),
						my::trim(field_buffer(fields[5],0)),
						my::string2type<char>(field_buffer(fields[7],0)) == 'A',
						my::string2type<char>(field_buffer(fields[7],0)) == 'h'
						));
			modified_ = true;
		} else { status("Aucun ajout d'élève"); }
		for(auto& f:fields){ free_field(f); }
	}
}

/*E_STUDE*/
void NCInterface::student_edit(std::shared_ptr<Class> const& class_ptr){
	if(select_[S_ST_OV[0]]<class_ptr->get_nstudents()){
		if(confirm("Voulez vous éditer " + class_ptr->get_student(select_[S_ST_OV[0]])->get_name(true) + "?")){
			unsigned int geometry[4];
			std::vector<FIELD*> fields;
			std::shared_ptr<Student> student_ptr(class_ptr->get_student(select_[S_ST_OV[0]]));
			student_configure(student_ptr->get_firstname(),student_ptr->get_lastname(),student_ptr->get_email(),student_ptr->in_group_A(),student_ptr->is_man(),fields,geometry);

			if(fill_form(fields,geometry,"Édition d'un étudiant")){
				student_ptr->edit(
						my::trim(field_buffer(fields[1],0)),
						my::trim(field_buffer(fields[3],0)),
						my::trim(field_buffer(fields[5],0)),
						my::string2type<char>(field_buffer(fields[7],0)) == 'A',
						my::string2type<char>(field_buffer(fields[9],0)) == 'h'
						);
				modified_ = true;
			} else { status("Édition de " + student_ptr->get_name(true) + " annulée"); }
			for(auto& f:fields){ free_field(f); }
		}
	} else { status("Étudiant non trouvé"); }
}

/*R_STUDE*/
void NCInterface::student_remove(std::shared_ptr<Class> const& class_ptr){
	if(select_[S_ST_OV[0]]<class_ptr->get_nstudents()){
		std::string studentname(class_ptr->get_student(select_[S_ST_OV[0]])->get_name(true));
		if(confirm("Voulez-vous supprimer " + studentname + "?")){
			if(class_ptr->student_remove(select_[S_ST_OV[0]])){
				select_[S_ST_OV[0]] = 0;
				modified_ = true;
				status(studentname + " supprimé");
				display_grades(class_ptr);
				wclear(win_interactive_[5]);
				set_window_frame(win_interactive_[5],msglist_[5],false);
				mvwprintwlist(win_interactive_[5],2,2,1,0,class_ptr->list_average());
			} else { status("Un problème est survenu durant la supression de " + studentname); }
		}
	} else { status("Étudiant non trouvé"); }
}

/*L_STUDE*/
void NCInterface::student_latex(std::shared_ptr<Class> const& class_ptr){
	if(confirm("Voulez-vous générer un PDF contenant tous les évaluations pour chaque élève?")){
		processing("Veuillez patienter durant la création du PDF contenant tous les évaluations pour chaque élève");
		if(class_ptr->student_feedback()){ status("Problème durant la compilation"); }
		else { status("PDF crée"); }
	}
}

/*S_ST_OV*/
void NCInterface::student_select_overview(std::shared_ptr<Class> const& class_ptr){
	if(class_ptr->get_nevals()){
		set_window_frame(win_interactive_[1],msglist_[1],false); //msglistok
		bool repeat(true);
		while(keepon_ && repeat){
			track_ = S_ST_OV;
			select(class_ptr->list_students(),geo_s_stude_);
			switch(track_[0]){
				case A_STUDE[0]: //F1
					student_add(class_ptr);
					break;
				case E_STUDE[0]: //F2
					student_edit(class_ptr);
					break;
				case R_STUDE[0]: //F3
					student_remove(class_ptr);
					break;
				case L_STUDE[0]: //F4
					student_latex(class_ptr);
					break;
				case S_ST_OV[0]:
					break;
				default:
					repeat = false;
			}
		}
	} else { status("Aucune évaluation"); }
}

/*S_ST_EV*/
void NCInterface::student_select_for_given_evaluation(std::shared_ptr<Class> const& class_ptr, std::shared_ptr<Evaluation> const& eval_ptr){
	msglist_[1] = eval_ptr->get_title();
	msglist_[3] = "Points";
	msglist_[5] = "Note";
	bool repeat(true);
	while(keepon_ && repeat){
		track_ = S_ST_EV;
		display_evaluation_criteria(eval_ptr);
		display_class_results(class_ptr,eval_ptr);
		select(class_ptr->list_students(),geo_s_stude_);
		/*TODO:(middle) display the number of students*/
		switch(track_[0]){
			case C_ST_EV[0]: //ENTER
				student_correct_evaluation(class_ptr,eval_ptr);
				break;
			case S_EX_EV[0]: //TAB
				evaluation_select_exercise(class_ptr,eval_ptr);
				break;
			case E_ST_EV[0]: //F2
				student_edit_evaluation(class_ptr,eval_ptr);
				break;
			case R_ST_GR[0]: //F3
				student_remove_grade(class_ptr,eval_ptr,class_ptr->get_student(select_[S_ST_EV[0]]));
				break;
			case M_ST_GR[0]: //F5
				student_mail_grade(class_ptr,eval_ptr);
				break;
			case S_ST_EV[0]:
				break;
			default:
				repeat = false;
		}
	}
}

/*C_ST_EV*/
void NCInterface::student_correct_evaluation(std::shared_ptr<Class> const& class_ptr, std::shared_ptr<Evaluation> const& eval_ptr){
	unsigned int const s(select_[S_ST_EV[0]]);
	if(eval_ptr->get_type() == TestOnSkillsWB::TYPE || eval_ptr->get_type() == TestOnSkills::TYPE){
		student_correct_evaluation(class_ptr, eval_ptr, std::dynamic_pointer_cast<OnSkills>(eval_ptr));
		class_ptr->get_student(s)->compute_average();
		/*TODO:(high) this doesn't seem to have an effect on the display*/
		eval_ptr->compute_evaluation_stats();
		class_ptr->compute_year_average();
	} else {
		unsigned int const e(select_[S_EVALU[0]]);
		unsigned int const n_data(eval_ptr->get_n_criteria_to_fill());
		if(n_data){
			fill_row_dis_[1] = geo_s_exerc_[1]*n_data;
			fill_row_dis_[2] = s+geo_s_exerc_[2];
			fill_row_dis_[4] = geo_s_exerc_[1];

			std::shared_ptr<GenericGrade> grade_ptr((*class_ptr)(s,e));
			std::vector<FIELD*> fields(n_data+1);
			for(unsigned int i(0);i<n_data;i++){
				fields[i] = new_field(1,fw_, 0, i*geo_s_exerc_[1], 0, 0);
				set_field_opts(fields[i], O_VISIBLE | O_PUBLIC | O_EDIT | O_ACTIVE | O_BLANK | O_STATIC);
				set_field_type(fields[i], TYPE_NUMERIC, 2, 0.0, eval_ptr->get_max_criteria_to_fill(i));
				set_field_back(fields[i], A_UNDERLINE);
				set_field_just(fields[i], JUSTIFY_RIGHT);
				grade_ptr?
					set_field_buffer(fields[i], 0, my::tostring((*grade_ptr)(i)).c_str()):
					set_field_buffer(fields[i], 0, "");
			}
			fields.back() = NULL;

			if(fill_form(fields,fill_row_dis_,"Points")){
				double tmp;
				if(grade_ptr){
					for(unsigned int i(0);i<n_data;i++){
						if(!my::string2type(field_buffer(fields[i],0),tmp)){ tmp = 0.0; }
						mvwprintw(win_interactive_[3], fill_row_dis_[2], geo_s_exerc_[3]+i*geo_s_exerc_[1], "%5.2f",tmp);
						(*grade_ptr)(i) = tmp;
					}
				} else {
					switch(eval_ptr->get_type()){
						case Test::TYPE:
							grade_ptr = std::make_shared<Test::Grade>(class_ptr->get_student(s),eval_ptr,n_data);
							break;
						case TestWB::TYPE:
							grade_ptr = std::make_shared<TestWB::Grade>(class_ptr->get_student(s),eval_ptr,n_data-1);
							break;
						case Note::TYPE:
							grade_ptr = std::make_shared<Note::Grade>(class_ptr->get_student(s),eval_ptr);
							break;
					}
					for(unsigned int i(0);i<n_data;i++){
						if(!my::string2type(field_buffer(fields[i],0),tmp)){ tmp = 0.0; }
						mvwprintw(win_interactive_[3], fill_row_dis_[2], geo_s_exerc_[3]+i*geo_s_exerc_[1], "%5.2f",tmp);
						(*grade_ptr)(i) = tmp;
					}
					class_ptr->add_grade(s,e,grade_ptr);
				}
				grade_ptr->compute_grade();
				class_ptr->get_student(s)->compute_average();
				eval_ptr->compute_evaluation_stats();
				class_ptr->compute_year_average();

				mvwprintw(win_interactive_[5], fill_row_dis_[2], 0, "%5.1f",grade_ptr->get_grade());
				wnoutrefresh(win_interactive_[5]);

				modified_ = true;
			} else {
				if(grade_ptr){
					for(unsigned int i(0);i<n_data;i++){
						mvwprintw(win_interactive_[3], fill_row_dis_[2], geo_s_exerc_[3]+i*geo_s_exerc_[1], "%5.2f",(*grade_ptr)(i));
					}
				}
			}

			for(auto& f:fields){ free_field(f); }
			wnoutrefresh(win_interactive_[3]);
		}
	}
}

/*C_ST_EV*/
void NCInterface::student_correct_evaluation(std::shared_ptr<Class> const& class_ptr, std::shared_ptr<Evaluation> const& eval_ptr, std::shared_ptr<OnSkills> const& onskills_ptr){
	unsigned int const st(select_[S_ST_EV[0]]);
	unsigned int const ev(select_[S_EVALU[0]]);
	unsigned int const n_exercise(onskills_ptr->get_n_exercise());
	std::shared_ptr<GradeOnSkills> grade_ptr(std::dynamic_pointer_cast<GradeOnSkills>((*class_ptr)(st,ev)));
	bool new_grade(false);
	switch(eval_ptr->get_type()){
		case TestOnSkills::TYPE:
			if(grade_ptr){ grade_ptr = std::dynamic_pointer_cast<TestOnSkills::Grade>(grade_ptr); }
			else {
				grade_ptr = std::make_shared<TestOnSkills::Grade>(class_ptr->get_student(st),eval_ptr);
				new_grade = true;
			}
			break;
		case TestOnSkillsWB::TYPE:
			if(grade_ptr){ grade_ptr = std::dynamic_pointer_cast<TestOnSkillsWB::Grade>(grade_ptr); }
			else {
				grade_ptr = std::make_shared<TestOnSkillsWB::Grade>(class_ptr->get_student(st),eval_ptr);
				new_grade = true;
			}
			{
				std::shared_ptr<TestWB::Grade> bonus_ptr(std::dynamic_pointer_cast<TestWB::Grade>(grade_ptr));
				fill_row_dis_[1] = geo_s_exerc_[1];
				fill_row_dis_[2] = st+geo_s_exerc_[2];
				fill_row_dis_[4] = geo_s_exerc_[1];

				std::vector<FIELD*> fields(2);
				fields[0] = new_field(1, fw_, 0, 0, 0, 0);
				set_field_opts(fields[0], O_VISIBLE | O_PUBLIC | O_EDIT | O_ACTIVE | O_BLANK | O_STATIC);
				set_field_type(fields[0], TYPE_NUMERIC, 2, 0.0, std::dynamic_pointer_cast<TestOnSkillsWB>(eval_ptr)->get_max_bonus_point());
				set_field_back(fields[0], A_UNDERLINE);
				set_field_just(fields[0], JUSTIFY_RIGHT);
				set_field_buffer(fields[0], 0, my::tostring(bonus_ptr->get_bonus_point()).c_str());
				fields[1] = NULL;

				if(fill_form(fields,fill_row_dis_,"Bonus")){
					double tmp;
					if(!my::string2type(field_buffer(fields[0],0),tmp)){ tmp = 0.0; }
					mvwprintw(win_interactive_[3], fill_row_dis_[2], geo_s_exerc_[3], "%5.2f",tmp);
					(*bonus_ptr)(0) = tmp;
					modified_ = true;

					if(new_grade){
						new_grade = false;
						class_ptr->add_grade(st,ev,std::dynamic_pointer_cast<GenericGrade>(grade_ptr));
						grade_ptr->compute_grade();
					}
				}
				for(auto& f:fields){ free_field(f); }
			}
			break;
	}

	if(n_exercise){
		unsigned int idx(0);
		std::vector<WINDOW*> sel_win(n_exercise);
		for(auto& s:sel_win){
			s = derwin(win_interactive_[1],
					geo_s_repcr_[0],
					geo_s_repcr_[1],
					geo_s_repcr_[2]+idx*geo_s_repcr_[0]*geo_s_repcr_[4],
					geo_s_repcr_[3]+idx*geo_s_repcr_[1]*geo_s_repcr_[5]);
			idx++;
		}
		idx = 0;

		std::shared_ptr<GradeOnSkills::Exercise> exercise_grade_ptr;
		std::shared_ptr<OnSkills::Exercise> exercise_ptr;
		msglist_[3] = "Indicateurs";
		msglist_[5] = "";
		bool repeat(true);
		while(keepon_ && repeat){
			exercise_grade_ptr = grade_ptr->get_exercise_ptr(idx);
			exercise_ptr = exercise_grade_ptr->get_exercise_ptr();

			WINDOW* win(win_interactive_[track_[1]]);
			wclear(win);
			wclear(win_interactive_[1]);
			set_window_frame(win_interactive_[1],msglist_[1],false);

			std::vector<ITEM*> my_items(OnSkills::N_SKILL+1);
			for(unsigned int s(0);s<OnSkills::N_SKILL;s++){
				my_items[s] = new_item(exercise_ptr->get_skill(s)->get_name().c_str(),"");
			}
			my_items[OnSkills::N_SKILL] = NULL;

			MENU* my_menu(new_menu(&my_items[0]));
			set_menu_win(my_menu, win);
			set_menu_sub(my_menu, derwin(
						win, 
						OnSkills::N_SKILL, 
						geo_s_crind_[1], 
						geo_s_crind_[2],
						geo_s_crind_[3])
					);
			post_menu(my_menu);
			for(unsigned int i(0);i<n_exercise;i++){
				mvwprintw(sel_win[i], 0, 0, grade_ptr->get_exercise_point_info(i).c_str());
				wnoutrefresh(sel_win[i]);
			}
			display_exercise_fulfillment(exercise_grade_ptr,sel_win[idx],grade_ptr->get_exercise_point_info(idx));
			display_exercise_skill_indicators(exercise_ptr->get_skill(0));

			bool same_exercise(true);
			bool changed_fulfillment(false);
			int point_change[OnSkills::N_SKILL] = {0,0,0,0};
			unsigned int current_skill_idx(0);
			while(same_exercise){
				set_window_frame(win,msglist_[track_[1]],true);
				display_help();
				doupdate();

				unsigned int in(wgetch(win)); //ensure a correct cast
				switch(in){
					case 258: //DOWN
						menu_driver(my_menu, REQ_DOWN_ITEM);
						current_skill_idx = item_index(current_item(my_menu));
						display_exercise_skill_indicators(exercise_ptr->get_skill(current_skill_idx));
						break;
					case 259: //UP
						menu_driver(my_menu, REQ_UP_ITEM);
						current_skill_idx = item_index(current_item(my_menu));
						display_exercise_skill_indicators(exercise_ptr->get_skill(current_skill_idx));
						break;
					case 261: //RIGHT
						point_change[current_skill_idx] += exercise_grade_ptr->change_fulfillment(current_skill_idx,1);
						grade_ptr->compute_grade();
						display_exercise_fulfillment(exercise_grade_ptr,sel_win[idx],grade_ptr->get_exercise_point_info(idx));
						changed_fulfillment = true;
						break;
					case 260: //LEFT
						point_change[current_skill_idx] += exercise_grade_ptr->change_fulfillment(current_skill_idx,-1);
						grade_ptr->compute_grade();
						display_exercise_fulfillment(exercise_grade_ptr,sel_win[idx],grade_ptr->get_exercise_point_info(idx));
						changed_fulfillment = true;
						break;
					case 9: //TAB
						if( n_exercise>1 && (!changed_fulfillment || confirm("Voulez-vous validez les changements effectués et changer d'exercice?")) ){
							if(idx<n_exercise-1){ idx++; }
							else { idx = 0; }
							same_exercise = false;
							if(changed_fulfillment){ modified_ = true; }
						}
						break;
					case 353: //MAJTAB
						if( n_exercise>1 && (!changed_fulfillment || confirm("Voulez-vous validez les changements effectués et changer d'exercice?")) ){
							if(idx){ idx--; }
							else { idx = n_exercise-1; }
							same_exercise = false;
							if(changed_fulfillment){ modified_ = true; }
						}
						break;
					case 10: //ENTER
						if(!changed_fulfillment || confirm("Voulez-vous validez les changements effectués et changer d'élève?")){
							same_exercise = false;
							repeat = false;
							if(changed_fulfillment){ modified_ = true; }
							if(new_grade){ class_ptr->add_grade(st,ev,std::dynamic_pointer_cast<GenericGrade>(grade_ptr)); }
						}
						break;
					case KEY_BACKSPACE://263: //BACKSPACE
					case 127: /*for OSX*/
					case '\b': /*for OSX*/
						if(changed_fulfillment){
							if(confirm("Voulez-vous annuler les changements effectués sur les indicateurs",2)){
								for(unsigned int s(0);s<OnSkills::N_SKILL;s++){
									exercise_grade_ptr->change_fulfillment(s,-point_change[s]);
									point_change[s] = 0;
								}
								grade_ptr->compute_grade();
								display_exercise_fulfillment(exercise_grade_ptr,sel_win[idx],grade_ptr->get_exercise_point_info(idx));
								changed_fulfillment = false;
							}
						} else {
							same_exercise = false;
							repeat = false;
						}
						break;
					case 27: //ESC
						keepon_ = repeat = same_exercise = !((modified_ || changed_fulfillment)?confirm("Voulez-vous quitter et perdre les modifications?",2):confirm("Voulez-vous quitter?"));
						break;
					case KEY_F(1): case KEY_F(2): case KEY_F(3): case KEY_F(4):
					case KEY_F(5): case KEY_F(6): case KEY_F(7): case KEY_F(8):
					case KEY_F(9): case KEY_F(10): case KEY_F(11): case KEY_F(12):
						status("Action Fn inconnue");
						break;
				}
			}

			unpost_menu(my_menu);
			free_menu(my_menu);
			for(auto const& i:my_items){ free_item(i); }
		}

		wclear(win_interactive_[4]);
		set_window_frame(win_interactive_[4],msglist_[4],false);
		for(auto const& w:sel_win){ delwin(w); }
	} else { status("Le test ne contient aucun exercice"); }
}

/*E_ST_EV*/
void NCInterface::student_edit_evaluation(std::shared_ptr<Class> const& class_ptr, std::shared_ptr<Evaluation> const& eval_ptr){
	unsigned int const s(select_[S_ST_EV[0]]);
	if(s<class_ptr->get_nstudents()){
		std::shared_ptr<Student> student_ptr(class_ptr->get_student(s));
		if(confirm("Voulez-vous éditer l'évaluation pour " + student_ptr->get_name(true) + "?")){
			std::shared_ptr<GenericGrade> grade_ptr(student_ptr->get_grade(eval_ptr));
			grade_ptr->set_catch_up(confirm("Rattrapage?"));
			grade_ptr->set_recorded(confirm("Comptabilisé?"));
			eval_ptr->compute_evaluation_stats();
			class_ptr->update_class();
			modified_ = true;
		} else { status("Évaluation inchangée"); }
	} else { status("Étudiant non trouvé"); }
}

/*R_ST_GR*/
void NCInterface::student_remove_grade(std::shared_ptr<Class> const& class_ptr, std::shared_ptr<Evaluation> const& eval_ptr, std::shared_ptr<Student> const& student_ptr){
	if(confirm("Voulez-vous supprimer la note pour " + student_ptr->get_name(true) + "?")){
		if(class_ptr->remove_grade(student_ptr,eval_ptr)){
			modified_ = true;
		} else { status("Erreure fatale, voir '~/.jdgrades.log'"); }
	}
}

/*M_ST_GR*/
void NCInterface::student_mail_grade(std::shared_ptr<Class> const& class_ptr, std::shared_ptr<Evaluation> const& eval_ptr){
	unsigned int const s(select_[S_ST_EV[0]]);
	if(s<class_ptr->get_nstudents()){
		std::shared_ptr<Student> student_ptr(class_ptr->get_student(s));
		if(confirm("Voulez-vous emailer le résultat pour " + student_ptr->get_name(true) + "?")){
			processing("Veuillez patienter durant l'envoi du mail de résultat de " + student_ptr->get_name(true));
			student_ptr->get_grade(eval_ptr)->send_mail();
		} else { status("Aucun mail envoyé"); }
	} else { status("Étudiant non trouvé"); }
}

/*S_ST_SE*/
void NCInterface::student_select_for_given_session(std::shared_ptr<TP> const& tp_ptr){
	unsigned int const s(select_[S_TP_SE[0]]);
	std::shared_ptr<TP::Session> session_ptr(tp_ptr->get_session(s));
	std::vector<std::shared_ptr<Student> > students(session_ptr->get_students());
	std::shared_ptr<TP::Report> report_ptr;
	std::shared_ptr<TP::Report::Criteria> criteria_ptr;
	bool repeat(true);
	while(keepon_ && repeat){
		msglist_[1] = session_ptr->get_title(true,true);
		msglist_[5] = "Note";
		track_ = S_ST_SE;

		wclear(win_interactive_[1]);
		wclear(win_interactive_[3]);
		wclear(win_interactive_[5]);
		wnoutrefresh(win_interactive_[5]);
		unsigned int j(0);
		for(unsigned int c(0);c<4;c++){
			if(session_ptr->is_selected(c)){
				for(unsigned int i(0);i<students.size();i++){
					report_ptr = session_ptr->get_report_ptr(students[i]);
					criteria_ptr = report_ptr->get(c);
					if(criteria_ptr){
						mvwprintw(
								win_interactive_[3], 
								geo_s_exerc_[2]+i,
								geo_s_exerc_[3]+j*geo_s_exerc_[1],
								criteria_ptr->get_result_str(false,false).c_str() 
								);
					}
				}
				WINDOW* tmp;
				tmp = derwin(
						win_interactive_[1],
						geo_s_exerc_[0],
						geo_s_exerc_[1],
						geo_s_exerc_[2],
						geo_s_exerc_[3]+j*geo_s_exerc_[1]
						);
				mvwprintw(tmp, 0, 0, session_ptr->get_result_str(c).c_str());
				wnoutrefresh(tmp);
				j++;
			}
		}
		set_window_frame(win_interactive_[1],msglist_[1],false); //msglistok
		set_window_frame(win_interactive_[3],msglist_[3],false); //msglistok

		select(session_ptr->list_students(),geo_s_stuse_);
		switch(track_[0]){
			case C_TP_RE[0]: //ENTER
				tp_correct_report(session_ptr->get_report_ptr(students[select_[S_ST_SE[0]]]));
				break;
			case S_ST_WA[0]: //F2
				student_select_warning(students[select_[S_ST_SE[0]]],tp_ptr);
				break;
			case L_TP_RE[0]: //F4
				tp_latex_report(session_ptr->get_report_ptr(students[select_[S_ST_SE[0]]]));
				break;
			case S_ST_SE[0]:
				break;
			default:
				repeat = false;
		}
	}
}

/*S_ST_TP*/
void NCInterface::student_select_for_given_tp(std::shared_ptr<Class> const& class_ptr, std::shared_ptr<Evaluation> const& eval_ptr){
	std::shared_ptr<TP> tp_ptr(std::dynamic_pointer_cast<TP>(eval_ptr));
	bool repeat(true);
	while(keepon_ && repeat){
		msglist_[1] = "Sessions";
		msglist_[2] = "Élèves";
		track_ = S_ST_TP;
		display_tp_sessions(tp_ptr);
		display_class_results(class_ptr->get_students(),tp_ptr);
		select(class_ptr->list_students(),geo_s_stude_);
		switch(track_[0]){
			case S_TP_RE[0]: //ENTER
				tp_select_report(class_ptr);
				break;
			case S_ST_WA[0]: //F2
				student_select_warning(class_ptr->get_student(select_[S_ST_TP[0]]),tp_ptr);
				break;
			case R_ST_GR[0]: //F3
				student_remove_grade(class_ptr,eval_ptr,class_ptr->get_student(select_[S_ST_TP[0]]));
				break;
			case L_ST_TP[0]: //F4
				student_latex_tp(class_ptr->get_student(select_[S_ST_TP[0]]),tp_ptr);
				break;
			case S_ST_TP[0]:
				break;
			default:
				repeat = false;
		}
	}
}

/*L_ST_TP*/
void NCInterface::student_latex_tp(std::shared_ptr<Student> const& student_ptr, std::shared_ptr<TP> const& tp_ptr){
	std::shared_ptr<TP::Grade> grade_ptr(std::dynamic_pointer_cast<TP::Grade>(tp_ptr->get_grade(student_ptr)));
	if(grade_ptr){
		if(confirm("Voulez-vous générer un PDF contenant toutes les évaluations des rapports de " + student_ptr->get_name(true) + "?")){
			processing("Veuillez patienter durant la création du PDF contenant toutes les évaluations des rapports de " + student_ptr->get_name(true));
			if(grade_ptr->student_feedback(year_->get_io_path(),"TP-rapports-eleve")){ status("Problème durant la compilation"); }
			else { status("PDF crée"); }
		}
	} else { status("L'élève n'a pas de note de TP"); }
}
/*}*/

/*{Handle evaluation*/
bool NCInterface::evaluation_configure(std::shared_ptr<Class> const& class_ptr, std::shared_ptr<Note> const& eval_ptr, bool const& add_new_evaluation){
	std::string title(eval_ptr->get_title());
	Date date(eval_ptr->get_date());
	double weight(eval_ptr->get_weight());

	std::vector<FIELD*> fields;
	unsigned int geometry[4];
	geometry[0] = 1;
	geometry[1] = 20;
	geometry[2] = 1;
	geometry[3] = 1;

	add_text_form(fields,"Titre",title,geometry);
	geometry[3] += geometry[1]+1;
	add_date_form(fields,date,geometry);
	geometry[3] += geometry[1]+1;
	add_double_form(fields,"Poids",weight,0,5,0,geometry);
	fields.push_back(NULL);

	geometry[0] = 3;
	geometry[1]+= geometry[3];
	geometry[3] = 1;

	if(fill_form(fields,geometry,"Configuration de l'évaluation")){
		title = my::trim(field_buffer(fields[1],0));
		date.set(
				my::string2type<unsigned int>(field_buffer(fields[3],0)),
				my::string2type<unsigned int>(field_buffer(fields[5],0)),
				my::string2type<unsigned int>(field_buffer(fields[7],0))
				);
		my::string2type(field_buffer(fields[9],0),weight);

		eval_ptr->edit(title,date,weight);
		eval_ptr->update_results();
		if(add_new_evaluation){ class_ptr->add_evaluation(eval_ptr); }
		else { class_ptr->compute_year_average(); }
		class_ptr->sort_evaluations();
		modified_ = true;
		return true;
	}
	return true;
}

bool NCInterface::evaluation_configure(std::shared_ptr<Class> const& class_ptr, std::shared_ptr<Test> const& eval_ptr, bool const& add_new_evaluation){
	std::string title(eval_ptr->get_title());
	Date date(eval_ptr->get_date());
	double deducted_points(eval_ptr->get_deducted_points());
	Vector<double> max_points(eval_ptr->get_max_points());
	std::string points_per_exercise(max_points.size()?my::tostring(max_points(0)):"");
	for(unsigned int i(1);i<max_points.size();i++){ points_per_exercise += "," + my::tostring(max_points(i)); }
	double weight(eval_ptr->get_weight());

	std::vector<FIELD*> fields;
	unsigned int geometry[4];
	geometry[0] = 1;
	geometry[1] = 20;
	geometry[2] = 1;
	geometry[3] = 1;

	add_text_form(fields,"Titre",title,geometry);
	geometry[3] += geometry[1]+1;
	add_date_form(fields,date,geometry);
	geometry[3] += geometry[1]+1;
	add_double_form(fields,"Points de déduction", deducted_points,0,99.99,0,geometry);
	geometry[3] += geometry[1]+1;
	add_text_form(fields,"Points par exercice",points_per_exercise,geometry);
	geometry[3] += geometry[1]+1;
	add_double_form(fields,"Poids",weight,0,5,0,geometry);
	fields.push_back(NULL);

	geometry[0] = 3;
	geometry[1]+= geometry[3]+1;
	geometry[3] = 1;

	if(fill_form(fields,geometry,"Configuration de l'évaluation")){
		title = my::trim(field_buffer(fields[1],0));
		date.set(
				my::string2type<unsigned int>(field_buffer(fields[3],0)),
				my::string2type<unsigned int>(field_buffer(fields[5],0)),
				my::string2type<unsigned int>(field_buffer(fields[7],0))
				);
		my::string2type(field_buffer(fields[9],0),deducted_points);
		points_per_exercise = my::trim(field_buffer(fields[11],0));
		my::string2type(field_buffer(fields[13],0),weight);

		std::vector<std::string> v_tmp(my::string_split(points_per_exercise,','));
		unsigned int const n_exercise(v_tmp.size());
		if(add_new_evaluation || n_exercise == max_points.size() || confirm("Voulez-vous vraiment valider et effacer tous les points pour cette évaluation?",2)){
			max_points.set(n_exercise);
			for(unsigned int i(0);i<n_exercise;i++){ my::string2type(v_tmp[i],max_points(i)); }
		}

		eval_ptr->edit(title,date,weight,deducted_points,max_points);
		bool noesc(true);
		if(eval_ptr->get_type() == TestOnSkills::TYPE){
			noesc = evaluation_configure(std::dynamic_pointer_cast<TestOnSkills>(eval_ptr),add_new_evaluation);
		}
		if(noesc){
			eval_ptr->update_results();
			if(add_new_evaluation){ class_ptr->add_evaluation(eval_ptr); }
			else { class_ptr->compute_year_average(); }
			class_ptr->sort_evaluations();
			modified_ = true;
			for(auto& f:fields){ free_field(f); }
			return true;
		}
	}
	for(auto& f:fields){ free_field(f); }
	return false;
}

bool NCInterface::evaluation_configure(std::shared_ptr<Class> const& class_ptr, std::shared_ptr<TestWB> const& eval_ptr, bool const& add_new_evaluation){
	std::string title(eval_ptr->get_title());
	Date date(eval_ptr->get_date());
	double deducted_points(eval_ptr->get_deducted_points());
	double bonus_coef(eval_ptr->get_bonus_coef());
	double max_bonus_point(eval_ptr->get_max_bonus_point());
	Vector<double> max_points(eval_ptr->get_max_points());
	std::string points_per_exercise(max_points.size()?my::tostring(max_points(0)):"");
	for(unsigned int i(1);i<max_points.size();i++){ points_per_exercise += "," + my::tostring(max_points(i)); }
	double weight(eval_ptr->get_weight());

	std::vector<FIELD*> fields;
	unsigned int geometry[4];
	geometry[0] = 1;
	geometry[1] = 20;
	geometry[2] = 1;
	geometry[3] = 1;

	add_text_form(fields,"Titre",title,geometry);
	geometry[3] += geometry[1]+1;
	add_date_form(fields,date,geometry);
	geometry[3] += geometry[1]+1;
	add_double_form(fields,"Points de déduction", deducted_points,0,99.99,0,geometry);
	geometry[3] += geometry[1]+1;
	add_double_form(fields,"Coefficient de bonus [0,1]",bonus_coef,0.0,1.0,0,geometry);
	geometry[3] += geometry[1]+1;
	add_double_form(fields,"Points au bonus",max_bonus_point,0.0,99.99,0,geometry);
	geometry[3] += geometry[1]+1;
	add_text_form(fields,"Points par exercice",points_per_exercise,geometry);
	geometry[3] += geometry[1]+1;
	add_double_form(fields,"Poids",weight,0,5,0,geometry);
	fields.push_back(NULL);

	geometry[0] = 3;
	geometry[1]+= geometry[3]+1;
	geometry[3] = 1;

	if(fill_form(fields,geometry,"Configuration de l'évaluation")){
		title = my::trim(field_buffer(fields[1],0));
		date.set(
				my::string2type<unsigned int>(field_buffer(fields[3],0)),
				my::string2type<unsigned int>(field_buffer(fields[5],0)),
				my::string2type<unsigned int>(field_buffer(fields[7],0))
				);
		my::string2type(field_buffer(fields[9],0),deducted_points);
		my::string2type(field_buffer(fields[11],0),bonus_coef);
		my::string2type(field_buffer(fields[13],0),max_bonus_point);
		points_per_exercise = my::trim(field_buffer(fields[15],0));
		my::string2type(field_buffer(fields[17],0),weight);

		std::vector<std::string> v_tmp(my::string_split(points_per_exercise,','));
		unsigned int const n_exercise(v_tmp.size());
		if(add_new_evaluation || n_exercise == max_points.size() || confirm("Voulez-vous vraiment valider et effacer tous les points pour cette évaluation?",2)){
			max_points.set(n_exercise);
			for(unsigned int i(0);i<n_exercise;i++){ my::string2type(v_tmp[i],max_points(i)); }
		}

		eval_ptr->edit(title,date,weight,deducted_points,bonus_coef,max_bonus_point,max_points);
		bool noesc(true);
		if(eval_ptr->get_type() == TestOnSkillsWB::TYPE){
			noesc = evaluation_configure(std::dynamic_pointer_cast<OnSkills>(eval_ptr),add_new_evaluation);
		}

		if(noesc){
			eval_ptr->update_results();
			if(add_new_evaluation){ class_ptr->add_evaluation(eval_ptr); }
			else { class_ptr->compute_year_average(); }
			class_ptr->sort_evaluations();
			modified_ = true;
			for(auto& f:fields){ free_field(f); }
			return true;
		}
	}
	for(auto& f:fields){ free_field(f); }
	return false;
}

bool NCInterface::evaluation_configure(std::shared_ptr<OnSkills> const& eval_ptr, bool const& add_new_evaluation){
	unsigned int i(0);
	unsigned int geometry[4];
	for(auto const& exercise_ptr:eval_ptr->get_exercises()){
		std::vector<FIELD*> fields;
		wclear(win_interactive_[4]);
		geometry[0] = 1;
		geometry[1] = 10;
		geometry[2] = 1;
		geometry[3] = 1;

		std::vector<Choice> edit_skill_indicator;
		for(unsigned int s(0);s<OnSkills::N_SKILL;s++){
			add_double_form(fields, exercise_ptr->get_skill(s)->get_name(), exercise_ptr->get_skill(s)->get_weight(), 0, 99, 0, geometry);
			geometry[3]+= geometry[1]+1;
			edit_skill_indicator.push_back(Choice(exercise_ptr->get_skill(s)->get_name(),add_new_evaluation));
		}
		add_choices_form(fields,"Indicateur à éditer",edit_skill_indicator,true,geometry);
		fields.push_back(NULL);

		geometry[0] = 3;
		geometry[1]+= geometry[3]+1;
		geometry[3] = 1;

		if(fill_form(fields,geometry,"Configuration du poids des compétences de l'exercice " + my::tostring(++i))){
			for(unsigned int s(0);s<OnSkills::N_SKILL;s++){
				double weight(my::string2type<double>(field_buffer(fields[2*s+1],0)));
				std::shared_ptr<TestOnSkillsWB::Exercise::Skill> skill_ptr(exercise_ptr->get_skill(s));
				if(!my::are_equal(skill_ptr->get_weight(),weight)){
					skill_ptr->set_weight(weight);
					modified_ = true;
				}

				if(edit_skill_indicator[s].sel()){
					track_ = S_EX_IN; //TODO:(low) may be useless here, likewise for S_TP_IN
					bool repeat(true);
					msglist_[track_[1]] = "Indicateurs pour " + skill_ptr->get_name();
					while(keepon_ && repeat){
						track_ = S_EX_IN;
						/*TODO:(low) inform when there is no indicator
						  if(!skill_ptr->get_n_indicator()){
						  mvwprintw(win_interactive_[track_[1]], 2, 6,"Aucun indicateur n'est actuellement présent,");
						  mvwprintw(win_interactive_[track_[1]], 3, 6,"appuyez sur F1 pour en ajouter.");
						  }*/
						/*TODO:(current) bugs when ESC is pressed (other actions are also undefined)*/
						select(skill_ptr->list_indicators(),geo_s_crind_);
						switch(track_[0]){
							case A_EX_IN[0]:
								exercise_add_indicator(skill_ptr);
								break;
							case E_EX_IN[0]:
								exercise_edit_indicator(skill_ptr);
								break;
							case R_EX_IN[0]:
								exercise_remove_indicator(skill_ptr);
								break;
							case S_EX_IN[0]:
								break;
							default:
								repeat = false;
						}
					}
				}
			}
			exercise_ptr->sum_weight();
			for(auto& f:fields){ free_field(f); }
		} else {
			for(auto& f:fields){ free_field(f); }
			return false;
		}
	}
	return true;
}

void NCInterface::exercise_add_indicator(std::shared_ptr<OnSkills::Exercise::Skill> const& skill_ptr){
	if(confirm("Voulez-vous ajouter un indicateur?")){
		std::vector<FIELD*> fields;
		unsigned int geometry[4];
		geometry[0] = 1;
		geometry[1] = 1;
		geometry[2] = 1;
		geometry[3] = 1;

		add_int_form(fields,"Numéro",skill_ptr->get_n_indicator(),0,skill_ptr->get_n_indicator(),1,0,geometry);
		geometry[3]+= geometry[1]+1;
		geometry[1] = geo_s_crind_[1];
		add_text_form(fields,"Indicateur","",geometry);
		fields.push_back(NULL);

		geometry[0] = 3;
		geometry[1]+= geometry[3];
		geometry[3] = 1;

		if(fill_form(fields,geometry,"Ajout d'un indicateur")){
			if(skill_ptr->add_indicator(my::string2type<unsigned int>(field_buffer(fields[1],0)),my::trim(field_buffer(fields[3],0)))){
				status("Indicateur correctement ajouté");
				modified_ = true;
			} else { status("Ne peut pas avoir deux indicateurs identiques"); }
		}
		for(auto& f:fields){ free_field(f); }
	} else { status("Indicateur non ajouté"); }
}

void NCInterface::exercise_edit_indicator(std::shared_ptr<OnSkills::Exercise::Skill> const& skill_ptr){
	if(skill_ptr && skill_ptr->get_n_indicator()){
		if(confirm("Voulez-vous éditer l'indicateur sélectionné?")){
			unsigned int const& idx(select_[S_EX_IN[0]]);
			unsigned int geometry[4];
			geometry[0] = 1;
			geometry[1] = 1;
			geometry[2] = 1;
			geometry[3] = 1;

			std::vector<FIELD*> fields;
			add_int_form(fields,"Numéro",idx,0,skill_ptr->get_n_indicator(),1,0,geometry);
			geometry[3]+= geometry[1]+1;
			geometry[1] = geo_s_crind_[1];
			add_text_form(fields,"Indicateur",skill_ptr->get_indicator_text(idx),geometry);
			fields.push_back(NULL);

			geometry[0] = 3;
			geometry[1]+= geometry[3];
			geometry[3] = 1;

			if(fill_form(fields,geometry,"Edition de l'indicateur")){
				if(skill_ptr->edit_indicator(idx,my::string2type<unsigned int>(field_buffer(fields[1],0)),my::trim(field_buffer(fields[3],0)))){
					status("Indicateur correctement édité");
					modified_ = true;
				} else { status("Ne peut pas avoir deux indicateurs identiques"); }
			} else { status("Édition de l'indicateur annulée"); }
			for(auto& f:fields){ free_field(f); }
		} else { status("Indicateur non édité"); }
	} else { status("Aucun indicateur à éditer"); }
}

void NCInterface::exercise_remove_indicator(std::shared_ptr<OnSkills::Exercise::Skill> const& skill_ptr){
	if(skill_ptr && skill_ptr->get_n_indicator()){
		if(confirm("Voulez-vous supprimer l'indicateur sélectionné?")){
			skill_ptr->remove_indicator(select_[S_EX_IN[0]]);
			select_[S_EX_IN[0]] = 0;
			modified_ = true;
			status("Indicateur supprimé");
		} else { status("Indicateur non supprimé"); }
	} else { status("Aucun indicateur à supprimer"); }
}

/*A_EVALU*/
void NCInterface::evaluation_add(std::shared_ptr<Class> const& class_ptr){
	if(confirm("Voulez-vous ajouter une évaluation?")){
		msglist_[4] = "Ajout d'une évaluation";
		bool repeat(true);
		while(keepon_ && repeat){
			track_ = A_EVALU;
			select(Year::NAMES,geo_a_evalu_);
			switch(track_[0]){
				case E_AD_EV[0]:
					switch(Year::TYPES[select_[A_EVALU[0]]]){
						case Test::TYPE:
							if(evaluation_configure(class_ptr,std::make_shared<Test>(),true)){
								repeat = false;
							} else { status("Évaluation non ajoutée"); }
							break;
						case TestWB::TYPE:
							if(evaluation_configure(class_ptr,std::make_shared<TestWB>(),true)){
								repeat = false;
							} else { status("Évaluation non ajoutée"); }
							break;
						case TestOnSkills::TYPE:
							if(evaluation_configure(class_ptr,std::make_shared<TestOnSkills>(),true)){
								repeat = false;
							} else { status("Évaluation non ajoutée"); }
							break;
						case TestOnSkillsWB::TYPE:
							if(evaluation_configure(class_ptr,std::make_shared<TestOnSkillsWB>(),true)){
								repeat = false;
							} else { status("Évaluation non ajoutée"); }
							break;
						case Note::TYPE:
							if(evaluation_configure(class_ptr,std::make_shared<Note>(),true)){
								repeat = false;
							} else { status("Évaluation non ajoutée"); }
							break;
						case TP::TYPE:
							if(evaluation_add_tp(class_ptr)){
								repeat = false;
							} else { status("TP non ajouté"); }
					} break;
				case A_EVALU[0]:
					break;
				default:
					repeat = false;
			}
		}
		wclear(win_interactive_[4]);
		msglist_[4] = "Saisie";
		set_window_frame(win_interactive_[4],msglist_[4],false);
	}
}

bool NCInterface::evaluation_add_tp(std::shared_ptr<Class> const& class_ptr){
	std::vector<FIELD*> fields;
	unsigned int geometry[4];
	geometry[0] = 1;
	geometry[1] = 80;
	geometry[2] = 1;
	geometry[3] = 1;

	add_text_form(fields,"Listes des indicateurs (chemin du fichier '.txt' )","",geometry);
	geometry[3] += geometry[1]+1;
	fields.push_back(NULL);

	geometry[0] = 3;
	geometry[1]+= geometry[3];
	geometry[3] = 1;

	wclear(win_interactive_[3]);
	set_window_frame(win_interactive_[3],"",false);
	mvwprintw(win_interactive_[3], 2, 2,"Sélectionnez ensuite le fichier contenant la liste des critères.");
	mvwprintw(win_interactive_[3], 3, 2,"Le nom du fichier doit avoir une extension '.txt'. Chaque ligne");
	mvwprintw(win_interactive_[3], 4, 2,"du fichier contient:");
	mvwprintw(win_interactive_[3], 6, 2,"     c|description de l'indicateur");
	mvwprintw(win_interactive_[3], 8, 2,"Le fichier doit respecter les conventions suivantes:");
	mvwprintw(win_interactive_[3],10, 2,"     + pas de ligne vide");
	mvwprintw(win_interactive_[3],11, 2,"     + pas d'espace autour des '|'");
	mvwprintw(win_interactive_[3],12, 2,"     + 'c' est le numéro du critère pour associé à l'indicateur");
	wnoutrefresh(win_interactive_[3]);

	if(fill_form(fields,geometry,"Configuration du TP")){
		std::string indicator_list(Linux().return_absolute(my::trim(field_buffer(fields[1],0))));

		std::shared_ptr<TP> tp_ptr(std::make_shared<TP>(year_->get_start()));
		switch(tp_ptr->set_indicators(indicator_list)){
			case 0:
				class_ptr->add_evaluation(tp_ptr);
				for(auto const& student:class_ptr->get_students()){
					class_ptr->add_grade(student,tp_ptr,std::make_shared<TP::Grade>(student,tp_ptr));
				}
				modified_ = true;
				for(auto& f:fields){ free_field(f); }
				return true;
				break;
			case 1:
				status("Le TP n'a pas été ajouté, le fichier donné doit avoir une extension '.txt'");
				break;
			case 2:
				status("Le TP n'a pas été ajouté, le fichier donné n'a pas pu être ouvert");
				break;
			case 3:
				status("Le TP n'a pas été ajouté, le numéro d'un critère est incorrect");
				break;
			case 4:
				status("Le TP n'a pas été ajouté, le fichier donné est mal formaté");
				break;
		}
	}
	for(auto& f:fields){ free_field(f); }
	return false;
}

/*E_EVALU*/
void NCInterface::evaluation_edit(std::shared_ptr<Class> const& class_ptr){
	if(select_[S_EVALU[0]]<class_ptr->get_nevals()){
		std::shared_ptr<Evaluation> eval_ptr(class_ptr->get_evaluation(select_[S_EVALU[0]]));
		if(confirm("Voulez-vous éditer " + eval_ptr->get_title() + "?")){
			switch(eval_ptr->get_type()){
				case Test::TYPE:
				case TestOnSkills::TYPE:
					if(!evaluation_configure(class_ptr,std::dynamic_pointer_cast<Test>(eval_ptr),false))
					{ status("Édition de " + eval_ptr->get_title() + " annulée"); }
					break;
				case TestWB::TYPE:
				case TestOnSkillsWB::TYPE:
					if(!evaluation_configure(class_ptr,std::dynamic_pointer_cast<TestWB>(eval_ptr),false))
					{ status("Édition de " + eval_ptr->get_title() + " annulée"); }
					break;
				case Note::TYPE:
					if(!evaluation_configure(class_ptr,std::dynamic_pointer_cast<Note>(eval_ptr),false))
					{ status("Édition de " + eval_ptr->get_title() + " annulée"); }
					break;
				case TP::TYPE:
					{
						std::shared_ptr<TP> tp_ptr(std::dynamic_pointer_cast<TP>(eval_ptr));
						std::vector<Choice> criteria = {Choice("1",false),Choice("2",false),Choice("3",false),Choice("4",false)};

						std::vector<FIELD*> fields;
						unsigned int geometry[4];
						geometry[0] = 1;
						geometry[1] = 20;
						geometry[2] = 1;
						geometry[3] = 1;

						add_double_form(fields,"Poids",tp_ptr->get_weight(),0,5,0,geometry);
						geometry[3] += geometry[1]+1;
						add_choices_form(fields,"Critères à éditer",criteria,true,geometry);
						fields.push_back(NULL);

						geometry[0] = 3;
						geometry[1]+= geometry[3];
						geometry[3] = 1;

						if(fill_form(fields,geometry,"Configuration du TP")){
							tp_ptr->set_weight(my::string2type<double>(field_buffer(fields[1],0)));
							for(unsigned int c(0);c<criteria.size();c++){
								if(criteria[c].sel()){
									track_ = S_TP_IN;
									if(confirm("Voulez-vous éditer les indicateurs du critère " + my::tostring(c+1))){
										bool repeat(true);
										msglist_[track_[1]] = "Indicateurs";
										while(keepon_ && repeat){
											track_ = S_TP_IN;
											select(tp_ptr->list_indicators(c),geo_s_crind_);
											switch(track_[0]){
												case A_TP_IN[0]:
													tp_add_indicator(c,tp_ptr);
													break;
												case E_TP_IN[0]:
													tp_edit_indicator(c,tp_ptr);
													break;
												case R_TP_IN[0]:
													tp_remove_indicator(c,tp_ptr);
													break;
												case S_TP_IN[0]:
													break;
												default:
													repeat = false;
											}
										}
										msglist_[track_[1]] = "Note";
									} else { status("Indicateur du critère " + my::tostring(c+1) + " inchangé"); }
								}
							}
							tp_ptr->update_results();
							class_ptr->compute_year_average();
							class_ptr->sort_evaluations();
							modified_ = true;
						} else { status("Édition de " + tp_ptr->get_title() + " annulée"); }
						for(auto& f:fields){ free_field(f); }
					} break;
			}
		} else { status("Évaluation non éditée"); }
	} else { status("Évaluation non trouvée"); }
}

/*R_EVALU*/
void NCInterface::evaluation_remove(std::shared_ptr<Class> const& class_ptr){
	if(select_[S_EVALU[0]]<class_ptr->get_nevals()){
		if(confirm("Voulez-vous supprimer " + class_ptr->get_evaluation(select_[S_EVALU[0]])->get_title() + "?")){
			class_ptr->remove_evaluation(select_[S_EVALU[0]]);
			select_[S_EVALU[0]] = 0;
			modified_ = true;
		} else { status("Évaluation non supprimée"); }
	} else { status("Évaluation non trouvée"); }
}

/*L_EVALU*/
void NCInterface::evaluation_latex(std::shared_ptr<Class> const& class_ptr){
	if(select_[S_EVALU[0]]<class_ptr->get_nevals()){
		std::shared_ptr<Evaluation> eval_ptr(class_ptr->get_evaluation(select_[S_EVALU[0]]));
		if(confirm("Voulez-vous générer un PDF contenant les résultats de l'évaluation " + eval_ptr->get_title() + "?")){
			processing("Veuillez patienter durant la création du PDF contenant les résultats de l'évaluation " + eval_ptr->get_title());
			if(eval_ptr->student_feedback(year_->get_io_path(),class_ptr->get_id())){ status("Problème durant la compilation"); }
			else { status("PDF crée"); }
		}
	} else { status("Évaluation non trouvée"); }
}

/*M_EVALU*/
void NCInterface::evaluation_mail(std::shared_ptr<Class> const& class_ptr){
	if(select_[S_EVALU[0]]<class_ptr->get_nevals()){
		std::shared_ptr<Evaluation> eval_ptr(class_ptr->get_evaluation(select_[S_EVALU[0]]));
		if(confirm("Voulez-vous emailer " + eval_ptr->get_title() + "?")){
			processing("Veuillez patienter durant l'envoi des mails pour le rapport de " + eval_ptr->get_title());
			int tmp(eval_ptr->send_mail());
			switch(tmp){
				case -2: status("Aucune note à envoyer"); break;
				case -1: status("Configuration mail non définie"); break;
				case  0: status("Envoi réussi"); break;
				default: status(my::tostring(tmp%10000) + " compilations et " + my::tostring(tmp/10000) + " envois ont échoués"); break;
			}
		}
	} else { status("Évaluation non trouvée"); }
}

/*S_EVALU*/
void NCInterface::evaluation_select(std::shared_ptr<Class> const& class_ptr){
	bool repeat(true);
	while(keepon_ && repeat){
		msglist_[1] = "Évaluations";
		msglist_[3] = "Note";
		msglist_[5] = "Moyenne";
		if(track_ != S_ST_OV){ display_grades(class_ptr); }
		track_ = S_EVALU;
		wclear(win_interactive_[2]);
		wclear(win_interactive_[5]);
		set_window_frame(win_interactive_[2],msglist_[2],false);
		set_window_frame(win_interactive_[5],msglist_[5],false);
		mvwprintwlist(win_interactive_[2],2,2,1,0,class_ptr->list_students());
		mvwprintwlist(win_interactive_[5],2,2,1,0,class_ptr->list_average());
		select(class_ptr->list_evaluations(geo_s_evalu_[1]-2),geo_s_evalu_);
		switch(track_[0]){
			case S_ST_EV[0]: //ENTER
				{
					if(class_ptr->get_nevals()){
						std::shared_ptr<Evaluation> eval_ptr(class_ptr->get_evaluation(select_[S_EVALU[0]]));
						if(eval_ptr->get_type() == TP::TYPE){
							track_ = S_TP_SE;
							tp_select_session(class_ptr,std::dynamic_pointer_cast<TP>(eval_ptr));
						} else { student_select_for_given_evaluation(class_ptr,eval_ptr); }
					} else { status("Aucune évaluation"); }
				}
				break;
			case S_ST_OV[0]: //TAB
				student_select_overview(class_ptr);
				break;
			case A_EVALU[0]: //F1
				evaluation_add(class_ptr);
				break;
			case E_EVALU[0]: //F2
				evaluation_edit(class_ptr);
				break;
			case R_EVALU[0]: //F3
				evaluation_remove(class_ptr);
				break;
			case L_EVALU[0]: //F4
				evaluation_latex(class_ptr);
				break;
			case M_EVALU[0]: //F5
				evaluation_mail(class_ptr);
				break;
			case S_EVALU[0]:
				break;
			default:
				repeat = false;
		}
	}
}

/*S_EX_EV*/
void NCInterface::evaluation_select_exercise(std::shared_ptr<Class> const& class_ptr, std::shared_ptr<Evaluation> const& eval_ptr){
	bool repeat(true);
	while(keepon_ && repeat){
		set_window_frame(win_interactive_[2],msglist_[2],false);
		track_ = S_EX_EV;
		display_class_results(class_ptr,eval_ptr);
		select(eval_ptr->list_evaluation_criteria(),geo_s_exerc_);
		/*TODO:(middle) allow F2 to edit OnSkills evaluation for given exercise*/
		switch(track_[0]){
			case C_EX_EV[0]: //ENTER
				evaluation_correct_exercise(class_ptr,eval_ptr);
				break;
			case S_EX_EV[0]:
				break;
			default:
				repeat = false;
		}
	}
}

/*C_EX_EV*/
void NCInterface::evaluation_correct_exercise(std::shared_ptr<Class> const& class_ptr, std::shared_ptr<Evaluation> const& eval_ptr){
	unsigned int const e(select_[S_EVALU[0]]);
	unsigned int const exo(select_[S_EX_EV[0]]);
	unsigned int const n_data(eval_ptr->get_n_criteria_to_fill());
	unsigned int const nst(class_ptr->get_nstudents());

	fill_col_dis_[0] = nst*geo_s_stude_[0];
	fill_col_dis_[1] = geo_s_exerc_[1];
	fill_col_dis_[3] = geo_s_exerc_[1]*exo+geo_s_exerc_[3];

	std::vector<FIELD*> fields(nst+1);
	for(unsigned int i(0);i<nst;i++){
		fields[i] = new_field(1,fw_, i*geo_s_stude_[0], 0, 0, 0);
		set_field_opts(fields[i], O_VISIBLE | O_PUBLIC | O_EDIT | O_ACTIVE | O_BLANK | O_STATIC);
		set_field_type(fields[i], TYPE_NUMERIC, 2, 0.0, eval_ptr->get_max_criteria_to_fill(exo));
		set_field_back(fields[i], A_UNDERLINE);
		set_field_just(fields[i], JUSTIFY_RIGHT);
		(*class_ptr)(i,e)?
			set_field_buffer(fields[i], 0, my::tostring((*(*class_ptr)(i,e))(exo)).c_str()):
			set_field_buffer(fields[i], 0, "");
	}
	fields.back() = NULL;

	if(fill_form(fields,fill_col_dis_,"Points")){
		double tmp;
		std::string s_tmp;
		for(unsigned int s(0);s<nst;s++){
			s_tmp = my::trim(field_buffer(fields[s],0));
			if(s_tmp != "" && my::string2type(s_tmp,tmp)){
				mvwprintw(win_interactive_[3], fill_col_dis_[2]+s*geo_s_stude_[0], fill_col_dis_[3],"%5.2f",tmp);
				std::shared_ptr<GenericGrade> grade_ptr((*class_ptr)(s,e));
				if(!grade_ptr){
					switch(eval_ptr->get_type()){
						case Test::TYPE:
							grade_ptr = std::make_shared<Test::Grade>(class_ptr->get_student(s),eval_ptr,n_data);
							break;
						case TestOnSkills::TYPE:
							grade_ptr = std::make_shared<TestOnSkills::Grade>(class_ptr->get_student(s),eval_ptr);
							break;
						case TestWB::TYPE:
							grade_ptr = std::make_shared<TestWB::Grade>(class_ptr->get_student(s),eval_ptr,n_data-1);
							break;
						case TestOnSkillsWB::TYPE:
							grade_ptr = std::make_shared<TestOnSkillsWB::Grade>(class_ptr->get_student(s),eval_ptr);
							break;
						case Note::TYPE:
							grade_ptr = std::make_shared<Note::Grade>(class_ptr->get_student(s),eval_ptr);
							break;
					}
					class_ptr->add_grade(s,e,grade_ptr);
				}
				(*grade_ptr)(exo) = tmp;
				grade_ptr->compute_grade();
			}
		}
		class_ptr->compute_student_average();
		eval_ptr->compute_evaluation_stats();
		class_ptr->compute_year_average();

		modified_ = true;
	} else {
		for(unsigned int i(0);i<nst;i++){
			if((*class_ptr)(i,e)){ mvwprintw(win_interactive_[3], fill_col_dis_[2]+i*geo_s_stude_[0], fill_col_dis_[3],"%5.2f",(*(*class_ptr)(i,e))(exo)); }
		}
	}

	for(auto& f:fields){ free_field(f); }
	wnoutrefresh(win_interactive_[3]);
}

/*{Handle TP*/
void NCInterface::tp_session_configure(std::string const& title, Date const& date, bool const& for_group_A, std::vector<Choice>& criteria, std::vector<FIELD*>& fields, unsigned int geometry[]){
	geometry[0] = 1;
	geometry[1] = 20;
	geometry[2] = 1;
	geometry[3] = 1;

	add_text_form(fields,"Titre",title,geometry);
	geometry[3] += geometry[1]+1;
	add_date_form(fields,date,geometry);
	geometry[3] += geometry[1]+1;
	add_list_form(fields,"Groupe [A-B]",{"B","A"},for_group_A,geometry);
	geometry[3] += geometry[1]+1;
	add_choices_form(fields,"Critères",criteria,true,geometry);
	fields.push_back(NULL);

	geometry[0] = 3;
	geometry[1]+= geometry[3]+1;
	geometry[3] = 1;
}

void NCInterface::tp_session_indicator_configure(unsigned int const& threshold, std::vector<std::shared_ptr<TP::Session::Indicator> > const& indicators, std::vector<FIELD*>& fields, unsigned int geometry[]){
	for(auto& f:fields){ free_field(f); }
	fields.clear();

	geometry[0] = 1;
	geometry[1] = 1;
	geometry[2] = 1;
	geometry[3] = 1;
	wclear(win_interactive_[3]);

	add_int_form(fields,"Nombre de 'bonnes' évaluations",threshold,0,indicators.size(),1,0,geometry);
	geometry[2] += 3;
	for(auto const& ind:indicators){
		add_int_form(fields,ind->get_text(),ind->get_half_weight(),0,10,1,4,geometry);
		geometry[2] += 1;
	}
	fields.push_back(NULL);

	geometry[0] = indicators.size()+5;
	geometry[1] = geo_s_crind_[1];
	geometry[2] = 1;
	geometry[3] = 1;
}

void NCInterface::tp_warning_configure(unsigned int const& criteria, std::vector<std::string> const& list_sessions, unsigned int const& current_choice, std::string const& comment, std::vector<FIELD*>& fields, unsigned int geometry[]){
	geometry[0] = 1;
	geometry[1] = 1;
	geometry[2] = 1;
	geometry[3] = 1;

	geometry[3] += geometry[1]+1;
	add_list_form(fields,"Critère",{"5","6"},criteria,geometry);
	geometry[3] += geometry[1]+1;
	add_list_form(fields,"Session [PgUp-PgDown]",list_sessions,current_choice,geometry);
	geometry[3] += geometry[1]+1;
	geometry[1] = 100;
	add_text_form(fields,"Motif",comment,geometry);
	fields.push_back(NULL);

	geometry[0] = 3;
	geometry[1]+= geometry[3];
	geometry[3] = 1;
}

void NCInterface::tp_add_criteria_session(unsigned int const& criteria, std::shared_ptr<TP> const& tp_ptr, std::shared_ptr<TP::Session> const& session_ptr){
	if(session_ptr->is_selected(criteria)){ status("Il n'est pas possible d'ajouter deux fois le même critère pour la même session"); }
	else {
		track_ = S_TP_IN;
		unsigned int geometry[4];
		std::vector<FIELD*> fields;
		/*TODO:(current) test adding, edditing, deleting indicator (seems ok)*/
		std::vector<std::shared_ptr<TP::Session::Indicator> > indicators;
		for(auto const& ind:tp_ptr->get_indicators(criteria)){
			indicators.push_back(std::make_shared<TP::Session::Indicator>(ind,1));
		}

		tp_session_indicator_configure(0, indicators, fields, geometry);
		if(fill_form(fields,geometry,"Configuration du critère " + my::tostring(criteria+1))){
			unsigned int i(0);
			for(auto& ind:indicators){
				/*TODO:(middle) could use atoi and atof for respectiverly int and double conversions*/
				ind->set_half_weight(my::string2type<unsigned int>(field_buffer(fields[3+2*i++],0)));
			}
			session_ptr->add(criteria, indicators);
			session_ptr->set_threshold(criteria,my::string2type<unsigned int>(field_buffer(fields[1],0)));
			modified_ = true;
		} else { status("Le critère " + my::tostring(criteria+1) + " n'a pas été ajouté"); }
		track_ = E_TP_SE;
	}
}

void NCInterface::tp_edit_criteria_session(unsigned int const& criteria, std::shared_ptr<TP::Session> const& session_ptr){
	track_ = S_TP_IN;
	set_window_frame(win_interactive_[1],msglist_[1],false);

	unsigned int geometry[4];
	std::vector<FIELD*> fields;
	tp_session_indicator_configure(session_ptr->get_threshold(criteria), session_ptr->get_indicators(criteria), fields, geometry);

	if(fill_form(fields,geometry,"Configuration du critère " + my::tostring(criteria+1))){
		unsigned int threshold(my::string2type<unsigned int>(field_buffer(fields[1],0)));
		if(threshold != session_ptr->get_threshold(criteria)){
			modified_ = true;
			session_ptr->set_threshold(criteria,threshold);
		}
		unsigned int i(0);
		double half_weight;
		for(auto& ind:session_ptr->get_indicators(criteria)){
			half_weight = my::string2type<unsigned int>(field_buffer(fields[3+2*i++],0));
			if(!my::are_equal(half_weight,ind->get_half_weight())){
				if(my::are_equal(0.0,ind->get_half_weight())){
					if(confirm("Voulez-vous ajouter l'indicateur '" + ind->get_text() + "' au critère " + my::tostring(criteria+1))){
						if(session_ptr->set_indicator(criteria,ind,half_weight)){ modified_ = true; }
						else { status("Ne peut pas avoir deux indicateurs identiques"); }
					} else { status("Indicateur non ajouté"); }
				} else if (my::are_equal(half_weight,0.0)) {
					if(confirm("Voulez-vous supprimer l'indicateur '" + ind->get_text() + "' du critère " + my::tostring(criteria+1),2)){
						if(session_ptr->set_indicator(criteria,ind,0.0)){ modified_ = true; }
						else { status("Erreure fatale, voir '~/.jdgrades.log'"); }
					} else { status("Indicateur non supprimé"); }
				} else {
					ind->set_half_weight(half_weight);
					modified_ = true;
				}
			}
		}
	} else { status("Critère inchangé"); }
	for(auto& f:fields){ free_field(f); }
	track_ = S_TP_SE;
}

/*A_TP_RE*/
void NCInterface::tp_add_report(std::shared_ptr<TP::Grade> const& grade_ptr){
	std::shared_ptr<TP> tp_ptr(std::dynamic_pointer_cast<TP>(grade_ptr->get_eval()));
	std::vector<std::string> no_duplicate_session_list;
	std::vector<std::shared_ptr<TP::Session> > no_duplicate_session;
	for(auto const& s:tp_ptr->get_sessions()){
		bool no_duplicate(true);
		for(auto const& g:grade_ptr->get_reports()){
			if(*s == *g->get_session()){
				no_duplicate = false;
				break;
			}
		}
		if(no_duplicate){
			no_duplicate_session.push_back(s);
			no_duplicate_session_list.push_back(s->get_title(true,true));
		}
	}

	if(no_duplicate_session.size()){
		track_ = A_TP_RE;
		/*otherwise it may suggest a non valid vector entry*/
		select_[A_TP_RE[0]] = 0;
		msglist_[4] = "Sélectionner le rapport à ajouter";
		select(no_duplicate_session_list,geo_s_stuse_);
		if(track_[0] == S_TP_RE[0]){
			std::shared_ptr<TP::Session> session_ptr(no_duplicate_session[select_[A_TP_RE[0]]]);
			std::shared_ptr<Student> student_ptr(grade_ptr->get_student());
			if(confirm("Voulez-vous ajouter un rapport pour la session '" + session_ptr->get_title(false,false) + "' pour " + student_ptr->get_name(true)+ "?")){
				if(grade_ptr->add_report(std::make_shared<TP::Report>(session_ptr,grade_ptr))){ modified_ = true; }
				else { status("Erreure fatale, voir '~/.jdgrades.log'"); }
			} else { status("La session n'a pas été ajoutée"); }
		}

		wclear(win_interactive_[4]);
		msglist_[4] = "Saisie";
		set_window_frame(win_interactive_[4],msglist_[4],false);
	} else { status("Cet élève à déjà rendu tous les rapports possibles"); }
}

/*R_TP_RE*/
void NCInterface::tp_remove_report(std::shared_ptr<TP::Grade> const& grade_ptr){
	std::shared_ptr<TP::Report> report_ptr(grade_ptr->get_report(select_[S_TP_RE[0]]));
	std::shared_ptr<TP::Session> session_ptr(report_ptr->get_session());
	if(confirm("Voulez-vous supprimer le rapport  '" + session_ptr->get_title(false,false) + "' pour " + grade_ptr->get_student()->get_name(true) + "?")){
		if(session_ptr->remove_report(report_ptr) && grade_ptr->remove_report(report_ptr)){
			select_[S_TP_RE[0]] = 0;
			modified_ = true;
		} else { status("Erreure fatale, voir '~/.jdgrades.log'"); }
	} else { status("Rapport non supprimé"); }
}

/*L_TP_RE*/
void NCInterface::tp_latex_report(std::shared_ptr<TP::Report> const& report_ptr){
	if(report_ptr){
		if(confirm("Voulez-vous générer un PDF contenant l'évaluation du rapport '" + report_ptr->get_session()->get_title(false,false) + "' pour " + report_ptr->get_student()->get_name(true) + "?")){
			processing("Veuillez patienter durant la création du PDF contenant l'évaluation du rapport '" + report_ptr->get_session()->get_title(false,false) + "' pour " + report_ptr->get_student()->get_name(true));
			if(report_ptr->feedback(year_->get_io_path(),"TP-rapport-eleve"))
			{ status("Problème durant la compilation"); }
			else { status("PDF crée"); }
		}
	} else { status("Erreure fatale, voir '~/.jdgrades.log'"); }
}

/*M_TP_RE*/
void NCInterface::tp_mail_report(std::shared_ptr<TP::Report> const& report_ptr){
	if(report_ptr){
		if(confirm("Voulez-vous envoyer l'évaluation du rapport '" + report_ptr->get_session()->get_title(false,true) + "' pour " + report_ptr->get_student()->get_name(true) + "?")){
			processing("Veuillez patienter durant l'envoi de l'évaluation du rapport '" + report_ptr->get_session()->get_title(false,true) + "' pour " + report_ptr->get_student()->get_name(true));
			switch(report_ptr->send_mail()){
				case -1: status("Configuration mail non définie"); break;
				case  0: status("Envoi réussi"); break;
				case  1: status("La compilation a échoué"); break;
				case  2: status("L'envoi à échoué"); break;
				default: status("Erreur inconnue"); break;
			}
		}
	} else { status("Erreure fatale, voir '~/.jdgrades.log'"); }
}

/*S_TP_RE*/
void NCInterface::tp_select_report(std::shared_ptr<Class> const& class_ptr){
	unsigned int const s(select_[S_ST_TP[0]]);
	unsigned int const e(select_[S_EVALU[0]]);

	std::shared_ptr<TP::Grade> grade_ptr(std::dynamic_pointer_cast<TP::Grade>((*class_ptr)(s,e)));
	if(!grade_ptr){
		if(confirm("Voulez-vous attribuer un note de TP à " + class_ptr->get_student(s)->get_name(true) + "?")){
			grade_ptr = std::make_shared<TP::Grade>(class_ptr->get_student(s),class_ptr->get_evaluation(e));
			class_ptr->add_grade(s,e,grade_ptr);
			modified_ = true;
		} else { status("Aucune note ajoutée"); }
	} else {
		wclear(win_interactive_[2]);
		msglist_[1] = "Critères";
		msglist_[2] = class_ptr->get_student(s)->get_name(true);
		set_window_frame(win_interactive_[2],msglist_[2],false);

		bool repeat(true);
		while(keepon_ && repeat){
			display_evaluation_criteria(grade_ptr->get_eval());
			display_tp_validated(grade_ptr);

			wclear(win_interactive_[5]);
			wnoutrefresh(win_interactive_[5]);

			track_ = S_TP_RE;
			select(grade_ptr->list_reports(),geo_s_tprep_);
			switch(track_[0]){
				case C_TP_RE[0]: //ENTER
					tp_correct_report(grade_ptr->get_report(select_[S_TP_RE[0]]));
					break;
				case A_TP_RE[0]: //F1
					tp_add_report(grade_ptr);
					break;
				case R_TP_RE[0]: //F3
					tp_remove_report(grade_ptr);
					break;
				case L_TP_RE[0]: //F4
					tp_latex_report(grade_ptr->get_report(select_[S_TP_RE[0]]));
					break;
				case M_TP_RE[0]: //F5
					tp_mail_report(grade_ptr->get_report(select_[S_TP_RE[0]]));
					break;
				case S_TP_RE[0]:
					break;
				default:
					repeat = false;
			}
			grade_ptr->compute_grade();
		}
	}
}

/*A_TP_IN*/
void NCInterface::tp_add_indicator(unsigned int const& criteria, std::shared_ptr<TP> const& tp_ptr){
	if(confirm("Voulez-vous ajouter un indicateur?")){
		std::vector<FIELD*> fields;
		unsigned int geometry[4];
		geometry[0] = 1;
		geometry[1] = 1;
		geometry[2] = 1;
		geometry[3] = 1;

		add_int_form(fields,"Numéro",0,0,tp_ptr->get_indicators(criteria).size(),1,0,geometry);
		geometry[3]+= geometry[1]+1;
		geometry[1] = geo_s_crind_[1];
		add_text_form(fields,"Indicateur","",geometry);
		fields.push_back(NULL);

		geometry[0] = 3;
		geometry[1]+= geometry[3];
		geometry[3] = 1;

		if(fill_form(fields,geometry,"Ajout d'un indicateur")){
			if(tp_ptr->add_indicator(criteria,my::string2type<unsigned int>(field_buffer(fields[1],0)),my::trim(field_buffer(fields[3],0)))){
				status("Indicateur correctement ajouté");
				modified_ = true;
			} else { status("Ne peut pas avoir deux indicateurs identiques"); }
		}
		for(auto& f:fields){ free_field(f); }

	} else { status("Aucun indicateur ajouté"); }
}

/*E_TP_IN*/
void NCInterface::tp_edit_indicator(unsigned int const& criteria, std::shared_ptr<TP> const& tp_ptr){
	if(confirm("Voulez-vous éditer l'indicateur sélectionné? Attention, cela affectera cet indicateur pour tous les rapports.",2)){
		unsigned int const& idx(select_[S_TP_IN[0]]);
		unsigned int geometry[4];
		geometry[0] = 1;
		geometry[1] = 1;
		geometry[2] = 1;
		geometry[3] = 1;

		std::vector<FIELD*> fields;
		add_int_form(fields,"Numéro",idx,0,tp_ptr->get_indicators(criteria).size()-1,1,0,geometry);
		geometry[3]+= geometry[1]+1;
		geometry[1] = geo_s_crind_[1];
		add_text_form(fields,"Indicateur",tp_ptr->get_indicator_text(criteria,idx),geometry);
		fields.push_back(NULL);

		geometry[0] = 3;
		geometry[1]+= geometry[3];
		geometry[3] = 1;

		if(fill_form(fields,geometry,"Edition de l'indicateur")){
			if(tp_ptr->edit_indicator(criteria,idx,my::string2type<unsigned int>(field_buffer(fields[1],0)),my::trim(field_buffer(fields[3],0)))){
				status("Indicateur correctement édité");
				modified_ = true;
			} else { status("Ne peut pas avoir deux indicateurs identiques"); }
		} else { status("Édition de l'indicateur annulée"); }
		for(auto& f:fields){ free_field(f); }
	}
}

/*R_TP_IN*/
void NCInterface::tp_remove_indicator(unsigned int const& criteria, std::shared_ptr<TP> const& tp_ptr){
	if(confirm("Voulez-vous supprimer l'indicateur sélectionné? Attention, cela supprimera cet indicateur pour toutes les rapports.",2)){
		if(tp_ptr->remove_indicator(criteria,select_[S_TP_IN[0]])){
			status("Indicateur supprimé");
			modified_ = true;
		} else { status("Erreure fatale, voir '~/.jdgrades.log'"); }
	} else { status("Indicateur non supprimé"); }
}

/*C_TP_RE*/
void NCInterface::tp_correct_report(std::shared_ptr<TP::Report> const& report_ptr){
	if(report_ptr){
		unsigned int n_criteria(report_ptr->get_n_criteria());
		if(n_criteria){
			auto set_criteria_windows=[&](std::vector<WINDOW*>& win, unsigned int const& n_criteria){
				if(win.size()){
					for(auto const& w:win){ delwin(w); }
					win.clear();
				}
				win.resize(n_criteria);
				unsigned int i(0);
				for(auto& w:win){
					w = derwin(win_interactive_[1],
							geo_s_repcr_[0],
							geo_s_repcr_[1],
							geo_s_repcr_[2]+i*geo_s_repcr_[0]*geo_s_repcr_[4],
							geo_s_repcr_[3]+i*geo_s_repcr_[1]*geo_s_repcr_[5]
							);
					i++;
				}
			};
			std::vector<WINDOW*> sel_win;
			set_criteria_windows(sel_win,n_criteria);

			msglist_[3] = "Indicateurs";
			msglist_[5] = "";
			bool repeat(true);
			unsigned int c_win_idx(0);
			while(keepon_ && repeat && n_criteria){
				std::shared_ptr<TP::Report::Criteria> criteria_ptr(report_ptr->get_by_idx(c_win_idx));
				std::vector<unsigned int> old_indicators;
				for(auto const& ind:criteria_ptr->get_indicators()){
					old_indicators.push_back(ind->get_mark());
				}

				WINDOW* win(win_interactive_[track_[1]]);
				wclear(win);
				wclear(win_interactive_[1]);
				set_window_frame(win_interactive_[1],msglist_[1],false);
				set_window_frame(win,msglist_[track_[1]],true);
				display_help();

				std::vector<ITEM*> my_items;
				for(auto const& ind:criteria_ptr->get_indicators()){
					my_items.push_back(new_item(ind->get_text().c_str(),""));
				}
				my_items.push_back(NULL);

				MENU* my_menu(new_menu(&my_items[0]));
				set_menu_win(my_menu, win);
				set_menu_sub(my_menu, derwin(
							win, 
							my_items.size()-1,
							geo_s_crind_[1],
							geo_s_crind_[2],
							geo_s_crind_[3])
						);
				post_menu(my_menu);
				for(unsigned int c(0);c<n_criteria;c++){
					mvwprintw(sel_win[c], 0,0, report_ptr->get_by_idx(c)->get_info().c_str());
					wnoutrefresh(sel_win[c]);
				}
				display_tp_criteria_points(criteria_ptr,sel_win[c_win_idx]);

				bool same_criteria(true);
				bool changed_indicators(false);
				while(same_criteria){
					unsigned int in(wgetch(win)); //ensure a correct cast
					switch(in){
						case 258: //DOWN
							menu_driver(my_menu, REQ_DOWN_ITEM);
							break;
						case 259: //UP
							menu_driver(my_menu, REQ_UP_ITEM);
							break;
						case 261: //RIGHT
							criteria_ptr->get_indicator(item_index(current_item(my_menu)))->up();
							criteria_ptr->compute_score();
							display_tp_criteria_points(criteria_ptr,sel_win[c_win_idx]);
							changed_indicators = true;
							break;
						case 260: //LEFT
							criteria_ptr->get_indicator(item_index(current_item(my_menu)))->down();
							criteria_ptr->compute_score();
							display_tp_criteria_points(criteria_ptr,sel_win[c_win_idx]);
							changed_indicators = true;
							break;
						case 9: //TAB
							if( n_criteria>1 && (!changed_indicators || confirm("Voulez-vous validez les changements effectués et changer de critère?")) ){
								if(c_win_idx<n_criteria-1){ c_win_idx++; }
								else { c_win_idx = 0; }
								same_criteria = false;
								if(changed_indicators){ modified_ = true; }
							}
							set_window_frame(win,msglist_[track_[1]],true);
							display_help();
							break;
						case 353: //MAJTAB
							if( n_criteria>1 && (!changed_indicators || confirm("Voulez-vous validez les changements effectués et changer de critère?")) ){
								if(c_win_idx){ c_win_idx--; }
								else { c_win_idx = n_criteria-1; }
								same_criteria = false;
								if(changed_indicators){ modified_ = true; }
							}
							set_window_frame(win,msglist_[track_[1]],true);
							display_help();
							break;
						case 10: //ENTER
							if(!changed_indicators || confirm("Voulez-vous validez les changements effectués et changer d'élève?")){
								same_criteria = false;
								repeat = false;
								if(changed_indicators){
									report_ptr->compute_grade();
									modified_ = true;
								}
							}
							set_window_frame(win,msglist_[track_[1]],true);
							display_help();
							break;
						case KEY_BACKSPACE://263: //BACKSPACE
						case 127: /*for OSX*/
						case '\b': /*for OSX*/
							if(changed_indicators){
								if(confirm("Voulez-vous annuler les changements effectués pour les indicateurs",2)){
									unsigned int i(0);
									for(auto const& ind:criteria_ptr->get_indicators()){
										ind->set_mark(old_indicators[i++]);
									}
									criteria_ptr->compute_score();
									display_tp_criteria_points(criteria_ptr,sel_win[c_win_idx]);
									changed_indicators = false;
								}
								set_window_frame(win,msglist_[track_[1]],true);
								display_help();
							} else {
								same_criteria = false;
								repeat = false;
							}
							break;
						case 27: //ESC
							keepon_ = repeat = same_criteria = !((modified_ || changed_indicators)?confirm("Voulez-vous quitter et perdre les modifications?",2):confirm("Voulez-vous quitter?"));
							set_window_frame(win,msglist_[track_[1]],true);
							display_help();
							break;
						case KEY_F(1): //To add a missing criteria to the report
							{
								std::shared_ptr<TP::Session> session_ptr(report_ptr->get_session());
								unsigned n_missing_criteria(0);
								for(unsigned int c(0);c<4;c++){
									if(session_ptr->is_selected(c) && !report_ptr->get(c)){ n_missing_criteria++; }
								}
								if(n_missing_criteria == 1 || (n_missing_criteria && confirm("Voulez-vous ajouter un des critères manquants à ce rapport?",3))){
									bool criteria_added(false);
									for(unsigned int c(0);c<4;c++){
										if(session_ptr->is_selected(c) && !report_ptr->get(c)){
											if(confirm("Voulez-vous ajouter le critère " + my::tostring(c+1) + " manquant?",3)){
												n_criteria++;
												report_ptr->add(c);
												modified_ = true;
												same_criteria = false;
												criteria_added = true;
											}
										}
									}
									if(criteria_added){ set_criteria_windows(sel_win,n_criteria); }
								} else { status("Aucun critère ajouté"); }
								set_window_frame(win,msglist_[track_[1]],true);
								display_help();
							}
							break;
						case KEY_F(2): //To add a comment
							tp_edit_criteria_comment(criteria_ptr);
							set_window_frame(win,msglist_[track_[1]],true);
							display_help();
							break;
						case KEY_F(3): //To remove a criteria to the report
							if(n_criteria){
								if(confirm("Voulez-vous supprimer le critère "+ my::tostring(criteria_ptr->get_criteria()+1) + " pour ce rapport?",2)){
									report_ptr->remove(criteria_ptr->get_criteria());
									n_criteria--;
									if(c_win_idx>=n_criteria){ c_win_idx = n_criteria-1; }
									same_criteria = false;
									modified_ = true;
									status("Critère supprimé");
								} else { status("Critère non supprimé"); }
								set_window_frame(win,msglist_[track_[1]],true);
								display_help();
							}
							break;
						case KEY_F(4): case KEY_F(5): case KEY_F(6):
						case KEY_F(7): case KEY_F(8): case KEY_F(9):
						case KEY_F(10): case KEY_F(11): case KEY_F(12):
							status("Action Fn inconnue");
							break;
					}
					doupdate();
				}

				unpost_menu(my_menu);
				free_menu(my_menu);
				for(auto const& i:my_items){ free_item(i); }
			}

			for(auto const& w:sel_win){ delwin(w); }
		} else { status("Aucun critère pour ce rapport"); }
	} else { status("Aucun rapport à évaluer"); }
}

/*E_CR_CO*/
void NCInterface::tp_edit_criteria_comment(std::shared_ptr<TP::Report::Criteria> const& criteria_ptr){
	if(confirm("Voulez-vous ajouter/modifier/supprimer le commentaire lié au critère " + my::tostring(criteria_ptr->get_criteria()+1) + "?")){
		status("Édition du commentaire");
		track_  = E_CR_CO;
		unsigned int geometry[4];
		geometry[0] = 1;
		geometry[1] = 100;
		geometry[2] = 1;
		geometry[3] = 1;

		std::vector<FIELD*> fields;
		add_text_form(fields,"Commentaire",criteria_ptr->get_comment(),geometry);

		fields.push_back(NULL);
		geometry[0] = 3;
		geometry[1]+= geometry[3];
		geometry[3] = 1;

		if(fill_form(fields,geometry,"")){
			criteria_ptr->set_comment(my::trim(field_buffer(fields[1],0)));
			modified_ = true;
			status("Commentaire mis à jour");
		} else { status("Commentaire inchangé"); }
		for(auto& f:fields){ free_field(f); }
		track_  = C_TP_RE;
	} else { status("Commentaire inchangé"); }
}

/*A_TP_SE*/
void NCInterface::tp_add_session(std::shared_ptr<TP> const& tp_ptr){
	if(confirm("Voulez-vous ajouter une session de TP?")){
		unsigned int geometry[4];
		std::vector<FIELD*> fields;
		std::string title("Titre du TP");
		Date date;
		std::vector<Choice> criteria = {Choice("1",true),Choice("2",true),Choice("3",true),Choice("4",true)};

		tp_session_configure(title,date,true,criteria,fields,geometry);
		if(fill_form(fields,geometry,"Configuration d'une session de TP")){
			title = my::trim(field_buffer(fields[1],0));
			date.set(
					my::string2type<unsigned int>(field_buffer(fields[3],0)),
					my::string2type<unsigned int>(field_buffer(fields[5],0)),
					my::string2type<unsigned int>(field_buffer(fields[7],0))
					);
			std::shared_ptr<TP::Session> session_ptr(
					std::make_shared<TP::Session>(
						date,
						my::string2type<char>(field_buffer(fields[9],0)) == 'A',
						title
						)
					);

			if(tp_ptr->add_session(session_ptr)){
				for(unsigned int c(0);c<4;c++){
					if(criteria[c].sel()){ tp_add_criteria_session(c,tp_ptr,session_ptr); }
				}

				std::shared_ptr<TP::Grade> grade_ptr;
				std::shared_ptr<Student> student_ptr;
				for(auto const& g:tp_ptr->get_grades()){
					grade_ptr = std::dynamic_pointer_cast<TP::Grade>(g);
					student_ptr = grade_ptr->get_student();
					if(session_ptr->for_group_A() == student_ptr->in_group_A()){
						grade_ptr->add_report(std::make_shared<TP::Report>(session_ptr,grade_ptr));
					}
				}
				modified_ = true;
			} else { status("Impossible d'ajouter une session ayant le même titre."); }
		} else { status("Évaluation non ajoutée"); }
		for(auto& f:fields){ free_field(f); }
	}  else { status("Évaluation non ajoutée"); }
}

/*E_TP_SE*/
void NCInterface::tp_edit_session(std::shared_ptr<TP> const& tp_ptr){
	if(tp_ptr->get_n_sessions()>select_[S_TP_SE[0]]){
		std::shared_ptr<TP::Session> session_ptr(tp_ptr->get_session(select_[S_TP_SE[0]]));
		if(confirm("Voulez-vous éditer la session '" + session_ptr->get_title(true,true) + "'?" )){
			unsigned int geometry[4];
			std::vector<FIELD*> fields;
			std::string title(session_ptr->get_title(false,false));
			Date date(session_ptr->get_date());
			std::vector<Choice> criteria;
			for(unsigned int c(0);c<4;c++){
				criteria.push_back(Choice(my::tostring(c+1),session_ptr->is_selected(c)));
			}

			tp_session_configure(title,date,session_ptr->for_group_A(),criteria,fields,geometry);
			if(fill_form(fields,geometry,"Configuration d'une session de TP")){
				track_ = S_TP_SE;
				title = my::trim(field_buffer(fields[1],0));
				date.set(
						my::string2type<unsigned int>(field_buffer(fields[3],0)),
						my::string2type<unsigned int>(field_buffer(fields[5],0)),
						my::string2type<unsigned int>(field_buffer(fields[7],0))
						);

				bool for_group_A(my::string2type<char>(field_buffer(fields[9],0)) == 'A');
				bool group_has_changed(session_ptr->for_group_A() != for_group_A);
				/*TODO:(current) test group change (seems ok)*/
				if(session_ptr->edit(tp_ptr,for_group_A,date,title)){
					modified_ = true;
					if(group_has_changed){
						if(confirm("Le groupe de TP a changé, voulez-vous ajouter un rapport aux élèves du nouvau groupe?")){
							std::shared_ptr<TP::Grade> grade_ptr;
							for(auto const& g:tp_ptr->get_grades()){
								if(session_ptr->for_group_A() == g->get_student()->in_group_A()){
									grade_ptr = std::dynamic_pointer_cast<TP::Grade>(g);
									if(!grade_ptr->add_report(std::make_shared<TP::Report>(session_ptr,grade_ptr)))
									{ status("Erreure fatale, voir '~/.jdgrades.log'"); }
								}
							}
						} else { status("Aucun rapport ajouté"); }
						if(confirm( "Le groupe de TP a changé, voulez-vous enlever le rapport"
									"aux élèves de l'ancien groupe? Attention, toutes les"
									"évaluations pour ce groupe seront perdues",2)){
							std::shared_ptr<Student> student_ptr;
							std::shared_ptr<TP::Report> report_ptr;
							for(auto const& g:tp_ptr->get_grades()){
								student_ptr = g->get_student();
								if(session_ptr->for_group_A() != student_ptr->in_group_A()){
									report_ptr = session_ptr->get_report_ptr(student_ptr);
									if(!(
												session_ptr->remove_report(report_ptr)
												&&
												std::dynamic_pointer_cast<TP::Grade>(g)->remove_report(report_ptr)
										)){ status("Erreure fatale, voir '~/.jdgrades.log'"); }
								}
							}
						} else { status("Aucun rapport enlevé"); }
					}
				} else { status("Paramètres généraux inchangés (voir '~/.jdgrades.log')"); }

				for(unsigned int c(0);c<4;c++){
					if(!(criteria[c].sel() == session_ptr->is_selected(c))){
						if(criteria[c].sel()){
							if(confirm("Voulez-vous ajouter le critère " + my::tostring(c+1) + " à cette session?")){
								tp_add_criteria_session(c,tp_ptr,session_ptr);
							} else { status("Le critère " + my::tostring(c+1) + " n'a pas été ajouté"); }
						} else {
							if(confirm("Voulez-vous supprimer le critère " + my::tostring(c+1) + " de cette session? Attention, les évaluations de ce critère seront perdues!",2)){
								session_ptr->remove(c);
								modified_ = true;
							} else { status("Le critère " + my::tostring(c+1) + " n'a pas été supprimé"); }
						}
					} else if (session_ptr->is_selected(c)){
						if(confirm("Voulez-vous éditer le critère " + my::tostring(c+1) + "?")){
							tp_edit_criteria_session(c,session_ptr);
						} else { status("Le critère " + my::tostring(c+1) + " n'a pas été édité"); }
					}
				}

				if(modified_){ session_ptr->compute_grade(); }
			} else { status("Évaluation non éditée"); }
		} else { status("Session non éditée"); }
	} else { status("Le TP ne contient pas de session a éditer"); }
}

/*R_TP_SE*/
void NCInterface::tp_remove_session(std::shared_ptr<TP> const& tp_ptr){
	if(tp_ptr->get_n_sessions()>select_[S_TP_SE[0]]){
		std::shared_ptr<TP::Session> session_ptr(tp_ptr->get_session(select_[S_TP_SE[0]]));
		if(confirm("Voulez-vous supprimer la session '" + session_ptr->get_title(true,false) + "'?" )){
			tp_ptr->remove_session(session_ptr);
			select_[S_TP_SE[0]] = 0;
			modified_ = true;
		} else { status("Session non supprimée"); }
	} else { status("Aucune session à supprimer"); }
}

/*L_TP_SE*/
void NCInterface::tp_latex_session(std::shared_ptr<TP> const& tp_ptr){
	if(tp_ptr->get_n_sessions()>select_[S_TP_SE[0]]){
		std::shared_ptr<TP::Session> session_ptr(tp_ptr->get_session(select_[S_TP_SE[0]]));
		if(confirm("Voulez-vous générer un PDF contenant toutes les évaluations des rapports de la session '" + session_ptr->get_title(true,false) + "'?")){
			processing("Veuillez patienter durant la création du PDF contenant toutes les évaluations des rapports de la session '" +session_ptr->get_title(false,true)+ "'");
			if(session_ptr->feedback()){ status("Problème durant la compilation"); }
			else { status("PDF crée"); }
		}
	} else { status("Le TP ne contient pas de session à latexer"); }
}

/*M_TP_SE*/
void NCInterface::tp_mail_session(std::shared_ptr<TP> const& tp_ptr){
	if(tp_ptr->get_n_sessions()>select_[S_TP_SE[0]]){
		std::shared_ptr<TP::Session> session_ptr(tp_ptr->get_session(select_[S_TP_SE[0]]));
		if(confirm("Voulez-vous envoyer toutes les évaluations des rapports de la session '" + session_ptr->get_title(false,true) + "'?")){
			processing("Veuillez patienter durant l'envoi de toutes les évaluations des rapports de la session '" +session_ptr->get_title(false,true)+ "'");
			int tmp(session_ptr->send_mail());
			switch(tmp){
				case -2: status("Aucun rapport à envoyer"); break;
				case -1: status("Configuration mail non définie"); break;
				case  0: status("Envoi réussi"); break;
				default: status(my::tostring(tmp%10000) + " compilations et " + my::tostring(tmp/10000) + " envois ont échoués"); break;
			}
		}
	} else { status("Le TP ne contient pas de session à emailer"); }
}

/*S_TP_SE*/
void NCInterface::tp_select_session(std::shared_ptr<Class> const& class_ptr, std::shared_ptr<TP> const& tp_ptr){
	bool repeat(true);
	while(keepon_ && repeat){
		msglist_[1] = "Sessions";
		msglist_[3] = "Résumé";
		msglist_[5] = "Note";
		set_window_frame(win_interactive_[1],msglist_[1],false);
		set_window_frame(win_interactive_[5],msglist_[5],false);
		unsigned int iA(0);
		unsigned int iB(0);
		for(auto const& s:tp_ptr->get_sessions()){
			if(s->for_group_A()){ iA++; }
			else { iB++; }
		}
		unsigned int tmp(geo_s_tpses_[1]);
		if(iA && iB){
			geo_s_tpses_[1] = ( geo_s_tpses_[1] - 2*geo_s_tpses_[3] - (iA>iB?iA-1:iB-1)*geo_s_tpses_[5] ) / (iA>iB?iA:iB);
		} else {
			geo_s_tpses_[1] = geo_s_tpses_[1] - 2*geo_s_tpses_[3];
		}

		display_tp_sessions(tp_ptr);
		display_class_results(class_ptr->get_students(),tp_ptr);
		track_ = S_TP_SE;

		std::vector<WINDOW*> sel_win;
		iA = iB = 0;
		for(auto const& s:tp_ptr->get_sessions()){
			if(s->for_group_A()){
				sel_win.push_back(derwin(
							win_interactive_[track_[1]],
							geo_s_tpses_[0],
							geo_s_tpses_[1],
							geo_s_tpses_[2],
							geo_s_tpses_[3]+(iA++)*(1+geo_s_tpses_[1])
							));
			} else {
				sel_win.push_back(derwin(
							win_interactive_[track_[1]],
							geo_s_tpses_[0],
							geo_s_tpses_[1],
							geo_s_tpses_[2]+1,
							geo_s_tpses_[3]+(iB++)*(1+geo_s_tpses_[1])
							));
			}
		}
		select(tp_ptr->list_sessions(),sel_win);
		for(auto const& w:sel_win){ delwin(w); }

		switch(track_[0]){
			case S_ST_SE[0]: //ENTER
				student_select_for_given_session(tp_ptr);
				break;
			case S_ST_TP[0]: //TAB
				student_select_for_given_tp(class_ptr,tp_ptr);
				break;
			case A_TP_SE[0]: //F1
				tp_add_session(tp_ptr);
				break;
			case E_TP_SE[0]: //F2
				tp_edit_session(tp_ptr);
				break;
			case R_TP_SE[0]: //F3
				tp_remove_session(tp_ptr);
				break;
			case L_TP_SE[0]: //F4
				tp_latex_session(tp_ptr);
				break;
			case M_TP_SE[0]: //F5
				tp_mail_session(tp_ptr);
				break;
			case S_TP_SE[0]:
				break;
			default:
				repeat = false;
		}

		geo_s_tpses_[1] = tmp;
	}
}

/*A_ST_WA*/
void NCInterface::tp_add_warning(std::shared_ptr<TP::Grade> const& grade_ptr){
	if(confirm("Voulez-vous ajouter un avertissement pour " + grade_ptr->get_student_name(true))){
		std::vector<FIELD*> fields;
		unsigned int geometry[4];

		std::shared_ptr<TP> tp_ptr(std::dynamic_pointer_cast<TP>(grade_ptr->get_eval()));
		std::vector<std::string> list_sessions(grade_ptr->list_sessions());
		unsigned int criteria(0);
		std::string comment("");

		tp_warning_configure(criteria, list_sessions, 0, comment, fields, geometry);
		if(fill_form(fields,geometry,"Ajout d'un avertissement pour" + grade_ptr->get_student_name(true))){
			criteria = my::string2type<char>(field_buffer(fields[1],0)) == '6';
			std::string session_name(my::trim(field_buffer(fields[3],0)));
			comment = my::trim(field_buffer(fields[5],0));
			bool found_matching_session(false);
			for(auto const& s:tp_ptr->get_sessions()){
				if(s->get_title(true,true) == session_name){
					grade_ptr->add_warning(s,criteria,comment);
					grade_ptr->compute_grade();
					found_matching_session = true;
					modified_ = true;
					break;
				}
			}
			if(!found_matching_session){ status("Session non trouvée, avertissement non ajouté"); }
		} else { status("Abandon, avertissement non ajouté"); }
	} else { status("Avertissement non ajouté"); }
}

/*E_ST_WA*/
void NCInterface::tp_edit_warning(std::shared_ptr<TP::Grade> const& grade_ptr){
	unsigned int const w(select_[S_ST_WA[0]]);
	if(grade_ptr->get_warnings().size()>w){
		if(confirm("Voulez-vous éditer l'avertissement pour " + grade_ptr->get_student_name(true))){
			std::vector<FIELD*> fields;
			unsigned int geometry[4];

			std::shared_ptr<TP> tp_ptr(std::dynamic_pointer_cast<TP>(grade_ptr->get_eval()));
			std::vector<std::string> list_sessions(grade_ptr->list_sessions());

			unsigned int criteria(grade_ptr->get_warning(w).get_criteria());
			std::string comment(grade_ptr->get_warning(w).get_comment());
			unsigned int idx(0);
			for(auto const& s:tp_ptr->get_sessions()){
				if(*s == *grade_ptr->get_warning(w).get_session()){ break; }
				idx++;
			}

			tp_warning_configure(criteria, list_sessions, idx, comment, fields, geometry);
			if(fill_form(fields,geometry,"Edition de l'avertissements pour " + grade_ptr->get_student_name(true))){
				criteria = my::string2type<char>(field_buffer(fields[1],0)) == '6';
				std::string session_name(my::trim(field_buffer(fields[3],0)));
				comment = my::trim(field_buffer(fields[5],0));
				bool found_matching_session(false);
				for(auto const& s:tp_ptr->get_sessions()){
					if(s->get_title(true,true) == session_name){
						grade_ptr->edit_warning(w,s,criteria,comment);
						grade_ptr->compute_grade();
						found_matching_session = true;
						modified_ = true;
						break;
					}
				}
				if(!found_matching_session){ status("Session non trouvée, avertissement inchangé"); }
			} else { status("Abandon, avertissement inchangé"); }
		} else { status("Avertissement inchangé"); }
	} else { status("Aucun avertissement à éditer"); }
}

/*R_ST_WA*/
void NCInterface::tp_remove_warning(std::shared_ptr<TP::Grade> const& grade_ptr){
	unsigned int const w(select_[S_ST_WA[0]]);
	if(grade_ptr->get_warnings().size()<S_ST_WA[0]){
		if(confirm("Voulez-vous supprimer l'avertissement pour " + grade_ptr->get_student_name(true))){
			grade_ptr->remove_warning(w);
			grade_ptr->compute_grade();
			modified_ = true;
		} else { status("Avertissement supprimé"); }
	} else { status("Aucun avertissement à supprimer"); }
}

/*S_ST_WA*/
void NCInterface::student_select_warning(std::shared_ptr<Student> const& student_ptr, std::shared_ptr<TP> const& tp_ptr){
	if(tp_ptr->get_n_sessions()){
		std::shared_ptr<TP::Grade> grade_ptr(std::dynamic_pointer_cast<TP::Grade>(tp_ptr->get_grade(student_ptr)));
		if(grade_ptr){
			msglist_[3] = "Avertissements";
			bool repeat(true);
			while(keepon_ && repeat){
				track_ = S_ST_WA;
				select(grade_ptr->list_warnings(),geo_s_crind_);
				switch(track_[0]){
					case A_ST_WA[0]: //F1
						tp_add_warning(grade_ptr);
						break;
					case E_ST_WA[0]: //F2
						tp_edit_warning(grade_ptr);
						break;
					case R_ST_WA[0]: //F3
						tp_remove_warning(grade_ptr);
						break;
					case S_ST_WA[0]:
						break;
					default:
						repeat = false;
				}
			}
			msglist_[3] = "Résumé";
		} else { status("L'élève n'a pas de note de TP"); }
	} else { status("Il est impossible d'accéder aux avertissements si aucune session n'a été définie."); }
}
/*}*/
/*}*/

void NCInterface::config_mail(){
	track_ = E_CLASS; // as long as this is (0,4) it doesn't matter
	for(unsigned int i(0);i<nfw_;i++){
		wclear(win_interactive_[i]);
		set_window_frame(win_interactive_[i],msglist_[i],false); //msglistok
	}
	mvwprintw(win_interactive_[3], 2, 3,"Vous allez maintenant configurer le serveur SMTP pour envoyer des mails");
	mvwprintw(win_interactive_[3], 3, 3,"automatiques aux étudiants. Votre addresse e-mail et mot de passe seront");
	mvwprintw(win_interactive_[3], 4, 3,"sauvegardés en hexadécimale dans un fichier binaire. Ils ne seront donc");
	mvwprintw(win_interactive_[3], 5, 3,"PAS encryptés mais simplement illisibles par un humain.");
	mvwprintw(win_interactive_[3], 6, 3,"Pour l'instant, seules les adresses educanet2, eduvaud et gmail ont été");
	mvwprintw(win_interactive_[3], 7, 3,"testées et les tests effectués sont minimaux. Il est n'est pas garantit que");
	mvwprintw(win_interactive_[3], 8, 3,"les mails soient correctement envoyés. Avec educanet2, si un envoi échoue,");
	mvwprintw(win_interactive_[3], 9, 3,"un mail d'erreur semble toujours être envoyé.");
	wnoutrefresh(win_interactive_[3]);

	std::string from("prenom.nom@eduvaud.ch");
	std::string signature("Prénom Nom");
	std::string url("smtp://smtp.office365.com");

	MailCurlSMTP mail;
	IOFiles r(year_->get_io_path()+"mails/mail.jdbin",false,false);
	if(r.is_open()){
		mail.set_account(r);
		from = mail.get_from();
		signature = mail.get_signature();
		url = mail.get_url();
	}

	unsigned int geometry[4];
	geometry[0] = 1;
	geometry[1] = 40;
	geometry[2] = 1;
	geometry[3] = 1;

	std::vector<FIELD*> fields;
	add_text_form(fields,"Entrez votre addresse email",from,geometry);
	geometry[2] += 2;
	add_text_form(fields,"Entrez votre signature",signature,geometry);
	geometry[2] -= 2;
	geometry[3] += geometry[1]+1;
	add_password_form(fields,"Entrez votre le mot de passe",geometry);
	geometry[2] += 2;
	add_password_form(fields,"Confirmez le mot de passe",geometry);
	geometry[3] += geometry[1]+1;
	geometry[2] -= 2;
	add_text_form(fields,"Entrez votre le serveur SMTP",url,geometry);
	fields.push_back(NULL);

	geometry[0] = 5;
	geometry[1]+= geometry[3];
	geometry[3] = 1;

	bool keepon(true);
	do{
		std::string password_a;
		std::string password_b;
		if(fill_form(fields,geometry,"Configuration mail")){
			from = my::trim(field_buffer(fields[1],0));
			signature = my::trim(field_buffer(fields[3],0));
			password_b = my::trim(field_buffer(fields[5],0));
			password_a = my::trim(field_buffer(fields[7],0));
			url = my::trim(field_buffer(fields[9],0));

			if(password_a == "" || password_a != password_b){
				status("Les mots de passe donnés ne correspondent pas ou sont vides.");
			} else {
				if(confirm("Voulez-vous tester cette configuration?")){
					status("Email de test envoyé");
					mail.set_account(from, signature, password_a, url, 587);
					mail.set_to(from);
					mail.set_log("mail.log");
					mail.set_subject("jdgrade test mail config");
					mail.set_body("Si vous êtes " + mail.get_signature() + " et que vous recevez ce mail, c'est que votre configuration est correcte.");
					mail.send();
					if(confirm("Avez-vous reçu le mail de test?")){
						keepon = false;
						Linux command;
						command.mkpath((year_->get_io_path()+"mails/").c_str());
						command("rm mail.log",true);
						IOFiles w(year_->get_io_path()+"mails/mail.jdbin",true,false);
						mail.save_account(w);
					} else { status("Corrigez votre configuration"); }
				} else { status(); }
			}
		} else {
			status("Configuration mail abandonnée");
			keepon = false;
		}
	} while (keepon);
	for(auto& f:fields){ free_field(f); }
}

bool NCInterface::confirm(std::string const& msg, unsigned int const& color){
	ITEM* my_items[3];
	my_items[0] = new_item("Non","");
	my_items[1] = new_item("Oui","");
	my_items[2] = NULL;

	set_window_frame(win_interactive_[track_[1]],msglist_[track_[1]],false); //msglistok
	WINDOW* win_info(derwin(win_help_,5,43,2,2));
	wclear(win_help_);
	MENU* my_menu(new_menu(&my_items[0]));
	set_menu_win(my_menu, win_help_);
	set_menu_sub(my_menu, derwin(win_help_, 2, 42, 7, 3));

	post_menu(my_menu);
	set_window_frame(win_help_,"Confirmation",true,color);
	mvwprintw(win_info, 0, 0, my::wrap(msg,44).c_str());
	wnoutrefresh(win_info);
	doupdate();

	bool keepon(true);
	int selected(0);
	while(keepon){
		unsigned int in(wgetch(win_help_)); //ensure a correct cast
		switch(in){
			case 261: //RIGHT
			case 258: //DOWN
				menu_driver(my_menu, REQ_DOWN_ITEM);
				break;
			case 260: //LEFT
			case 259: //UP
				menu_driver(my_menu, REQ_UP_ITEM);
				break;
			case 10: //ENTER
				selected = item_index(current_item(my_menu));
				keepon = false;
				break;
			case 27: //ESC
				selected = -1;
				keepon = false;
				break;
		}
	}
	unpost_menu(my_menu);
	free_menu(my_menu);
	for(auto& i:my_items){ free_item(i); }

	return selected==1;
}

void NCInterface::select(std::vector<std::string> const& list, unsigned int const geometry[]){
	std::vector<WINDOW*> sel_win(list.size());
	unsigned int i(0);
	for(auto& s:sel_win){
		s = derwin(win_interactive_[track_[1]],
				geometry[0],
				geometry[1],
				geometry[2]+i*geometry[0]*geometry[4],
				geometry[3]+i*geometry[1]*geometry[5]);
		i++;
	}
	sel_win.push_back(NULL);

	select(list,sel_win);
	for(auto const& w:sel_win){ delwin(w); }
}

void NCInterface::select(std::vector<std::string> const& list, std::vector<WINDOW*> const& sel_win){
	WINDOW* win(win_interactive_[track_[1]]);
	std::string msg(msglist_[track_[1]]); //msglistok

	unsigned int nsel(list.size());
	std::vector<unsigned int> sel(std::max(nsel,(unsigned int)1));
	for(unsigned int i(0);i<nsel;i++) { sel[i] = i; }

	std::string cmp("");
	unsigned int highlight(select_[track_[0]]);
	bool keepon(true);

	while(keepon){
		if(cmp == ""){ display_help(); }
		wclear(win);
		set_window_frame(win,msg,true);
		for(unsigned int i(0);i<list.size();i++){
			/*TODO:(middle) give the option to change A_BOLD by A_STANDOUT*/
			if(i == sel[highlight]){
				wattron(sel_win[i], A_BOLD);
				mvwprintw(sel_win[i], 0,0, list[i].c_str());
				wattroff(sel_win[i], A_BOLD);
			} else {
				mvwprintw(sel_win[i], 0,0, list[i].c_str());
			}
			wnoutrefresh(sel_win[i]);
		}
		doupdate();

		unsigned int in(wgetch(win_help_)); //ensure a correct cast
		switch(in){
			case 9: //TAB
				if(cmp == ""){
					status();
					select_[track_[0]] = sel[highlight];
					switch(track_[0]){
						case S_EVALU[0]: /*calls student_select_overview */
							track_ = S_ST_OV;
							break;
						case S_ST_OV[0]: /*same as backspace*/
							track_ = S_EVALU;
							break;
						case S_ST_EV[0]: /*calls evaluation_select_exercise*/
							track_ = S_EX_EV;
							break;
						case S_EX_EV[0]: /*same as backspace*/
							track_ = S_ST_EV;
							break;
						case S_TP_SE[0]: /*calls student_select_for_given_tp*/
							track_ = S_ST_TP;
							break;
						case S_ST_TP[0]: /*same as backspace*/
							track_ = S_TP_SE;
							break;
					}
					keepon = false;
				} else if(nsel){
					if(highlight+1 == nsel){ highlight = 0; }
					else { ++highlight; }
				}
				break;
			case 353://MAJTAB
				if(cmp != "" && nsel){
					if(highlight == 0){ highlight = nsel-1; }
					else { --highlight; }
				}
				break;
			case 258: //DOWN
			case 261: //RIGHT
				if(nsel){
					if(highlight+1 == nsel){ highlight = 0; }
					else { ++highlight; }
				}
				break;
			case 260: //LEFT
			case 259: //UP
				if(nsel){
					if(highlight == 0) { highlight = nsel-1; }
					else { --highlight; }
				}
				break;
			case 10: //ENTER
				status();
				set_window_frame(win,msg,false);
				select_[track_[0]] = sel[highlight];
				switch(track_[0]){
					case S_CLASS[0]:
					case S_TP_IN[0]:
						track_ = S_EVALU;
						break;
					case S_EVALU[0]:
						track_ = S_ST_EV;
						break;
					case A_EVALU[0]:
						track_ = E_AD_EV;
						break;
					case S_ST_EV[0]:
						track_ = C_ST_EV;
						break;
					case S_EX_EV[0]:
						track_ = C_EX_EV;
						break;
					case A_TP_RE[0]:
					case S_ST_TP[0]:
						track_ = S_TP_RE;
						break;
					case S_ST_SE[0]:
					case S_TP_RE[0]:
						track_ = C_TP_RE;
						break;
					case S_TP_SE[0]:
						track_ = S_ST_SE;
						break;
					case S_ST_WA[0]:
						track_ = S_ST_TP;
						break;
					case S_EX_IN[0]:
						track_ = A_EVALU;
						break;
				}
				keepon = false;
				break;
			case 27: //ESC
				if(cmp != ""){ cmp = ""; }
				else {
					status();
					keepon_ = keepon = !(modified_ ?confirm("Voulez-vous quitter et perdre les modifications?",2):confirm("Voulez-vous quitter?"));
				}
				break;
			case 330://DEL
				break;
				/*TODO:(low) figure out what is relevant for OSX (two switch)*/
			case KEY_BACKSPACE://263: //BACKSPACE
			case 127: /*for OSX*/
			case '\b': /*for OSX*/
				if(cmp.size()){
					my::pop_back_utf8(cmp);
					if(!cmp.size()){
						nsel = list.size();
						sel.resize(std::max(nsel,(unsigned int)1));
						for(unsigned int i(0);i<nsel;i++) { sel[i] = i; }
					}
				} else {
					status();
					set_window_frame(win,msg,false);
					select_[track_[0]] = 0;
					keepon = false;
					switch(track_[0]){
						case S_CLASS[0]:
						case S_EVALU[0]:
							track_ = S_CLASS;
							break;
						case S_ST_OV[0]:
						case S_ST_EV[0]:
						case A_EVALU[0]:
						case S_TP_SE[0]:
							track_ = S_EVALU;
							break;
						case S_EX_EV[0]:
							track_ = S_ST_EV;
							break;
						case S_TP_RE[0]:
							track_ = S_ST_TP;
							break;
						case C_TP_RE[0]:
							track_ = S_TP_RE;
							break;
						case S_ST_SE[0]:
						case S_ST_TP[0]:
							track_ = S_TP_SE;
							break;
					}
				}
				break;
			case KEY_F(1): //To add
				set_window_frame(win,msg,false);
				switch(track_[0]){
					case S_CLASS[0]:
						track_ = A_CLASS;
						break;
					case S_EVALU[0]:
						track_ = A_EVALU;
						break;
					case S_ST_OV[0]:
						track_ = A_STUDE;
						break;
					case S_TP_RE[0]:
						track_ = A_TP_RE;
						break;
					case S_TP_SE[0]:
						track_ = A_TP_SE;
						break;
					case S_TP_IN[0]:
						track_ = A_TP_IN;
						break;
					case S_EX_IN[0]:
						track_ = A_EX_IN;
						break;
					case S_ST_WA[0]:
						track_ = A_ST_WA;
						break;
					default:
						status("Action Fn inconnue");
				}
				cmp = "";
				keepon = false;
				break;
			case KEY_F(2): //To edit
				set_window_frame(win,msg,false);
				select_[track_[0]] = sel[highlight];
				switch(track_[0]){
					case S_CLASS[0]:
						track_ = E_CLASS;
						break;
					case S_EVALU[0]:
						track_ = E_EVALU;
						break;
					case S_ST_OV[0]:
						track_ = E_STUDE;
						break;
					case S_ST_EV[0]:
						track_ = E_ST_EV;
						break;
					case S_TP_IN[0]:
						track_ = E_TP_IN;
						break;
					case S_EX_IN[0]:
						track_ = E_EX_IN;
						break;
					case S_ST_SE[0]:
					case S_ST_TP[0]:
						track_ = S_ST_WA;
						break;
					case S_ST_WA[0]:
						track_ = E_ST_WA;
						break;
					case S_TP_SE[0]:
						track_ = E_TP_SE;
						break;
					case C_TP_RE[0]:
						track_ = E_CR_CO;
						break;
					default:
						status("Action Fn inconnue");
				}
				cmp = "";
				keepon = false;
				break;
			case KEY_F(3): //To remove
				set_window_frame(win,msg,false);
				select_[track_[0]] = sel[highlight];
				switch(track_[0]){
					case S_CLASS[0]:
						track_ = R_CLASS;
						break;
					case S_EVALU[0]:
						track_ = R_EVALU;
						break;
					case S_ST_OV[0]:
						track_ = R_STUDE;
						break;
					case S_ST_EV[0]:
					case S_ST_TP[0]:
						track_ = R_ST_GR;
						break;
					case S_TP_SE[0]:
						track_ = R_TP_SE;
						break;
					case S_TP_RE[0]:
						track_ = R_TP_RE;
						break;
					case S_TP_IN[0]:
						track_ = R_TP_IN;
						break;
					case S_EX_IN[0]:
						track_ = R_EX_IN;
						break;
					case S_ST_WA[0]:
						track_ = R_ST_WA;
						break;
					default:
						status("Action Fn inconnue");
				}
				cmp = "";
				keepon = false;
				break;
			case KEY_F(4): //To Latex
				set_window_frame(win,msg,false);
				select_[track_[0]] = sel[highlight];
				switch(track_[0]){
					case S_CLASS[0]:
						track_ = L_CLASS;
						break;
					case S_EVALU[0]:
						track_ = L_EVALU;
						break;
					case S_ST_OV[0]:
						track_ = L_STUDE;
						break;
					case S_TP_RE[0]:
						track_ = L_TP_RE;
						break;
					case S_ST_SE[0]:
						track_ = L_TP_RE;
						break;
					case S_TP_SE[0]:
						track_ = L_TP_SE;
						break;
					case S_ST_TP[0]:
						track_ = L_ST_TP;
						break;
					default:
						status("Action Fn inconnue");
				}
				cmp = "";
				keepon = false;
				break;
			case KEY_F(5):
				set_window_frame(win,msg,false);
				select_[track_[0]] = sel[highlight];
				switch(track_[0]){
					case S_EVALU[0]:
						track_ = M_EVALU;
						break;
					case S_ST_EV[0]:
						track_ = M_ST_GR;
						break;
					case S_TP_SE[0]:
						track_ = M_TP_SE;
						break;
					case S_TP_RE[0]:
						track_ = M_TP_RE;
						break;
					default:
						status("Action Fn inconnue");
				}
				cmp = "";
				keepon = false;
				break;
			case KEY_F(6): case KEY_F(7): case KEY_F(8):
			case KEY_F(9): case KEY_F(10): case KEY_F(11):
				status("Action Fn inconnue");
				break;
			case KEY_F(12):
				if(modified_){
					year_->save();
					status("Sauvegarde");
					modified_ = false;
				} else { status("Aucune modification à sauvegarder"); }
				cmp = "";
				keepon = false;
				break;
			default:
				if(nsel > 1){
					cmp += in;
					highlight = 0;
				}
				break;
		}

		if(cmp.size()){
			sel.clear();
			std::regex r(cmp,std::regex_constants::icase);
			std::smatch sm;
			for(unsigned int i(0);i<list.size();i++){
				if(std::regex_search(list[i],sm,r)){ sel.push_back(i); }
			}
			nsel = sel.size();

			wclear(win_help_);
			mvwprintw(win_help_,2,2, ("Recherche: " + cmp).c_str() );
			switch(sel.size()){
				case 0:
					/*TODO:(high) when no match is found and that the last key
					 * pressed is é,à,è... it is not displayed -> probably need
					 * to change wgetch by wget_wch*/
					set_window_frame(win_help_,"Aucune correspondance",true,2);
					break;
				case 1:
					set_window_frame(win_help_,"Unique correspondance",true,3);
					[[fallthrough]];
				default:
					std::string tmp;
					for(unsigned int i(0); i<sel.size();i++){
						if(i==6){
							mvwprintw(win_help_, i+3, 13, "... (autres choix)");
							i=sel.size();
						} else {
							tmp = list[sel[i]];
							if(tmp.find('\n') != std::string::npos){ std::replace(tmp.begin(),tmp.end(),'\n',' '); }
							if(tmp.size()>select_suggestions_max_length){
								tmp = tmp.substr(0,select_suggestions_max_length) + "...";
							}
							mvwprintw(win_help_,i+3,13,tmp.c_str());
						}
					}
			}
			wnoutrefresh(win_help_);
		}
	}
}

bool NCInterface::fill_form(std::vector<FIELD*>& fields, unsigned int const geometry[], std::string const& msg){
	display_help();

	WINDOW* win(win_interactive_[track_[1]]);
	wclear(win);
	set_window_frame(win,msg,true);
	curs_set(1);

	WINDOW* win_form(derwin(
				win,
				geometry[0],
				geometry[1],
				geometry[2],
				geometry[3])
			);
	FORM* form = new_form(&fields[0]);
	set_form_win(form, win);
	set_form_sub(form, win_form);
	post_form(form);

	void* c_ptr(NULL);
	bool keepon(true);
	bool save(false);
	wint_t in(0);
	while(keepon){
		if(wget_wch(win,&in) != ERR){
			switch(in){
				case 9: //TAB
					form_driver(form, REQ_VALIDATION);
					form_driver(form, REQ_NEXT_FIELD);
					c_ptr = field_userptr(current_field(form));
					break;
				case KEY_PPAGE: //PAGE_UP
					form_driver(form, REQ_NEXT_CHOICE);
					break;
				case KEY_NPAGE: //PAGE_DOWN
					form_driver(form, REQ_PREV_CHOICE);
					break;
				case 353: //MAJTAB
					form_driver(form, REQ_VALIDATION);
					form_driver(form, REQ_PREV_FIELD);
					c_ptr = field_userptr(current_field(form));
					break;
				case 260: //LEFT
				case 259: //UP
					form_driver(form, REQ_VALIDATION);
					form_driver(form, REQ_PREV_CHAR);
					break;
				case 261: //RIGHT
				case 258: //DOWN
					form_driver(form, REQ_VALIDATION);
					form_driver(form, REQ_NEXT_CHAR);
					break;
				case KEY_HOME:/*for OSX ???*/
					form_driver(form, REQ_BEG_LINE);
					break;
				case KEY_END: /*for OSX ???*/
					form_driver(form, REQ_END_LINE);
					break;
				case KEY_BACKSPACE://263: //BACKSPACE
				case 127: /*for OSX*/
				case '\b': /*for OSX*/
					form_driver(form, REQ_DEL_PREV);
					break;
				case 10: //ENTER
					if(!form_driver(form, REQ_VALIDATION)){
						form_driver(form, REQ_NEXT_FIELD);
						form_driver(form, REQ_PREV_FIELD);
						save = true;
						keepon = false;
					}
					break;
				case 27: //ESC
					keepon = false;
					break;
				case 330: //DEL
					form_driver(form, REQ_DEL_CHAR);
					break;
				case KEY_F(1): case KEY_F(2): case KEY_F(3): case KEY_F(4):
				case KEY_F(5): case KEY_F(6): case KEY_F(7): case KEY_F(8):
				case KEY_F(9): case KEY_F(10): case KEY_F(11):
					status("Action Fn inconnue");
					break;
				default:
					if(c_ptr){
						bool* tmp(static_cast<bool*>(c_ptr));
						*tmp = !*tmp;
						if(*tmp){ set_field_back(current_field(form), A_UNDERLINE); }
						else { set_field_back(current_field(form), A_NORMAL); }
						form_driver(form, REQ_VALIDATION);
					} else { form_driver_w(form, OK, in); }
			}
		}
	}

	unpost_form(form);
	free_form(form);
	delwin(win_form);
	curs_set(0);

	set_window_frame(win,msglist_[track_[1]],false); //msglistok
	return save;
}

void NCInterface::add_text_form(std::vector<FIELD*>& fields, std::string const& input, std::string const& output, unsigned int geometry[]){
	FIELD* tmp;
	geometry[1] = std::max((unsigned int)input.size(),geometry[1]);
	tmp = new_field(geometry[0], geometry[1], geometry[2], geometry[3], 0, 0);
	set_field_opts(tmp, O_VISIBLE | O_PUBLIC | O_STATIC);
	set_field_buffer(tmp,0,input.c_str());
	fields.push_back(tmp);

	tmp = new_field(geometry[0], geometry[1], geometry[2]+1, geometry[3], 0, 0);
	set_field_opts(tmp, O_VISIBLE | O_PUBLIC | O_EDIT | O_ACTIVE | O_BLANK);
	set_field_back(tmp, A_UNDERLINE);
	set_field_buffer(tmp,0,output.c_str());
	fields.push_back(tmp);
}

void NCInterface::add_password_form(std::vector<FIELD*>& fields, std::string const& input, unsigned int geometry[]){
	FIELD* tmp;
	geometry[1] = std::max((unsigned int)input.size(),geometry[1]);
	tmp = new_field(geometry[0], geometry[1], geometry[2], geometry[3], 0, 0);
	set_field_opts(tmp, O_VISIBLE | O_PUBLIC | O_STATIC);
	set_field_buffer(tmp,0,input.c_str());
	fields.push_back(tmp);

	tmp = new_field(geometry[0], geometry[1], geometry[2]+1, geometry[3], 0, 0);
	set_field_opts(tmp, O_VISIBLE | O_EDIT | O_ACTIVE | O_BLANK);
	set_field_back(tmp, A_UNDERLINE);
	set_field_buffer(tmp,0,"");
	fields.push_back(tmp);
}

void NCInterface::add_list_form(std::vector<FIELD*>& fields, std::string const& input, std::vector<std::string> const& choices, unsigned int const& current_choice, unsigned int geometry[]){
	std::vector<char const*> c_choices;
	unsigned int size(choices.size());
	c_choices.reserve(size+1);
	for(unsigned int i(0);i<size;i++){
		geometry[1] = choices[i].size()>geometry[1]?choices[i].size():geometry[1];
		c_choices.push_back(choices[(i+current_choice)%size].data());
	}
	c_choices.push_back(nullptr);

	FIELD* tmp;
	geometry[1] = std::min(std::max((unsigned int)input.size(),geometry[1]),(unsigned int)40);
	tmp = new_field(geometry[0], geometry[1], geometry[2], geometry[3], 0, 0);
	set_field_opts(tmp, O_VISIBLE | O_PUBLIC | O_STATIC);
	set_field_buffer(tmp,0,input.c_str());
	fields.push_back(tmp);

	tmp = new_field(geometry[0], geometry[1], geometry[2]+1, geometry[3], 0, 0);
	set_field_opts(tmp, O_VISIBLE | O_PUBLIC | O_EDIT | O_ACTIVE | O_BLANK);
	set_field_back(tmp, A_UNDERLINE);
	/*TODO:(high) only a reference to c_choices is passed, it will not be
	 * valid on exit (see https://linux.die.net/man/3/set_field_type) */
	set_field_type(tmp, TYPE_ENUM, &c_choices[0], false, false);
	set_field_buffer(tmp,0,c_choices[0]);
	fields.push_back(tmp);
}

void NCInterface::add_double_form(std::vector<FIELD*>& fields, std::string const& input, double const& output, double const& min, double const& max, unsigned int const& in_line, unsigned int geometry[]){
	FIELD* tmp;
	geometry[1] = input.size();
	tmp = new_field(geometry[0], geometry[1], geometry[2], geometry[3]+in_line, 0, 0);
	set_field_opts(tmp, O_VISIBLE | O_PUBLIC | O_STATIC);
	set_field_buffer(tmp,0,input.c_str());
	fields.push_back(tmp);

	tmp = new_field(geometry[0], in_line?in_line-1:geometry[1], geometry[2]+(in_line?0:1), geometry[3], 0, 0);
	set_field_opts(tmp, O_VISIBLE | O_PUBLIC | O_EDIT | O_ACTIVE | O_BLANK);
	set_field_back(tmp, A_UNDERLINE);
	set_field_type(tmp, TYPE_NUMERIC, 2, min, max);
	set_field_just(tmp, JUSTIFY_RIGHT);
	set_field_buffer(tmp,0,my::tostring(output).c_str());
	fields.push_back(tmp);
}

void NCInterface::add_int_form(std::vector<FIELD*>& fields, std::string const& input, int const& output, int const& min, int const& max, unsigned int const& padding, unsigned int const& in_line, unsigned int geometry[]){
	FIELD* tmp;
	geometry[1] = input.size();
	tmp = new_field(geometry[0], geometry[1], geometry[2], geometry[3]+in_line, 0, 0);
	set_field_opts(tmp, O_VISIBLE | O_PUBLIC | O_STATIC);
	set_field_buffer(tmp,0,input.c_str());
	fields.push_back(tmp);

	tmp = new_field(geometry[0], in_line?in_line-1:geometry[1], geometry[2]+(in_line?0:1), geometry[3], 0, 0);
	set_field_opts(tmp, O_VISIBLE | O_PUBLIC | O_EDIT | O_ACTIVE | O_BLANK);
	set_field_back(tmp, A_UNDERLINE);
	set_field_type(tmp, TYPE_INTEGER, padding, min, max);
	set_field_just(tmp, JUSTIFY_RIGHT);
	set_field_buffer(tmp,0,my::tostring(output).c_str());
	fields.push_back(tmp);
}

void NCInterface::add_date_form(std::vector<FIELD*>& fields, Date const& input, unsigned int geometry[]){
	if(input.year()==1){
		add_int_form(fields,"Jour",year_->get_today().day(),1,31,2,0,geometry);
		geometry[3] += geometry[1]+1;
		add_int_form(fields,"Mois",year_->get_today().month(),1,12,2,0,geometry);
		geometry[3] += geometry[1]+1;
		add_int_form(fields,"Année",year_->get_today().year(),year_->get_start().year(),year_->get_end().year(),0,0,geometry);
	} else {
		add_int_form(fields,"Jour",input.day(),1,31,2,0,geometry);
		geometry[3] += geometry[1]+1;
		add_int_form(fields,"Mois",input.month(),1,12,2,0,geometry);
		geometry[3] += geometry[1]+1;
		add_int_form(fields,"Année",input.year(),year_->get_start().year(),year_->get_end().year(),0,0,geometry);
	}
}

void NCInterface::add_choices_form(std::vector<FIELD*>& fields, std::string const& input, std::vector<Choice>& output, bool horizontal, unsigned int geometry[]){
	FIELD* tmp;
	geometry[1] = input.size()+1;
	tmp = new_field(geometry[0], geometry[1], geometry[2], geometry[3], 0, 0);
	set_field_opts(tmp, O_VISIBLE | O_PUBLIC | O_STATIC);
	set_field_buffer(tmp,0,input.c_str());
	fields.push_back(tmp);

	for(auto& o:output){
		geometry[1] = o.msg_size();
		tmp = new_field(geometry[0], geometry[1], geometry[2]+1, geometry[3], 0, 0);
		set_field_opts(tmp, O_VISIBLE | O_PUBLIC | O_STATIC | O_ACTIVE);
		if(o.sel()){ set_field_back(tmp, A_UNDERLINE); }
		set_field_buffer(tmp,0,o.msg().c_str());
		set_field_userptr(tmp,&o.sel());
		fields.push_back(tmp);
		if(horizontal){ geometry[3] += geometry[1]+1; }
		else { geometry[2] += 1; }
	}
	geometry[1] = std::max(output.size()*(geometry[1]+1),input.size()+2);
}

void NCInterface::display_class_results(std::shared_ptr<Class> const& class_ptr, std::shared_ptr<Evaluation> const& eval_ptr) const {
	wclear(win_interactive_[3]);
	wclear(win_interactive_[5]);
	std::shared_ptr<GenericGrade> grade_ptr;
	for(unsigned int i(0);i<class_ptr->get_nstudents();i++){
		grade_ptr = (*class_ptr)(i,select_[S_EVALU[0]]);
		if(grade_ptr){
			for(unsigned int j(0);j<eval_ptr->get_n_criteria();j++){
				mvwprintw(
						win_interactive_[3],
						geo_s_stude_[2]+i*geo_s_stude_[0],
						geo_s_exerc_[3]+j*geo_s_exerc_[1],
						"%5.2f",(*grade_ptr)(j)
						);
			}
			if(!grade_ptr->is_recorded()){ mvwprintw(win_interactive_[3],geo_s_stude_[2]+i*geo_s_stude_[0],1,"!");  }
			if(grade_ptr->is_catch_up()){ mvwprintw(win_interactive_[3],geo_s_stude_[2]+i*geo_s_stude_[0],2,"*"); }
			mvwprintw(win_interactive_[5],geo_s_stude_[2]+i*geo_s_stude_[0],2,"%1.1f",grade_ptr->get_grade());
		}
	}
	set_window_frame(win_interactive_[3],msglist_[3],false); //msglistok
	set_window_frame(win_interactive_[5],msglist_[5],false); //msglistok
}

void NCInterface::display_class_results(std::vector<std::shared_ptr<Student> > students, std::shared_ptr<TP> const& tp_ptr) const {
	wclear(win_interactive_[2]);
	wclear(win_interactive_[3]);
	wclear(win_interactive_[5]);
	std::shared_ptr<TP::Grade> grade_ptr;
	for(unsigned int i(0);i<students.size();i++){
		grade_ptr = std::dynamic_pointer_cast<TP::Grade>(tp_ptr->get_grade(students[i]));
		if(grade_ptr){
			/*TODO:(low) does not update properly when a criteria is newly validated*/
			mvwprintw(win_interactive_[3],geo_s_stude_[2]+i*geo_s_stude_[0],geo_s_exerc_[3],grade_ptr->get_warning_status().c_str());
			mvwprintw(win_interactive_[5],geo_s_stude_[2]+i*geo_s_stude_[0],2,"%1.1f",grade_ptr->get_grade());
		}
		mvwprintw(win_interactive_[2],geo_s_stude_[2]+i*geo_s_stude_[0],geo_s_stude_[3],students[i]->get_name(true).c_str());
	}
	set_window_frame(win_interactive_[2],msglist_[2],false); //msglistok
	set_window_frame(win_interactive_[3],msglist_[3],false); //msglistok
	set_window_frame(win_interactive_[5],msglist_[5],false); //msglistok
}

void NCInterface::display_grades(std::shared_ptr<Class> const& class_ptr) const {
	wclear(win_interactive_[3]);
	std::shared_ptr<GenericGrade> grade_ptr;
	for(unsigned int i(0);i<class_ptr->get_nstudents();i++){
		for(unsigned int j(0);j<class_ptr->get_nevals();j++){
			grade_ptr = (*class_ptr)(i,j);
			if(grade_ptr){
				mvwprintw(
						win_interactive_[3],
						geo_s_stude_[2]+i*geo_s_stude_[0],
						geo_s_evalu_[3]+j*geo_s_evalu_[1],
						grade_ptr->print_grade().c_str()
						);
				if(!grade_ptr->is_recorded()){ mvwprintw(win_interactive_[3],geo_s_stude_[2]+i*geo_s_stude_[0],geo_s_evalu_[3]+j*geo_s_evalu_[1]-2,"!");  }
				if(grade_ptr->is_catch_up()){ mvwprintw(win_interactive_[3],geo_s_stude_[2]+i*geo_s_stude_[0],geo_s_evalu_[3]+j*geo_s_evalu_[1]-1,"*"); }
			}
		}
	}
	set_window_frame(win_interactive_[3],msglist_[3],false); //msglistok
}

void NCInterface::display_evaluation_criteria(std::shared_ptr<Evaluation> const& eval_ptr) const {
	wclear(win_interactive_[1]);
	std::vector<std::string> list(eval_ptr->list_evaluation_criteria());
	for(unsigned int i(0);i<list.size();i++){
		WINDOW* tmp;
		tmp = derwin(
				win_interactive_[1],
				geo_s_exerc_[0],
				geo_s_exerc_[1],
				geo_s_exerc_[2],
				geo_s_exerc_[3]+i*geo_s_exerc_[1]
				);
		mvwprintw(tmp, 0, 0, list[i].c_str());
		wnoutrefresh(tmp);
		delwin(tmp);
	}
	set_window_frame(win_interactive_[1],msglist_[1],false); //msglistok
}

void NCInterface::display_exercise_fulfillment(std::shared_ptr<TestOnSkillsWB::Grade::Exercise> const& exercise_ptr, WINDOW* win, std::string const& info) const {
	wclear(win_interactive_[5]);
	for(unsigned int s(0);s<OnSkills::N_SKILL;s++){
		mvwprintw(win_interactive_[5], geo_s_crind_[0]*s+geo_s_crind_[2], 1, exercise_ptr->get_status(s).c_str());
	}
	wclear(win);
	wattron(win, A_BOLD);
	mvwprintw(win, 0,0, info.c_str());
	wattroff(win, A_BOLD);
	set_window_frame(win_interactive_[5],msglist_[5],false); //msglistok
	wnoutrefresh(win);
}

void NCInterface::display_exercise_skill_indicators(std::shared_ptr<OnSkills::Exercise::Skill> const& skill_ptr){
	wclear(win_interactive_[4]);
	set_window_frame(win_interactive_[4],skill_ptr->get_name(),false); //msglistok
	unsigned int i(0);
	for(auto const& l:skill_ptr->list_indicators()){
		mvwprintw(win_interactive_[4], ++i+1, 2, l.c_str());
	}
	wnoutrefresh(win_interactive_[4]);
}

void NCInterface::display_tp_sessions(std::shared_ptr<TP> const& tp_ptr) const {
	wclear(win_interactive_[1]);
	unsigned int iA(0);
	unsigned int iB(0);
	for(auto const& s:tp_ptr->get_sessions()){
		if(s->for_group_A()){
			mvwprintw(
					win_interactive_[1],
					geo_s_tpses_[2],
					geo_s_tpses_[3]+(iA++)*(1+geo_s_tpses_[1]),
					"%.*s",geo_s_tpses_[1],s->get_title(true,false).c_str()
					);
		} else {
			mvwprintw(
					win_interactive_[1],
					geo_s_tpses_[2]+1,
					geo_s_tpses_[3]+(iB++)*(1+geo_s_tpses_[1]),
					"%.*s",geo_s_tpses_[1],s->get_title(true,false).c_str()
					);
		}
	}
	set_window_frame(win_interactive_[1],msglist_[1],false); //msglistok
}

void NCInterface::display_tp_criteria_points(std::shared_ptr<TP::Report::Criteria> const& criteria_ptr, WINDOW* win) const {
	wclear(win_interactive_[5]);
	unsigned int i(0);
	for(auto const& ind:criteria_ptr->get_indicators()){
		mvwprintw(win_interactive_[5], geo_s_crind_[0]*(i++)+geo_s_crind_[2], 1, ind->get_status().c_str());
	}
	set_window_frame(win_interactive_[5],msglist_[5],false); //msglistok
	wclear(win);
	wattron(win, A_BOLD);
	mvwprintw(win, 0,0, criteria_ptr->get_info().c_str());
	wattroff(win, A_BOLD);
	wnoutrefresh(win);
}

void NCInterface::display_tp_validated(std::shared_ptr<TP::Grade> const& grade_ptr) const {
	wclear(win_interactive_[3]);
	std::shared_ptr<TP::Report> report_ptr;
	std::shared_ptr<TP::Report::Criteria> criteria_ptr;
	unsigned int i(0);
	for(auto const& r:grade_ptr->get_reports()){
		for(unsigned int c(0);c<4;c++){
			criteria_ptr = r->get(c);
			if(criteria_ptr){
				mvwprintw(
						win_interactive_[3],
						geo_s_exerc_[2]+i,
						geo_s_exerc_[3]+c*geo_s_exerc_[1],
						criteria_ptr->get_result_str(false,false).c_str() 
						);
			}
		}
		i++;
	}
	set_window_frame(win_interactive_[3],msglist_[3],false); //msglistok
}

void NCInterface::display_help() const {
	wclear(win_help_);
	switch(track_[0]){
		case S_CLASS[0]: case S_EVALU[0]: case S_ST_OV[0]: case S_ST_EV[0]:
		case S_EX_EV[0]: case S_ST_TP[0]: case A_TP_RE[0]: case S_TP_RE[0]:
		case S_ST_WA[0]: case S_TP_SE[0]: case S_ST_SE[0]:
			mvwprintw(win_help_, 1, 2, "Sélectionner: Écrire du texte");
			mvwprintw(win_help_, 2, 2, "           ou Utiliser les flèches"); break;
		case S_EX_IN[0]:
			mvwprintw(win_help_, 1, 2, "Utilisez les flèches 'haut/bas' pour");
			mvwprintw(win_help_, 2, 2, "sélectionner un indicateur"); break;
		case C_TP_RE[0]:
			mvwprintw(win_help_, 1, 2, "Utilisez les flèches 'haut/bas' pour");
			mvwprintw(win_help_, 2, 2, "sélectionner un indicateur et les flèches");
			mvwprintw(win_help_, 3, 2, "'gauche/droite' pour l'évaluer."); break;
		default:
			mvwprintw(win_help_, 1, 2, "Entrez les différents champs.");
			mvwprintw(win_help_, 9, 2, "PgUp et PgDown permettent de faire défiler");
			mvwprintw(win_help_,10, 2, "les options lorsque cela est possible."); break;
	}
	switch(track_[0]){
		case S_CLASS[0]: case S_EVALU[0]: case S_ST_OV[0]: case S_ST_EV[0]:
		case S_TP_RE[0]: case S_ST_WA[0]: case S_TP_SE[0]: case S_EX_IN[0]:
		case C_TP_RE[0]: case S_TP_IN[0]:
			mvwprintw(win_help_, 4, 2, "F1:  Ajouter"); break;
	}
	switch(track_[0]){
		case S_CLASS[0]: case S_EVALU[0]: case S_ST_OV[0]: case S_ST_EV[0]:
		case S_ST_WA[0]: case S_TP_SE[0]: case S_EX_IN[0]: case S_TP_IN[0]:
			mvwprintw(win_help_, 5, 2, "F2:  Éditer"); break;
		case S_ST_TP[0]: case S_ST_SE[0]:
			mvwprintw(win_help_, 5, 2, "F2:  Avertissements"); break;
		case C_TP_RE[0]:
			mvwprintw(win_help_, 5, 2, "F2:  Commenter"); break;
	}
	switch(track_[0]){
		case S_CLASS[0]: case S_EVALU[0]: case S_ST_OV[0]: case S_ST_EV[0]:
		case S_ST_TP[0]: case S_TP_RE[0]: case S_ST_WA[0]: case S_TP_SE[0]:
		case S_EX_IN[0]: case C_TP_RE[0]: case S_TP_IN[0]:
			mvwprintw(win_help_, 6, 2, "F3:  Supprimer");  break;
	}
	switch(track_[0]){
		case S_CLASS[0]: case S_EVALU[0]: case S_ST_OV[0]: case S_ST_TP[0]:
		case S_TP_RE[0]: case S_TP_SE[0]: case S_ST_SE[0]:
			mvwprintw(win_help_, 7, 2, "F4:  Latex"); break;
	}
	switch(track_[0]){
		case S_TP_SE[0]: case S_TP_RE[0]: case S_EVALU[0]: case S_ST_EV[0]:
			mvwprintw(win_help_,8, 2, "F5:  Mail"); break;
	}
	switch(track_[0]){
		case S_CLASS[0]: case S_EVALU[0]: case S_ST_OV[0]: case S_ST_EV[0]:
		case S_EX_EV[0]: case S_ST_TP[0]: case S_TP_RE[0]: case S_TP_SE[0]:
		case S_ST_SE[0]:
			mvwprintw(win_help_, 8, 22,"F12:       Sauvgarder"); break;
	}
	switch(track_[0]){
		case S_ST_OV[0]:
			mvwprintw(win_help_, 4, 22,"TAB:       Évaluations"); break;
		case S_ST_EV[0]:
			mvwprintw(win_help_, 4, 22,"TAB:       Exercices"); break;
		case S_EVALU[0]: case S_TP_SE[0]: case S_EX_EV[0]:
			mvwprintw(win_help_, 4, 22,"TAB:       Élèves"); break;
		case S_ST_TP[0]:
			mvwprintw(win_help_, 4, 22,"TAB:       Sessions"); break;
		case C_TP_RE[0]:
			mvwprintw(win_help_, 4, 22,"TAB:       Critères"); break;
		case S_TP_IN[0]:
			mvwprintw(win_help_, 4, 22,"TAB:       Suivant"); break;
	}
	switch(track_[0]){
		case S_CLASS[0]: case S_EVALU[0]: case S_ST_EV[0]: case S_EX_EV[0]:
		case S_ST_TP[0]: case A_TP_RE[0]: case S_TP_SE[0]: case S_ST_SE[0]:
		case S_TP_RE[0]:
			mvwprintw(win_help_, 5, 22,"Enter:     Sélectionner"); break;
		case S_TP_IN[0]: case S_ST_WA[0]: case C_TP_RE[0]:
			mvwprintw(win_help_, 5, 22,"Enter:     Valider"); break;
	}
	switch(track_[0]){
		case S_CLASS[0]: case S_EVALU[0]: case S_ST_OV[0]: case S_ST_EV[0]:
		case S_EX_EV[0]: case S_ST_TP[0]: case A_TP_RE[0]: case S_TP_RE[0]:
		case S_TP_SE[0]: case S_ST_SE[0]: case C_TP_RE[0]: case S_ST_WA[0]:
			mvwprintw(win_help_, 7, 22,"Esc:       Quitter"); break;
		case S_TP_IN[0]:
			mvwprintw(win_help_, 7, 22,"Esc:       Annuler"); break;
	}
	switch(track_[0]){
		case S_EVALU[0]: case S_ST_OV[0]: case S_ST_EV[0]: case S_EX_EV[0]:
		case S_ST_TP[0]: case A_TP_RE[0]: case S_TP_RE[0]: case S_TP_SE[0]:
		case S_ST_SE[0]:
			mvwprintw(win_help_, 6, 22,"Backspace: Retour"); break;
		case C_TP_RE[0]:
			mvwprintw(win_help_, 6, 22,"Backspace: Anuler"); break;
		case S_TP_IN[0]:
			mvwprintw(win_help_, 4, 22,"Backspace: Précédent"); break;
	}
	wnoutrefresh(win_help_);
}

void NCInterface::processing(std::string const& msg){
	wclear(win_help_);
	mvwprintw(derwin(win_help_,5,43,2,2), 0, 0, my::wrap(msg,42).c_str());
	set_window_frame(win_help_,"",true,2);
	status("En attente");
	doupdate();
}

void NCInterface::status(std::string info){
	if(debug_){
		mvwprintw(win_status_,0,0,"Action: %d",track_[0]);
		mvwprintw(win_status_,0,12,"Focus: %d",track_[1]);
		std::string tmp("Select: ");
		for(unsigned int i(0);i<N_ACTIONS;i++){
			if(i==track_[0]) { tmp += "|"+my::tostring(select_[i]) + (i+1<N_ACTIONS?"|":""); }
			else { tmp += " "+ my::tostring(select_[i]) + (i+1<N_ACTIONS?" ":""); }
		}
		mvwprintw(win_status_,0,22,tmp.c_str());
	}
	if(!info.size()){
		info = "Date: " + year_->get_date() + "   Semestre: " + year_->get_semester();
	}
	mvwprintw(win_status_,0,COLS-info.size()-1,info.c_str());
	wnoutrefresh(win_status_);
	wclear(win_status_);
}

/*{NCurses wrappers*/
void NCInterface::set_window_frame(WINDOW* win, std::string const& msg, bool const& highlight, unsigned int const& color) const {
	if(highlight){
		wattron(win,COLOR_PAIR(color));
		box(win,0,0);
		wattroff(win,COLOR_PAIR(color));
	} else { box(win,0,0); }
	mvwprintw(win,0,0,msg.c_str());
	wnoutrefresh(win);
}

void NCInterface::mvwprintwlist(WINDOW* win, unsigned int const& y, unsigned int const& x, unsigned int const& nlines, unsigned int const& ncols, std::vector<std::string> const& list){
	for(unsigned int i(0); i<list.size();i++){ mvwprintw(win, y+i*nlines, x+i*ncols, list[i].c_str()); }
	wnoutrefresh(win);
}

void NCInterface::mvwprintwlist(WINDOW* win, unsigned int const& y, unsigned int const& x, unsigned int const& nlines, unsigned int const& ncols, std::vector<unsigned int> const& list){
	for(unsigned int i(0); i<list.size();i++){ mvwprintw(win, y+i*nlines, x+i*ncols, "%d", list[i]); }
	wnoutrefresh(win);
}
/*}*/

const constexpr unsigned int NCInterface::A_CLASS[]; //Add class
const constexpr unsigned int NCInterface::E_CLASS[]; //Edit class
const constexpr unsigned int NCInterface::R_CLASS[]; //Remove class
const constexpr unsigned int NCInterface::L_CLASS[]; //Latex class
const constexpr unsigned int NCInterface::S_CLASS[]; //Select class

const constexpr unsigned int NCInterface::A_STUDE[]; //Add student
const constexpr unsigned int NCInterface::E_STUDE[]; //Edit student
const constexpr unsigned int NCInterface::R_STUDE[]; //Remove student
const constexpr unsigned int NCInterface::L_STUDE[]; //Latex student
const constexpr unsigned int NCInterface::S_ST_OV[]; //Select student overview
const constexpr unsigned int NCInterface::S_ST_EV[]; //Select student for given elvaluation
const constexpr unsigned int NCInterface::C_ST_EV[]; //Correct evaluation for given student
const constexpr unsigned int NCInterface::E_ST_EV[]; //Edit evaluation for given student
const constexpr unsigned int NCInterface::R_ST_GR[]; //Remove grade for given student
const constexpr unsigned int NCInterface::M_ST_GR[]; //Mail grade for given student

const constexpr unsigned int NCInterface::A_EVALU[]; //Add evaluation
const constexpr unsigned int NCInterface::E_EVALU[]; //Edit evaluation
const constexpr unsigned int NCInterface::R_EVALU[]; //Remove evaluation
const constexpr unsigned int NCInterface::L_EVALU[]; //Latex evaluation
const constexpr unsigned int NCInterface::M_EVALU[]; //Mail evaluation
const constexpr unsigned int NCInterface::S_EVALU[]; //Select evaluation
const constexpr unsigned int NCInterface::S_EX_EV[]; //Select exercise for given evaluation
const constexpr unsigned int NCInterface::C_EX_EV[]; //Correct given exercise

const constexpr unsigned int NCInterface::A_TP_SE[]; //Add TP session
const constexpr unsigned int NCInterface::E_TP_SE[]; //Edit TP session
const constexpr unsigned int NCInterface::R_TP_SE[]; //Remove TP session
const constexpr unsigned int NCInterface::L_TP_SE[]; //Latex TP session
const constexpr unsigned int NCInterface::M_TP_SE[]; //Mail TP session
const constexpr unsigned int NCInterface::S_TP_SE[]; //Select TP session

const constexpr unsigned int NCInterface::S_ST_SE[]; //Select student for given session
const constexpr unsigned int NCInterface::L_ST_TP[]; //Latex all reports for given student
const constexpr unsigned int NCInterface::S_ST_TP[]; //Select student for TP

const constexpr unsigned int NCInterface::A_TP_RE[]; //Add report to given student
const constexpr unsigned int NCInterface::R_TP_RE[]; //Remove report from given student
const constexpr unsigned int NCInterface::L_TP_RE[]; //Latex report for given student
const constexpr unsigned int NCInterface::M_TP_RE[]; //Mail report for given student
const constexpr unsigned int NCInterface::S_TP_RE[]; //Select report for given student
const constexpr unsigned int NCInterface::C_TP_RE[]; //Correct current repport

const constexpr unsigned int NCInterface::A_TP_IN[]; //Add indicator
const constexpr unsigned int NCInterface::E_TP_IN[]; //Edit indicator
const constexpr unsigned int NCInterface::R_TP_IN[]; //Remove indicator
const constexpr unsigned int NCInterface::S_TP_IN[]; //Select indicator

const constexpr unsigned int NCInterface::A_ST_WA[]; //Add student warning
const constexpr unsigned int NCInterface::E_ST_WA[]; //Edit student warning
const constexpr unsigned int NCInterface::R_ST_WA[]; //Remove student warning
const constexpr unsigned int NCInterface::S_ST_WA[]; //Select student warning

const constexpr unsigned int NCInterface::E_CR_CO[]; //Add comment to given criteria
const constexpr unsigned int NCInterface::E_AD_EV[]; //Edit evaluation that should be added

const constexpr unsigned int NCInterface::A_EX_IN[];
const constexpr unsigned int NCInterface::E_EX_IN[];
const constexpr unsigned int NCInterface::R_EX_IN[];
const constexpr unsigned int NCInterface::S_EX_IN[];
