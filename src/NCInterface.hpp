#ifndef DEF_NCINTERFACE
#define DEF_NCINTERFACE

#include "Year.hpp"
#include <ncurses.h>
#include <form.h>
#include <menu.h>
#include <regex>

class NCInterface{
	public:
		NCInterface(Year* year, bool const& debug, bool const& force_display);
		~NCInterface();
		/*{Forbidden*/
		NCInterface(NCInterface const&) = delete;
		NCInterface(NCInterface&&) = delete;
		NCInterface& operator=(NCInterface);
		/*}*/

		void class_select();
		bool const& is_ready() const { return is_ready_; }
		void config_mail();

	private:
		class Choice{
			public:
			Choice(std::string const& msg, bool const& sel):
				msg_(msg),sel_(sel){}

			unsigned int msg_size() const { return msg_.size(); }
			std::string const& msg() const { return msg_; }
			bool const& sel() const { return sel_; }
			bool& sel() { return sel_; }

			private:
			std::string msg_;
			bool sel_;
		};

		void class_add();
		void class_edit();
		void class_remove();
		void class_latex();

		void student_configure(std::string const& firstname, std::string const& lastname, std::string const& email, bool const& in_group_A, bool const& is_man, std::vector<FIELD*>& fields, unsigned int geometry[]);
		void student_add(std::shared_ptr<Class> const& class_ptr);
		void student_edit(std::shared_ptr<Class> const& class_ptr);
		void student_remove(std::shared_ptr<Class> const& class_ptr);
		void student_latex(std::shared_ptr<Class> const& class_ptr);
		void student_select_overview(std::shared_ptr<Class> const& class_ptr);
		void student_select_for_given_evaluation(std::shared_ptr<Class> const& class_ptr, std::shared_ptr<Evaluation> const& eval_ptr);
		void student_select_for_given_tp(std::shared_ptr<Class> const& class_ptr, std::shared_ptr<Evaluation> const& eval_ptr);
		void student_correct_evaluation(std::shared_ptr<Class> const& class_ptr, std::shared_ptr<Evaluation> const& eval_ptr);
		void student_correct_evaluation(std::shared_ptr<Class> const& class_ptr, std::shared_ptr<Evaluation> const& eval_ptr, std::shared_ptr<OnSkills> const& onskills_ptr);
		void student_select_for_given_session(std::shared_ptr<TP> const& tp_ptr);
		void student_remove_grade(std::shared_ptr<Class> const& class_ptr, std::shared_ptr<Evaluation> const& eval_ptr, std::shared_ptr<Student> const& student_ptr);
		void student_mail_grade(std::shared_ptr<Class> const& class_ptr, std::shared_ptr<Evaluation> const& eval_ptr);
		void student_edit_evaluation(std::shared_ptr<Class> const& class_ptr, std::shared_ptr<Evaluation> const& eval_ptr);
		void student_latex_tp(std::shared_ptr<Student> const& student_ptr, std::shared_ptr<TP> const& tp_ptr);
		void student_select_warning(std::shared_ptr<Student> const& student_ptr, std::shared_ptr<TP> const& tp_ptr);

		void evaluation_add(std::shared_ptr<Class> const& class_ptr);
		bool evaluation_add_tp(std::shared_ptr<Class> const& class_ptr);
		void evaluation_edit(std::shared_ptr<Class> const& class_ptr);
		void evaluation_remove(std::shared_ptr<Class> const& class_ptr);
		void evaluation_latex(std::shared_ptr<Class> const& class_ptr);
		void evaluation_mail(std::shared_ptr<Class> const& class_ptr);
		void evaluation_select(std::shared_ptr<Class> const& class_ptr);
		void evaluation_select_exercise(std::shared_ptr<Class> const& class_ptr, std::shared_ptr<Evaluation> const& eval_ptr);
		void evaluation_correct_exercise(std::shared_ptr<Class> const& class_ptr, std::shared_ptr<Evaluation> const& eval_ptr);
		bool evaluation_configure(std::shared_ptr<Class> const& class_ptr, std::shared_ptr<Note> const& eval_ptr, bool const& add_new_evaluation);
		bool evaluation_configure(std::shared_ptr<Class> const& class_ptr, std::shared_ptr<Test> const& eval_ptr, bool const& add_new_evaluation);
		bool evaluation_configure(std::shared_ptr<Class> const& class_ptr, std::shared_ptr<TestWB> const& eval_ptr, bool const& add_new_evaluation);
		bool evaluation_configure(std::shared_ptr<OnSkills> const& eval_ptr, bool const& add_new_evaluation);

		void exercise_add_indicator(std::shared_ptr<OnSkills::Exercise::Skill> const& skill_ptr);
		void exercise_edit_indicator(std::shared_ptr<OnSkills::Exercise::Skill> const& skill_ptr);
		void exercise_remove_indicator(std::shared_ptr<OnSkills::Exercise::Skill> const& skill_ptr);

		void tp_session_configure(std::string const& title, Date const& date,  bool const& for_group_A, std::vector<Choice>& criteria, std::vector<FIELD*>& fields, unsigned int geometry[]);
		void tp_session_indicator_configure(unsigned int const& threshold, std::vector<std::shared_ptr<TP::Session::Indicator> > const& indicators , std::vector<FIELD*>& fields, unsigned int geometry[]);
		void tp_warning_configure(unsigned int const& criteria, std::vector<std::string> const& list_sessions, unsigned int const& current_choice, std::string const& comment, std::vector<FIELD*>& fields, unsigned int geometry[]);

		void tp_add_criteria_session(unsigned int const& criteria, std::shared_ptr<TP> const& tp_ptr, std::shared_ptr<TP::Session> const& session_ptr);
		void tp_edit_criteria_session(unsigned int const& criteria, std::shared_ptr<TP::Session> const& session_ptr);
		void tp_edit_criteria_comment(std::shared_ptr<TP::Report::Criteria> const& criteria_ptr);

		void tp_add_report(std::shared_ptr<TP::Grade> const& grade_ptr);
		void tp_remove_report(std::shared_ptr<TP::Grade> const& grade_ptr);
		void tp_latex_report(std::shared_ptr<TP::Report> const& report_ptr);
		void tp_mail_report(std::shared_ptr<TP::Report> const& report_ptr);
		void tp_select_report(std::shared_ptr<Class> const& class_ptr);
		void tp_correct_report(std::shared_ptr<TP::Report> const& report_ptr);

		void tp_add_indicator(unsigned int const& criteria, std::shared_ptr<TP> const& tp_ptr);
		void tp_edit_indicator(unsigned int const& criteria, std::shared_ptr<TP> const& tp_ptr);
		void tp_remove_indicator(unsigned int const& criteria, std::shared_ptr<TP> const& tp_ptr);

		void tp_add_session(std::shared_ptr<TP> const& tp_ptr);
		void tp_edit_session(std::shared_ptr<TP> const& tp_ptr);
		void tp_remove_session(std::shared_ptr<TP> const& tp_ptr);
		void tp_latex_session(std::shared_ptr<TP> const& tp_ptr);
		void tp_mail_session(std::shared_ptr<TP> const& tp_ptr);
		void tp_select_session(std::shared_ptr<Class> const& class_ptr, std::shared_ptr<TP> const& tp_ptr);

		void tp_add_warning(std::shared_ptr<TP::Grade> const& grade_ptr);
		void tp_edit_warning(std::shared_ptr<TP::Grade> const& grade_ptr);
		void tp_remove_warning(std::shared_ptr<TP::Grade> const& grade_ptr);

		void select(std::vector<std::string> const& list, unsigned int const geometry[]);
		void select(std::vector<std::string> const& list, std::vector<WINDOW*> const& sel_win);
		bool confirm(std::string const& msg, unsigned int const& color=3);

		bool fill_form(std::vector<FIELD*>& fields, unsigned int const geometry[], std::string const& msg);
		void add_text_form(std::vector<FIELD*>& fields, std::string const& input, std::string const& output, unsigned int geometry[]);
		void add_password_form(std::vector<FIELD*>& fields, std::string const& input, unsigned int geometry[]);
		void add_double_form(std::vector<FIELD*>& fields, std::string const& input, double const& output, double const& min, double const& max, unsigned int const& in_line, unsigned int geometry[]);
		void add_int_form(std::vector<FIELD*>& fields, std::string const& input, int const& output, int const& min, int const& max, unsigned int const& padding, unsigned int const& in_line, unsigned int geometry[]);
		void add_date_form(std::vector<FIELD*>& fields, Date const& input, unsigned int geometry[]);
		void add_list_form(std::vector<FIELD*>& fields, std::string const& input, std::vector<std::string> const& choices, unsigned int const& current_choice, unsigned int geometry[]);
		void add_choices_form(std::vector<FIELD*>& fields, std::string const& input, std::vector<Choice>& output, bool horizontal, unsigned int geometry[]);

		void display_class_results(std::shared_ptr<Class> const& class_ptr, std::shared_ptr<Evaluation> const& eval_ptr) const;
		void display_class_results(std::vector<std::shared_ptr<Student> > students, std::shared_ptr<TP> const& tp_ptr) const;
		void display_grades(std::shared_ptr<Class> const& class_ptr) const;
		void display_evaluation_criteria(std::shared_ptr<Evaluation> const& eval_ptr) const;
		void display_exercise_fulfillment(std::shared_ptr<GradeOnSkills::Exercise> const& exercise_ptr, WINDOW* win, std::string const& info) const;
		void display_exercise_skill_indicators(std::shared_ptr<OnSkills::Exercise::Skill> const& skill_ptr);
		void display_tp_sessions(std::shared_ptr<TP> const& tp_ptr) const;
		void display_tp_criteria_points(std::shared_ptr<TP::Report::Criteria> const& criteria_ptr, WINDOW* win) const;
		void display_tp_validated(std::shared_ptr<TP::Grade> const& grade_ptr) const;
		void display_help() const;

		void status(std::string info="");
		void processing(std::string const& msg);

		void set_window_frame(WINDOW* win, std::string const& msg, bool const& highlight, unsigned int const& color=1) const;
		void mvwprintwlist(WINDOW* win, unsigned int const& y, unsigned int const& x, unsigned int const& ih, unsigned int const& iw, std::vector<std::string> const& list);
		void mvwprintwlist(WINDOW* win, unsigned int const& y, unsigned int const& x, unsigned int const& ih, unsigned int const& iw, std::vector<unsigned int> const& list);

		Year* year_;
		WINDOW* win_interactive_[6];
		WINDOW* win_help_;
		WINDOW* win_status_;
		bool debug_;
		bool is_ready_ = true;
		bool keepon_ = true;
		bool modified_ = false;
		unsigned int const nfw_ = 6; //!< number of interactive windows
		unsigned int const fw_ = 5;  //!< width of the fields where the points are entered
		unsigned int const select_suggestions_max_length = 26;  //!< max string length of the suggestions given when selecting
		/*The four entris are the window dimension: {nlines,ncols,begin_y,begin_x}*/
		/*The last two are the xy-shifts {shift_y,shift_x}*/
		unsigned int geo_s_class_[6] = {1,10,2,2,0,1};
		unsigned int geo_s_stude_[6] = {1,38,2,2,1,0};
		unsigned int geo_s_evalu_[6] = {5,16,2,2,0,1};
		unsigned int geo_a_evalu_[6] = {2,20,2,3,0,1};
		unsigned int geo_s_exerc_[6] = {3,9,2,2,0,1};
		unsigned int geo_s_tprep_[6] = {1,38,2,2,1,0};
		unsigned int geo_s_repcr_[6] = {3,9,2,2,0,1};
		unsigned int geo_a_repcr_[6] = {2,4,2,2,0,1};
		unsigned int geo_s_crind_[6] = {1,100,2,2,1,0};
		unsigned int geo_s_stuse_[6] = {1,38,2,2,1,0};
		unsigned int geo_s_tpses_[6] = {1,666,2,2,1,1};
		/*The last one is the field width*/
		unsigned int fill_row_dis_[5] = {1,666,666,3,666}; //Need to define ncols and begin_y
		unsigned int fill_col_dis_[5] = {666,666,2,666,1}; //Need to define total nlines and begin_x

		std::string msglist_[6] = {"Classes","Évaluations","Élèves","Note","Saisie","Moyenne"};

		/*Action list
		  + first entry indexes the action
		  + select window focus
		  */
		static const constexpr unsigned int A_CLASS[2] = {0,4}; //Add class
		static const constexpr unsigned int E_CLASS[2] = {1,4}; //Edit class
		static const constexpr unsigned int R_CLASS[2] = {2,0}; //Remove class
		static const constexpr unsigned int L_CLASS[2] = {3,0}; //Latex class
		static const constexpr unsigned int S_CLASS[2] = {4,0}; //Select class

		static const constexpr unsigned int A_STUDE[2] = {5,4};  //Add student
		static const constexpr unsigned int E_STUDE[2] = {6,4};  //Edit student
		static const constexpr unsigned int R_STUDE[2] = {7,2};  //Remove student
		static const constexpr unsigned int L_STUDE[2] = {8,2};  //Latex student
		static const constexpr unsigned int S_ST_OV[2] = {9,2};  //Select student overview
		static const constexpr unsigned int S_ST_EV[2] = {10,2}; //Select student for given elvaluation
		static const constexpr unsigned int C_ST_EV[2] = {11,3}; //Correct evaluation for given student
		static const constexpr unsigned int E_ST_EV[2] = {12,2}; //Edit evaluation for given student
		static const constexpr unsigned int R_ST_GR[2] = {13,2}; //Remove grade for given student
		static const constexpr unsigned int M_ST_GR[2] = {14,2}; //Mail grade for given student

		static const constexpr unsigned int A_EVALU[2] = {15,4}; //Add evaluation
		static const constexpr unsigned int E_EVALU[2] = {16,4}; //Edit evaluation
		static const constexpr unsigned int R_EVALU[2] = {17,1}; //Remove evaluation
		static const constexpr unsigned int L_EVALU[2] = {18,1}; //Latex evaluation
		static const constexpr unsigned int M_EVALU[2] = {19,1}; //Mail evaluation
		static const constexpr unsigned int S_EVALU[2] = {21,1}; //Select evaluation
		static const constexpr unsigned int S_EX_EV[2] = {22,1}; //Select exercise for given evaluation
		static const constexpr unsigned int C_EX_EV[2] = {23,3}; //Correct given exercise

		static const constexpr unsigned int A_TP_SE[2] = {23,4}; //Add TP session
		static const constexpr unsigned int E_TP_SE[2] = {24,4}; //Edit TP session
		static const constexpr unsigned int R_TP_SE[2] = {25,4}; //Remove TP session
		static const constexpr unsigned int L_TP_SE[2] = {26,2}; //Latex TP session
		static const constexpr unsigned int M_TP_SE[2] = {27,1}; //Mail TP session
		static const constexpr unsigned int S_TP_SE[2] = {28,1}; //Select TP session

		static const constexpr unsigned int S_ST_SE[2] = {29,2}; //Select student for given session
		static const constexpr unsigned int L_ST_TP[2] = {30,2}; //Latex all reports for given student
		static const constexpr unsigned int S_ST_TP[2] = {31,2}; //Select student for TP

		static const constexpr unsigned int A_TP_RE[2] = {32,4}; //Add report to given student
		static const constexpr unsigned int R_TP_RE[2] = {33,2}; //Remove report from given student
		static const constexpr unsigned int L_TP_RE[2] = {34,2}; //Latex report for given student
		static const constexpr unsigned int M_TP_RE[2] = {35,1}; //Mail report for given student
		static const constexpr unsigned int S_TP_RE[2] = {36,2}; //Select report for given student
		static const constexpr unsigned int C_TP_RE[2] = {37,3}; //Correct current repport

		static const constexpr unsigned int A_TP_IN[2] = {38,4}; //Add TP indicator
		static const constexpr unsigned int E_TP_IN[2] = {39,4}; //Edit TP indicator
		static const constexpr unsigned int R_TP_IN[2] = {40,3}; //Remove TP indicator
		static const constexpr unsigned int S_TP_IN[2] = {41,3}; //Select TP indicator

		static const constexpr unsigned int A_ST_WA[2] = {42,4}; //Add student warning
		static const constexpr unsigned int E_ST_WA[2] = {43,4}; //Edit student warning
		static const constexpr unsigned int R_ST_WA[2] = {44,3}; //Remove student warning
		static const constexpr unsigned int S_ST_WA[2] = {45,3}; //Select student warning

		static const constexpr unsigned int E_CR_CO[2] = {46,4}; //Add comment to given criteria
		static const constexpr unsigned int E_AD_EV[2] = {47,4}; //Edit evaluation that should be added

		static const constexpr unsigned int A_EX_IN[2] = {48,4}; //Add exercise indicator
		static const constexpr unsigned int E_EX_IN[2] = {49,4}; //Edit exercise indicator
		static const constexpr unsigned int R_EX_IN[2] = {50,3}; //Remove exercise indicator
		static const constexpr unsigned int S_EX_IN[2] = {51,3}; //Select exercise indicator

		static unsigned int const N_ACTIONS = 52;
		unsigned int select_[N_ACTIONS];
		unsigned int const* track_;
};
#endif
