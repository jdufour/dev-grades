#ifndef DEF_TESTONSKILLSWBC
#define DEF_TESTONSKILLSWBC

#include "TestWB.hpp"
#include "OnSkills.hpp"

class TestOnSkillsWB: public TestWB, public OnSkills{
	public:
		TestOnSkillsWB() = default;
		TestOnSkillsWB(IOFiles& r);
		~TestOnSkillsWB() = default;
		/*{Forbidden*/
		TestOnSkillsWB(TestOnSkillsWB&&) = delete;
		TestOnSkillsWB(TestOnSkillsWB const&) = delete;
		TestOnSkillsWB& operator=(TestOnSkillsWB const&) = delete;
		/*}*/

		class Grade;

		virtual void edit(std::string const& title, Date const& date, double const& weight, double const& deducted_points, double const& bonus_coef, double const& max_bonus_point, Vector<double> const& max_points) override;
		void save(IOFiles& w) const override;
		void compute_evaluation_stats() override;
		void student_feedback(Latex& latex, std::shared_ptr<GenericGrade> const& grade) const override;
		unsigned int get_n_criteria_to_fill() const override { return 1; }

		unsigned int const& get_type() const override { return TYPE; }
		static const constexpr unsigned int TYPE = 4;
		static const constexpr char* NAME = "Test avec bonus\n(compétences)";
};

class TestOnSkillsWB::Grade: public TestWB::Grade, public GradeOnSkills{
	public:
		Grade(std::shared_ptr<Student> const& student, std::shared_ptr<Evaluation> const& eval);
		Grade(std::shared_ptr<Student> const& student, std::shared_ptr<Evaluation> const& eval, IOFiles& r);
		virtual ~Grade() override = default;
		/*{Forbidden*/
		Grade(Grade&&) = delete;
		Grade(Grade const&) = delete;
		Grade& operator=(Grade const&) = delete;
		/*}*/

		void save(IOFiles& w) const override;
		void compute_grade() override;
		void reset_grade(unsigned int const& n) override;

		std::string get_exercise_point_info(unsigned int const& i) const override;
};
#endif
