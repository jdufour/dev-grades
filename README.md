# jdgrades

## Install

Download via:

    git clone --recursive https://jdufour@bitbucket.org/jdufour/dev-grades.git

Go in the **src/** directory and type:

+ **make** to create the **jdgrade** executable.
+ **make update** to compile the  **jdgrade** executable to transit to a new **.jdbin** file version.
+ **make debug** to compile the **jdgrade** executable compatible for **gdb**.
+ **make install** move the executable in the user defined directory (default is **bin/**).
+ **make ref** to build the partial references in the **doxyfile/** directory.
+ **make clean** to clean the  **src/** directory.

You can configure some options by editing the file **lib/jdlib/config.mk**

On OSX, *curl* and *ncurses* can be installed via:

    brew install ncurses curl-openssl

For Linux, the required *curl* version is shipped in a git submodule linked to
this project.

On top of that, the code requires to have the locale *fr_CH.UTF-8* correctly
defined. To do so, use **dpkg-reconfigure locales** (works on Debian) and add
it to the list. This locale is required to correctly sort the student names
taking into account various accents and typographic signs.

## Run

On the first run, it is advised to only type:

    ./jdgrades

This will display some help and ask you to choose where the files should be
saved. If you choose to save your data in the directory **/some/path/**, you
can reload them later by running:

    ./jdgrades -path /some/path/

Be aware that your data will be stored as binary files with extension
**.jdbin** in the directory **/some/path/jdbin/**. 

To get help on the different options, type:

    ./jdgrades -help

For instance, if you want to undo some modifications that were saved, you can
just remove the most recent binary file or run the program with:

    ./jdgrades -path /some/path/ -backup

## Update

To update the code, simply type:

    make clean
    make git
    make 

## Versions
The different versions are tagged in **git**. You can't jump from one version
to another without having problem with the **.jdbin** files. This is because
the data are differently saved from one version to another. The different
versions can be listed with:

    git tag

If the previous command doesn't return anything, try it again after running:

    git pull --tags

### From v1.2 to v1.4
Starting from **v1.2**, you can simply update from **1.n** the latest version
by doing

    make clean
    make update v=1_n

where *n* is the version you want to update to `(1<n<4)`. This creates an
executable that can load the **.jdbin** file compatible with the **1.n**
version. Using this executable, run 

    ./jdgrades -path /some/path/ -save

to create a new backup compatible with the latest version. You can then exit
the program right after its opening, a backup will be created anyway. To
finalize the update, it is important to recompile the sources with:

    make clean
    make

### From v1.4 and above
The procedure is simplified thanks to a new variable saved in the **.jdbin**
file, the version number. Once the conversion to **v1.4** is successful, all
following version should correctly load. Just compile the latest version and
run the code. The next **.jdbin** file saved will compatible with the latest
version.

## Git trick

In order to easily see the evolution of the graph, add this line to your
**~/.gitconfig** configuration file:

    gr = log --graph --full-history --all --color --date=short --pretty=tformat:"%x1b[31m%h%x09%Cblue%cd%x1b[32m%d%x1b[0m%x20%s%x20%x1b[33m(%an)%x1b[0m" --author-date-order

This allows you to type:

    git gr

to get the whole history with the different versions.
